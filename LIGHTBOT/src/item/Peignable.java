package item;

import javafx.animation.Transition;
import javafx.animation.TranslateTransition;
import javafx.scene.paint.Color;
import javafx.util.Duration;
import lightbotException.ElectrocutionBot;
import main.Main;
import cases.CasesPleine;
import robot.Robot;

public class Peignable extends ItemsStatique{ 
	public Peignable(){
		couleur = Color.RED;
		nom = "Peignable";
	}
	public Transition run(Robot bot, CasesPleine lacase) throws ElectrocutionBot {
		couleur = Color.YELLOW;
		
		return lacase.changeColorTransition(Color.YELLOW, 500);
	}
}

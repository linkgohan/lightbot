package item;

import position.Position3D;

public class ItemsDynamique extends Items{
	private Position3D position;
	
	public ItemsDynamique(int x, int y, int z){
		position = new Position3D(x,y,z);
	}

	public Position3D getPosition() {
		return position;
	}

	public void setPosition(int x, int y, int z) {
		position = new Position3D(x,y,z);
	}
}

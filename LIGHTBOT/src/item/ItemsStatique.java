package item;

import javafx.animation.Transition;
import lightbotException.ElectrocutionBot;
import cases.CasesPleine;
import robot.Robot;

public abstract class ItemsStatique extends Items{
	public abstract Transition run(Robot bot, CasesPleine lacase) throws ElectrocutionBot;
}

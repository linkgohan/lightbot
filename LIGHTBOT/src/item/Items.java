package item;

import javafx.scene.paint.Color;

public class Items {
	protected Color couleur;
	protected String nom;
	
	public String getNom(){
		return nom;
	}
	
	public Color getColor(){
		return couleur;
	}
	
	public void setColor(Color c){
		couleur = c;
	}
}

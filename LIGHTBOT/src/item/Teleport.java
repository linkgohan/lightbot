package item;

import java.io.File;

import javafx.animation.FadeTransition;
import javafx.animation.SequentialTransition;
import javafx.animation.Transition;
import javafx.animation.TranslateTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;
import javafx.util.Duration;
import lightbotException.ElectrocutionBot;
import lightbotException.OutOfBoundBot;
import position.Position3D;
import robot.Robot;
import world.World;
import cases.Cases;
import cases.CasesPleine;

public class Teleport extends ItemsStatique{
	private int x_vers;
	private int y_vers;
	private int z_vers;
	
	private Position3D pos = new Position3D(-1,-1,-1);
	
	public Teleport(int x, int y, int z){
		x_vers = x;
		y_vers = y;
		z_vers = z;
		couleur = Color.GRAY;
		nom = "Teleport";
	}
	
	public Teleport(int x, int y, int z, Position3D posi){
		x_vers = x;
		y_vers = y;
		z_vers = z;
		couleur = Color.GRAY;
		nom = "Teleport";
		pos = posi;
	}
	
	public void setPos(Position3D posi){
		pos = posi;
	}
	
	public Position3D getPos(){
		return pos;
	}
	
	public Transition run(Robot bot, CasesPleine lacase) throws ElectrocutionBot {
		SequentialTransition s = new SequentialTransition();
		
		FadeTransition ft1 = new FadeTransition(Duration.millis(250), bot.getRobot3D());
		FadeTransition ft2 = new FadeTransition(Duration.millis(250), bot.getRobot3D());
		
		ft1.setFromValue(1.0);
		ft1.setToValue(0.0);
		
		ft1.setOnFinished(new EventHandler<ActionEvent>(){
            @Override
            public void handle(ActionEvent arg0) {
            	 final File file = new File("src/menu/teleport.mp3"); 
                 final Media media2 = new Media(file.toURI().toString()); 
                 MediaPlayer mediaPlayer = new MediaPlayer(media2);
             	 mediaPlayer.play();
            }
        });
		
		ft2.setFromValue(0.0);
		ft2.setToValue(1.0);
		
	    TranslateTransition t = new TranslateTransition(Duration.millis(1),bot.getRobot3D());
	    t.setToX(x_vers*Cases.SIZE);
	    try {
			t.setToY(-(World.get_case(x_vers, y_vers).getPos().get_z())*Cases.SIZE+bot.tailleR());
		} catch (OutOfBoundBot e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    t.setToZ(y_vers*Cases.SIZE);
	    
	    
	    s.getChildren().addAll(ft1,t,ft2);
	    
		bot.setPosition(x_vers, y_vers, z_vers);
		
		return s;
	}
	
	public int get_xvers(){
		return x_vers;
	}
	
	public int get_yvers(){
		return y_vers;
	}
	
	public int get_zvers(){
		return z_vers;
	}
	
	public void set_xvers(int x, int y, int z){
		x_vers = x;
		y_vers = y;
		z_vers = z;
	}
	
	public boolean canInit(){
		return x_vers == -1 && y_vers == -1 && z_vers == -1;
	}
}

package item;

import javafx.animation.Transition;
import javafx.animation.TranslateTransition;
import javafx.scene.paint.Color;
import javafx.util.Duration;
import lightbotException.ElectrocutionBot;
import main.Main;
import cases.CasesPleine;
import robot.Robot;

public class Inerte extends ItemsStatique{
	public Inerte(){
		couleur = Color.CYAN;
		nom = "Inerte";
	}
	public Transition run(Robot bot, CasesPleine lacase) throws ElectrocutionBot {
		Main.addT(bot.getRobotId(), new TranslateTransition(Duration.millis(500),bot.getRobot3D()));
		throw new ElectrocutionBot();
	}
}

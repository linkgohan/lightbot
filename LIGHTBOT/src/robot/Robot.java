package robot; 

import item.Teleport;

import java.awt.Color;
import java.net.URL;

import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.ParallelTransition;
import javafx.animation.RotateTransition;
import javafx.animation.Timeline;
import javafx.animation.Transition;
import javafx.animation.TranslateTransition;
import javafx.beans.binding.When;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.geometry.Point3D;
import javafx.scene.Cursor;
import javafx.scene.DepthTest;
import javafx.scene.Group;
import javafx.scene.shape.DrawMode;
import javafx.scene.shape.MeshView;
import javafx.scene.shape.Sphere;
import javafx.util.Duration;
import lightbotException.ElectrocutionBot;
import lightbotException.OutOfBoundBot;
import main.Main;
import position.Position3D;
import procedures.Procedures;
import thread.ThreadTemps;
import world.World;
import affichage.AffichagePartie;
import cases.Cases;
import cases.CasesPleine;

import com.interactivemesh.jfx.importer.obj.ObjModelImporter;

import direction.Direction;


public class Robot extends Thread{
	private Position3D position;
	Color couleur; 
	private Direction LaDirection;
	private Group robot3D;
	private double tailler;
	private int nb;
	private int posix;
	private int posiy;
	private int ident;
	private boolean seul = false;
	private boolean continuer = false;
	private Direction La1stDirection;
	private Teleport Objet_teleport = new Teleport(-1,-1,-1);
	
	private Procedures PMain;
	private Sphere sphere;
	
	public boolean samePosBalise() {
		return position.equals(Objet_teleport.getPos());
	}
	
	public void initTP(){
		Objet_teleport.setPos(new Position3D(position));
		Objet_teleport.set_xvers(position.get_x(), position.get_y(), position.get_z());
	}
	
	public void takeTP(){
		Objet_teleport.set_xvers(-1, -1, -1);
	}
	
	public boolean canInitTP(){
		return Objet_teleport.canInit();
	}
	
	public Transition runTP(){
		try {
			return Objet_teleport.run(this, (CasesPleine) World.get_case(position.get_x(), position.get_y()));
		} catch (ElectrocutionBot e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (OutOfBoundBot e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new TranslateTransition(Duration.millis(500),this.getRobot3D());
	}
	
	public int getRobotId(){
		return ident;
	}
	
	public int getPosix(){
		return posix;
	}
	
	public int getPosiy(){
		return posiy;
	}
	
	public int getPosiz(){
		return nb;
	}
	
	public Direction get1stDir(){
		return La1stDirection;
	}
	
	public boolean getContinuer(){
		return continuer;
	}
	
	public boolean getSeul(){
		return seul;
	}
	
	public void setSeul(){
		seul = true;
	}
	
	public void setNotSeul(){
		seul = false;
	}
	
	public void setMain(Procedures M){
		PMain = M;
	}
	
	public void run(){
		try {
			PMain.run(this);
			Main.thread.stop();
			Procedures.continuer();
			if(World.VerifyCasesAColorer()){
	 			System.out.println("Gagner spe"+this.getRobotId());
	 		}
			else{
				System.out.println("Perdu spe"+this.getRobotId());
			}
			
			if(this.getRobotId() == 0 && World.getNbRobot()>1){
				World.getRobot(1).notifyR();
				World.getRobot(1).setSeul();
			}
			
			if(this.getRobotId() == 1){
				World.getRobot(0).notifyR();
				World.getRobot(0).setSeul();
			}
		} catch (OutOfBoundBot e) {
			
			System.out.println("BALALALALAL spe"+this.getRobotId());
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(this.getRobotId() == 0 && World.getNbRobot()==1){
			if(World.VerifyCasesAColorer()){
				System.out.println("GGGGGGGGGGGGG "+this.getRobotId());
				World.debloqueLvl();
				AffichagePartie.gagner();
	 		}
			else{
				AffichagePartie.perdu();
			}
			
			Main.playT1();
		}
		
		if(this.getRobotId() == 0 && World.getNbRobot()>1 && World.getNbRobot()>1){
			while(World.getRobot(1).isAlive());
			
			if(World.VerifyCasesAColorer()){
				System.out.println("GGGGGGGGGGGGG "+this.getRobotId());
	 			World.debloqueLvl();
	 			AffichagePartie.gagner();
			}
			else{
				AffichagePartie.perdu();
			}
			
			Main.playT12();
		}
		
		this.stop();
	}
	
	public void notifyR(){
		this.continuer = true;
	}
	
	public void waitR(){
		this.continuer = false;
	}
	
	public Robot(int x, int y, int z, Direction.capType monCap, int id) {
		position = new Position3D(x,y,z);
		couleur = Color.BLUE;
		LaDirection = new Direction(monCap);
		ident = id;
		
		
		
		this.saveFirst();
	}
	
	public Robot(Robot r) {
		position = new Position3D(r.getPosix(),r.getPosiy(),r.getPosiz());
		nb = r.getPosiz();
		posix = r.getPosix();
		posiy = r.getPosiy();
		couleur = Color.BLUE;
		LaDirection = r.get1stDir();
		La1stDirection = r.get1stDir();
		ident = r.getRobotId();
		this.tailler = r.tailleR();
		this.robot3D = r.getRobot3D();
	}

	public void setDirection(Direction.capType newCap){
		LaDirection.setCap(newCap);
	}

	public Direction getDirection(){
		return LaDirection;
	}

	public void setDirection(Direction newCap){
		LaDirection.setCap(newCap.getCap());
	}
	
	public void setPosition(int x, int y, int z) {
		position = new Position3D(x,y,z);
	}

	public Position3D getPosition(){
		return position;
	}
	
	public void createBaymax(){
		
		final BooleanProperty showWireframe = new SimpleBooleanProperty(false);

		// BayMax Import OBJ 3d Model
        ObjModelImporter BMaxImporter = new ObjModelImporter();
        URL objUrl = this.getClass().getResource("baymax_super.obj");
        BMaxImporter.read(objUrl);
        MeshView[] BMaxMesh = BMaxImporter.getImport();
        BMaxImporter.close();

        Group BMaxNode = new Group();
        for (MeshView _BMaxMesh : BMaxMesh) {
        	BMaxNode.getChildren().addAll(_BMaxMesh);
            _BMaxMesh.drawModeProperty().bind(new When(showWireframe).then(DrawMode.LINE).otherwise(DrawMode.FILL));
        }
        //On change le curseur sur le Baymax
		BMaxNode.setCursor(Cursor.HAND);
        
        //Agrandir Baymax
        BMaxNode.setScaleX(20);
        BMaxNode.setScaleY(20);
        BMaxNode.setScaleZ(20);
        //Direction du Baymax
        
        
        
        //Event onClick sur Baymax
//		EventHandler<MouseEvent> eventBaymax = new EventHandler<MouseEvent>() {
//
//			@Override
//			public void handle(MouseEvent event) {
//		        
//		        final File file = new File("C:\\Users\\Jordan\\Music\\jojo-what-are-you-doing.mp3"); 
//		        final Media media = new Media(file.toURI().toString()); 
//		        final MediaPlayer mediaPlayer = new MediaPlayer(media); 
//		        mediaPlayer.play(); 
//			
//			}
//		};
//		BMaxNode.setOnMouseClicked(eventBaymax);
		//Fin eventBaymax
		robot3D = BMaxNode;
		
		tailler = ((double)Cases.SIZE-(robot3D.getBoundsInLocal().getHeight()*20))+5;
		
		
		robot3D.setTranslateX((position.get_x()*Cases.SIZE));
		robot3D.setTranslateY(tailler-(nb*Cases.SIZE));
		robot3D.setTranslateZ((position.get_y()*Cases.SIZE));
		
		BMaxNode.setRotationAxis(new Point3D(0, 1, 0));
        
		switch(LaDirection.getCap()){
		case Nord:
		{
			BMaxNode.setRotate(90);
			break;
		}
		case Ouest:
		{
			BMaxNode.setRotate(0);
			break;
		}
		case Est:
		{
			BMaxNode.setRotate(180);
			break;
		}
		default:
			BMaxNode.setRotate(-90);
		}
		
		robot3D.setDepthTest(DepthTest.ENABLE);
	}
	
	public void saveFirst(){
		nb = position.get_z();
		posix = position.get_x();
		posiy = position.get_y();
		La1stDirection = new Direction(LaDirection.getCap());
	}
	
	public RotateTransition refreshBaymaxRotate(int angle){
		RotateTransition rt = new RotateTransition(Duration.millis(500), robot3D);
	    rt.setByAngle(angle);
	    rt.setCycleCount(1);
	    
	    return rt;
	}
	
	public Timeline refreshBaymax(){
		
		int ancienX = (int) ((robot3D.getTranslateX())/Cases.SIZE);
		int ancienY = (-(int) (robot3D.getTranslateY()/Cases.SIZE));
		int ancienZ = (int) ((robot3D.getTranslateZ())/Cases.SIZE);
		int newX = position.get_x();
		int newY = position.get_y();
		int newZ = position.get_z();
		
		System.out.println(""+ancienX);
		System.out.println(""+ancienY);
		System.out.println(""+ancienZ);
		System.out.println(""+newX);
		System.out.println(""+newY);
		System.out.println(""+newZ);
		
		int majX = newX;
		int majY = newZ;
		int majZ = newY;
		
		Timeline timeline = new Timeline();
		timeline.setCycleCount(1);
		
		KeyValue kv;
		KeyFrame kf;
		
		if(majX!=ancienX){
			kv = new KeyValue(robot3D.translateXProperty(), majX*Cases.SIZE);
			kf = new KeyFrame(Duration.millis(2000), kv);
			timeline.getKeyFrames().add(kf);
			robot3D.setTranslateX(robot3D.getTranslateX() + ((majX-ancienX)*Cases.SIZE));
		}
		if(majY!=ancienY){
			kv = new KeyValue(robot3D.translateYProperty(), majY*Cases.SIZE);
			kf = new KeyFrame(Duration.millis(2000), kv);
			timeline.getKeyFrames().add(kf);
			robot3D.setTranslateY(robot3D.getTranslateY() + ((majY-ancienY)*Cases.SIZE));
		}
		if(majZ!=ancienZ){
			kv = new KeyValue(robot3D.translateZProperty(), majZ*Cases.SIZE);
			kf = new KeyFrame(Duration.millis(2000), kv);
			timeline.getKeyFrames().add(kf);
			robot3D.setTranslateZ(robot3D.getTranslateZ() + ((majZ-ancienZ)*Cases.SIZE));
		}
		
		System.out.println(""+majX);
		System.out.println(""+majY);
		System.out.println(""+majZ);
		
		
		
		
		
		switch(LaDirection.getCap()){
		case Nord:
		{
			//robot3D.setRotate(90);
			break;
		}
		case Ouest:
		{
			//robot3D.setRotate(0);
			break;
		}
		case Est:
		{
			//robot3D.setRotate(180);
			break;
		}
		default:
			//robot3D.setRotate(-90);
		}
		
		return timeline;
	}
	
	public Transition initRobot(){
		setPosition(posix,posiy,nb);
		LaDirection = new Direction(La1stDirection.getCap());
		
		ParallelTransition p = new ParallelTransition();
		
		TranslateTransition t = new TranslateTransition(Duration.millis(500),robot3D);
		
		t.setToX((posix*Cases.SIZE));
		t.setToY(tailler-(nb*Cases.SIZE));
		t.setToZ((posiy*Cases.SIZE));
		
		RotateTransition rt = new RotateTransition(Duration.millis(500),robot3D);
		
		switch(La1stDirection.getCap()){
		case Nord:
		{
			rt.setToAngle(90);
			break;
		}
		case Ouest:
		{
			rt.setToAngle(0);
			break;
		}
		case Est:
		{
			rt.setToAngle(180);
			break;
		}
		default:
			rt.setToAngle(-90);
		}
		
		p.getChildren().addAll(t, rt);
		
		return p;
	}
	
	public Group getRobot3D(){
		return robot3D;
	}
	
	
	
	public double tailleR(){
		return tailler;
	}

	public void setSphere(Sphere s) {
		this.sphere = s;
	}

	public Sphere getSphere() {
		return this.sphere;
	}
	
}

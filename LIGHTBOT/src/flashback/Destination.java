package flashback;

import java.util.LinkedList;

import affichage.AffichagePartie;
import cases.Cases;
import javafx.animation.FadeTransition;
import javafx.animation.ParallelTransition;
import javafx.animation.SequentialTransition;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Sphere;
import javafx.util.Duration;
import lightbotException.OutOfBoundBot;
import main.Main;
import ordre_bot.Ordre_bot;
import robot.Robot;
import world.World;

public class Destination extends Ordre_bot{
	public Destination(Color c){
		clr = c;
		code = 0;
	}
	
	@Override
	public void run_exe(Robot bot) throws OutOfBoundBot {
		if(bot.samePosBalise()){
			bot.takeTP();
			
			Sphere s = bot.getSphere();
			
			FadeTransition f = new FadeTransition(Duration.millis(500),s);
			f.setFromValue(100.0);
			f.setToValue(0.0);
			
			f.setOnFinished(new EventHandler<ActionEvent>(){
	            @Override
	            public void handle(ActionEvent arg0) {
	            	AffichagePartie.getField().getChildren().remove(s);
	            }
	        });
			
			Main.addT(bot.getRobotId(), f);
		}
		else{
			if(bot.canInitTP()){
				bot.initTP();
				
				Sphere s = new Sphere(50);
				
				World.listSphere.add(s);
				
				s.setOpacity(0.0);
				
				PhongMaterial material = new PhongMaterial(Color.GOLD, null, null, null, null);
				s.setMaterial(material);
				
				s.setTranslateX(bot.getPosition().get_x()*Cases.SIZE);
				s.setTranslateY((-bot.getPosition().get_z()*Cases.SIZE)+70);
				s.setTranslateZ(bot.getPosition().get_y()*Cases.SIZE);
				
				Platform.runLater(() -> AffichagePartie.getField().getChildren().add(s));
				
				FadeTransition f = new FadeTransition(Duration.millis(500),s);
				f.setFromValue(0.0);
				f.setToValue(100.0);
				
				bot.setSphere(s);
				
				Main.addT(bot.getRobotId(), f);
			}
		}
	}
}
package flashback;

import java.util.LinkedList;

import javafx.scene.paint.Color;
import lightbotException.OutOfBoundBot;
import main.Main;
import ordre_bot.Ordre_bot;
import robot.Robot;

public class Flashback extends Ordre_bot{
	public Flashback(Color c){
		clr = c;
		code = 0;
	}
	
	@Override
	public void run_exe(Robot bot) throws OutOfBoundBot {
		if(!bot.canInitTP()){
			Main.addT(bot.getRobotId(), bot.runTP());
		}
	}
}

package ordre_nombre;

import main.Main;
import javafx.animation.Transition;
import javafx.animation.TranslateTransition;
import javafx.scene.paint.Color;
import javafx.util.Duration;
import ordre_bot.Ordre_bot;
import robot.Robot;

public class Nombre extends Ordre_bot{
	public Nombre(Color c, int nb){
		clr = Color.CYAN;
		if(nb<1){
			code = 1;
		}
		else{
			code = nb;
		}
	}
	
	public Nombre(int nb){
		clr = Color.CYAN;
		if(nb<1){
			code = 1;
		}
		else{
			code = nb;
		}
	}
	
	public void run_exe(Robot bot) {
		Main.addT(bot.getRobotId(), new TranslateTransition(Duration.millis(500),bot.getRobot3D()));
	}
}
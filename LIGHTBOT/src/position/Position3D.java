package position;

public class Position3D extends Position2D{
	private int z;
	
	public Position3D(int x, int y, int z){
		super(x, y);
		this.z = z;
	}
	
	public Position3D(Position3D p){
		super(p.get_x(), p.get_y());
		this.z = p.get_z();
	}
	
	public int get_z(){
		return z;
	}
	
	public void set_z(int z){
		this.z = z;
	}
	
	public boolean equals(Position3D p){
		return this.x == p.get_x() && this.y == p.get_y() && this.z == p.get_z();
	}
}

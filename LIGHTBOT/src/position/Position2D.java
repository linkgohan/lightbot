package position;

public class Position2D {
	protected int x;
	protected int y;
	
	public Position2D(int x, int y){
		this.x = x;
		this.y = y;
	}
	
	public int get_x(){
		return x;
	}
	
	public int get_y(){
		return y;
	}
	
	public void set_x(int x){
		this.x = x;
	}
	
	public void set_y(int y){
		this.y = y;
	}
}

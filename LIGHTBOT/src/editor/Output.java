package editor;

import item.*;

import java.io.*;
import java.util.LinkedList;

import org.jdom2.*;
import org.jdom2.output.*;

import cases.*;
import direction.Direction;
import robot.Robot;

public class Output{
   //Nous allons commencer notre arborescence en cr�ant la racine XML
   //qui sera ici "personnes".
	private Element map = new Element("map");

   //On cr�e un nouveau Document JDOM bas� sur la racine que l'on vient de cr�er
   private org.jdom2.Document document = new Document(map);
   
   private LinkedList<Robot> listbot = new LinkedList<Robot>();
   private Cases Plateau[][];
   private int nb_col;
   private int nb_lig;
   
   String fichier;
   
   public Output(String fichier, int nb_lig, int nb_col){
	   this.fichier = fichier;
	   this.nb_col = nb_col;
	   this.nb_lig = nb_lig;
	   
	   Plateau = new Cases[nb_lig][nb_col];
	   
	   for(int i=0; i<nb_lig;i++){
		   for(int j=0; j<nb_col;j++){
			   Plateau[i][j] = new CasesPleine(i, j, 1, new Inerte());
		   }
	   }
   }
   
   public Cases getCases(int i, int j){
	   return Plateau[i][j];
   }

   public void save()
   {  
      //On cr�e un nouvel Element etudiant et on l'ajoute
      //en tant qu'Element de racine
      Element taille = new Element("taille");
      Attribute colonne = new Attribute("colonne",""+nb_col);
      Attribute ligne = new Attribute("ligne",""+nb_lig);
      taille.setAttribute(colonne);
      taille.setAttribute(ligne);
      map.addContent(taille);
      
      Element robot;
      Attribute x;
      Attribute y;
      Attribute z;
      Attribute cap;
      
      for(Robot r:listbot){
    	  robot = new Element("robot");
		  x = new Attribute("x",""+r.getPosition().get_x());
		  y = new Attribute("y",""+r.getPosition().get_y());
		  z = new Attribute("z",""+r.getPosition().get_z());
		  cap = new Attribute("cap",Direction.capToString(r.getDirection().getCap()));
		  robot.setAttribute(x);
		  robot.setAttribute(y);
		  robot.setAttribute(z);
		  robot.setAttribute(cap);
		  map.addContent(robot);
	   }
      
      Element Lacase;
      Attribute type;
      
      Cases lacase;
      
      for(int i=0; i<nb_lig; i++){
    	  for(int j=0; j<nb_col; j++){
    		  lacase = Plateau[i][j];
    		  
    		  Lacase = new Element("CasesPleine");
    		  type = new Attribute("type","Inerte");
    		  Lacase.setAttribute(type);
    		  x = new Attribute("x",""+i);
    		  Lacase.setAttribute(x);
    		  y = new Attribute("y",""+j);
    		  Lacase.setAttribute(y);
    		  z = new Attribute("z","1");
    		  Lacase.setAttribute(z);
    		 
    	      
    	      if(lacase.nom().equals("CasesVide")){
    	    	  Lacase = new Element("CasesVide");
        		  x = new Attribute("x",""+i);
        		  Lacase.setAttribute(x);
        		  y = new Attribute("y",""+j);
        		  Lacase.setAttribute(y);
        		  z = new Attribute("z","0");
        		  Lacase.setAttribute(z);
    	      }
    	      else{
    	    	  ItemsStatique obj = (((CasesPleine) lacase).getObjet());
    	    	  String Type_vers = obj.getNom();
    	    	  int h = lacase.getPos().get_z();
    	    	  
    	    	  Lacase = new Element("CasesPleine");
		        	
	        		if(Type_vers.equals("Inerte")){
	          		  	type = new Attribute("type","Inerte");
	  				}
	        		else if(Type_vers.equals("Peignable")){
	          		  	type = new Attribute("type","Peignable");	
	  				}
	        		else if(Type_vers.equals("Teleport")){
	          		  	type = new Attribute("type","Teleport");
	        			int x_vers = ((Teleport)obj).get_xvers();
	        			int y_vers = ((Teleport)obj).get_yvers();
	        			int z_vers = ((Teleport)obj).get_zvers();
	        			
	        			Attribute ax = new Attribute("x_vers",""+x_vers);
	        			Lacase.setAttribute(ax);	
	        		    Attribute ay = new Attribute("y_vers",""+y_vers);
	        		    Lacase.setAttribute(ay);
	        		    Attribute az = new Attribute("z_vers",""+z_vers);
	        		    Lacase.setAttribute(az);
	        		}
	        		Lacase.setAttribute(type);
	          		x = new Attribute("x",""+i);
	          		Lacase.setAttribute(x);
	          		y = new Attribute("y",""+j);
	          		Lacase.setAttribute(y);
	          		z = new Attribute("z",""+h);
	          		Lacase.setAttribute(z);
	        	}
    	      
    	      map.addContent(Lacase);
          }
      }

      //Les deux m�thodes qui suivent seront d�finies plus loin dans l'article
      //affiche();
      enregistre(fichier);
   }
   
   void affiche()
   {
      try
      {
         //On utilise ici un affichage classique avec getPrettyFormat()
         XMLOutputter sortie = new XMLOutputter(Format.getPrettyFormat());
         sortie.output(document, System.out);
      }
      catch (java.io.IOException e){}
   }

   void enregistre(String fichier)
   {
      try
      {
         //On utilise ici un affichage classique avec getPrettyFormat()
         XMLOutputter sortie = new XMLOutputter(Format.getPrettyFormat());
         //Remarquez qu'il suffit simplement de cr�er une instance de FileOutputStream
         //avec en argument le nom du fichier pour effectuer la s�rialisation.
         sortie.output(document, new FileOutputStream(fichier));
      }
      catch (java.io.IOException e){}
   }
   
   public void set_Cases(Cases lacase, int x, int y){
	   Plateau[x][y] = lacase;
   }
   
   public boolean CasesValide(int x, int y){
	   if(x>=0 && x<=(nb_lig-1) && y>=0 && y<=(nb_col-1)){
		   return !((Plateau[x][y].nom()).equals("CasesVide"));
	   }
	   return false;
   }
   
   public boolean VerifyAllCases(int x, int y){
	   String nom;
	   int x_o;
	   int y_o;
	   
	   for(Cases[] tc : Plateau){
		   for(Cases c : tc){
			   if((c.nom()).equals("CasesPleine")){
				   nom = (((CasesPleine)c).getObjet()).getNom();
				   if(nom.equals("Teleport")){
					   x_o = ((Teleport) ((CasesPleine)c).getObjet()).get_xvers();
					   y_o = ((Teleport) ((CasesPleine)c).getObjet()).get_yvers();
					   
					   if(x == x_o && y == y_o){
						   return false;
					   }
				   }
			   }
		   }
	   }
	   
	   return true;
   }
   
   public boolean testBotExist(int x, int y){
	   for(Robot r:listbot){
		   if(r.getPosition().get_x() == x && r.getPosition().get_y() == y){
			   return true;
		   }
	   }
	   return false;
   }
   
   public void suppBot(int x, int y){
	   for(Robot r:listbot){
		   if(r.getPosition().get_x() == x && r.getPosition().get_y() == y){
			   listbot.remove(r);
			   break;
		   }
	   }
   }
   
   public void addBot(int x, int y, Direction.capType monCap){
	   suppBot(x, y);
	   listbot.add(new Robot(x, y, Plateau[x][y].getPos().get_z(), monCap, 0));
   }
   
   public String capBot(int x, int y){
	   for(Robot r:listbot){
		   if(r.getPosition().get_x() == x && r.getPosition().get_y() == y){
			   switch(r.getDirection().getCap()){
				case Nord:
				{
					return "NORD";
				}
				case Ouest:
				{
					return "OUEST";
				}
				case Est:
				{
					return "EST";
				}
				case Sud:
				{
					return "SUD";
				}
				default:
					System.out.println("erreur direction");
				}
		   }
	   }
	return "erreur";
   }
   
   public boolean existBot(){
	   return !(listbot.isEmpty());
   }
}

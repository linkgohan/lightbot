package ordre_break;

import main.Main;
import javafx.animation.Transition;
import javafx.animation.TranslateTransition;
import javafx.scene.paint.Color;
import javafx.util.Duration;
import ordre_bot.Ordre_bot;
import robot.Robot;

public class Break extends Ordre_bot{
	public Break(Color c){
		clr = c;
		code = -1;
	}
	
	public Break(){
		clr = Color.CYAN;
	}
	
	public void run_exe(Robot bot) {
		Main.addT(bot.getRobotId(), new TranslateTransition(Duration.millis(500),bot.getRobot3D()));
	}
}

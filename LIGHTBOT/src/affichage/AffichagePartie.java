package affichage;

import java.io.File;
import java.util.Optional;

import javafx.animation.RotateTransition;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Camera;
import javafx.scene.Cursor;
import javafx.scene.DepthTest;
import javafx.scene.Group;
import javafx.scene.PerspectiveCamera;
import javafx.scene.Scene;
import javafx.scene.SceneAntialiasing;
import javafx.scene.SubScene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaPlayer.Status;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.transform.Rotate;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.util.Duration;
import lightbotException.OutOfBoundBot;
import main.Main;
import mouvement.Avancer;
import mouvement.Sauter;
import mouvement.TournerDroite;
import mouvement.TournerGauche;
import ordre_bot.Ordre_bot;
import ordre_break.Break;
import ordre_for.For;
import ordre_nombre.Nombre;
import world.World;
import actions.Activer;
import cases.Cases;
import flashback.Destination;
import flashback.Flashback;

public class AffichagePartie extends Application {

	private static int NbRobots;
	private int deltaCount = 0;
	private final double DEFAULT_ZOOM = 1.0;
	private DoubleProperty zoomMax = new SimpleDoubleProperty(10.0);
	private DoubleProperty zoomMin = new SimpleDoubleProperty(0.1);
	private DoubleProperty zoomDelta = new SimpleDoubleProperty(1.2);
	private DoubleProperty zoom = new SimpleDoubleProperty(DEFAULT_ZOOM);
	double centreX = Cases.SIZE * 0.5 * World.getLig();
	double centreZ = Cases.SIZE * 0.5 * World.getCol();
	double centreY = Cases.SIZE*2.5;
	private Color couleurPreselec = Color.CYAN;
	private int nbMainActions = 12;
	private int nbPActions = 8;
	private static Group TheField;
	private static Button boutonAColorier;
	int nb_mainFocus = 0;
	int nb_p1Focus = 0;
	int nb_p2Focus = 0;
	int nbActionParLigne = 4;

	boolean mainFocus = false;
	boolean p1Focus = false;
	boolean p2Focus = false;
	
	public static void gagner(){
		boutonAColorier.setStyle("-fx-base: #"+Color.GREEN.toString().split("x")[1]+";");
	}
	
	public static void perdu(){
		boutonAColorier.setStyle("-fx-base: #"+Color.RED.toString().split("x")[1]+";");
	}

	public static BorderPane createField(int COTE, int SIZE){

		//Noeud principal pour placer field en bottom
		BorderPane root = new BorderPane();
		//Noeud qui récupère le terrain
		Group field = new Group();
		//Initialisation du terrain
		Group grp;
		for(int j=World.getCol() -1; j>=0; j--){
			for(int i=0; i<=World.getLig() -1; i++){
				grp = World.getBox(i, j);
				field.getChildren().add(grp);
			}
		}
		for(int i = 0; i<NbRobots; i++){
			field.getChildren().add(World.getRobot(i).getRobot3D());
		}
		root.setBottom(field);
		
		TheField = field;
		
		return	root;
	}

	public void start(final Stage primaryStage) { 
		NbRobots = World.getNbRobot();
		
		primaryStage.getIcons().setAll(new Image(getClass().getResource("Baymax_head.png").toExternalForm()));

		primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() { 
		    @Override 
		    public void handle(WindowEvent event) { 
		        event.consume(); 
		    } 
		});
		
		
		int COTE = 3;
		int SIZE=200;
		int ROTATION = 45;
		final int TRANSFORMATION = 30;

		//
		double winWidth = Screen.getPrimary().getVisualBounds().getWidth();
		double winHeight = Screen.getPrimary().getVisualBounds().getHeight();
		//
		//Noeud de la liste des couleurs possibles
		double listeActionsWidth = 2*winWidth/3;
		double listeActionsHeight = (winHeight/4) + (winHeight/25);
		HBox couleurActions = new HBox(); 
		int nb_couleurs = 3;
		int tailleCouleur = 25;
		final double actionSize = winWidth/30;
		final double buttonSize = actionSize*1.3;
		
		//Noeud racine
		final GridPane root = new GridPane(); 
		root.setGridLinesVisible(true);
		//Noeud du terrain
		for(int i = 0; i<NbRobots; i++){
			World.getRobot(i).createBaymax();
		}
		final BorderPane field = createField(COTE, SIZE);
		//
		//Subcene de la liste d'actions avec couleurs
		//Subscene Main et Procedures
		final TilePane listeMain = new TilePane(); 
		listeMain.setMaxWidth(6*actionSize);
		listeMain.setPrefTileWidth(buttonSize);
		listeMain.setPrefTileHeight(buttonSize);
		final TilePane listeP1 = new TilePane(); 
		listeP1.setMaxWidth(6*actionSize);
		listeP1.setPrefTileWidth(buttonSize);
		listeP1.setPrefTileHeight(buttonSize);
		final TilePane listeP2 = new TilePane();
		listeP2.setMaxWidth(6*actionSize);
		listeP2.setPrefTileWidth(buttonSize);
		listeP2.setPrefTileHeight(buttonSize);
		
		Label textMain = new Label("MAIN");
		textMain.setTextFill(Color.WHITE);
		textMain.setTranslateX((nbActionParLigne+1)*actionSize/2);
		EventHandler<MouseEvent> eventMain = new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				if(nb_mainFocus < 12){
					mainFocus = true;
					p1Focus = false;
					p2Focus = false;
				}
				else{
					mainFocus = false;
				}
				if(nb_mainFocus < 12){
					listeMain.getChildren().get(nb_mainFocus).setStyle("-fx-background-color:green;");
				}
				if(nb_p1Focus < 8){
					listeP1.getChildren().get(nb_p1Focus).setStyle("");
				}
				if(nb_p2Focus < 8){
					listeP2.getChildren().get(nb_p2Focus).setStyle("");
				}
			}
		};
		textMain.setOnMouseClicked(eventMain);	
		
		Label textP1 = new Label("P1");
		textP1.setTextFill(Color.WHITE);

		textP1.setTranslateX((nbActionParLigne+1)*actionSize/2);

		EventHandler<MouseEvent> eventP1 = new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				if(nb_p1Focus < 8){
					mainFocus = false;
					p2Focus = false;
					p1Focus = true;
				}
				else{
					p1Focus = false;
				}
				if(nb_mainFocus < 12){
					listeMain.getChildren().get(nb_mainFocus).setStyle("");
				}
				if(nb_p1Focus < 8){
					listeP1.getChildren().get(nb_p1Focus).setStyle("-fx-background-color:green;");
				}
				if(nb_p2Focus < 8){
					listeP2.getChildren().get(nb_p2Focus).setStyle("");
				}
			}
		};
		textP1.setOnMouseClicked(eventP1);
		
		Label textP2 = new Label("P2");
		textP2.setTextFill(Color.WHITE);

		textP2.setTranslateX((nbActionParLigne+1)*actionSize/2);

		EventHandler<MouseEvent> eventP2 = new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				if(nb_p2Focus < 8){
					mainFocus = false;
					p1Focus = false;
					p2Focus = true;
				}
				else{
					p2Focus = false;
				}
				if(nb_mainFocus < 12){
					listeMain.getChildren().get(nb_mainFocus).setStyle("");
				}
				if(nb_p1Focus < 8){
					listeP1.getChildren().get(nb_p1Focus).setStyle("");
				}
				if(nb_p2Focus < 8){
					listeP2.getChildren().get(nb_p2Focus).setStyle("-fx-background-color:green;");
				}
			}
		};
		textP2.setOnMouseClicked(eventP2);
		
		//Noeud de la liste d'actions de couleurs possibles
		HBox listeActions = new HBox();
		
		listeActions.setCursor(Cursor.OPEN_HAND);
		listeActions.setMaxWidth(6*actionSize);
		listeActions.setPrefWidth(buttonSize);
		listeActions.setPrefHeight(buttonSize);
		

		final Button boutonAvancer = new Button();
		final Image image = new Image("./affichage/image_avancer.png"); 
		final ImageView imageView = new ImageView(image); 
		imageView.setFitWidth(actionSize); 
		imageView.setFitHeight(actionSize); 
		boutonAvancer.setGraphic(imageView);
		boutonAvancer.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");

		EventHandler<MouseEvent> eventBoutonAvancer = new EventHandler<MouseEvent>() {
			public void handle(MouseEvent event) {
				final Ordre_bot o1 = new Avancer(couleurPreselec);
				final Ordre_bot o2 = new Avancer(couleurPreselec);
				final Image imageSpe = new Image("./affichage/image_avancer.png"); 
				final ImageView imageViewSpe = new ImageView(imageSpe);
				imageViewSpe.setFitWidth(actionSize); 
				imageViewSpe.setFitHeight(actionSize); 
				if(mainFocus == true){
					if(nb_mainFocus < 12){
						final Button b = ((Button) listeMain.getChildren().get(nb_mainFocus));
						
						b.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");
						b.setGraphic(imageViewSpe);
						
						EventHandler<MouseEvent> eventActionMain = new EventHandler<MouseEvent>() {
							
							@Override
							public void handle(MouseEvent event) {
								Main.suppMainR1Ordre(o1);
								if(NbRobots == 2){
									Main.suppMainR2Ordre(o2);
								}
								listeMain.getChildren().remove(b);
								if(nb_mainFocus >0)
									nb_mainFocus--;
								Button bouton = new Button();
								bouton.setMaxSize(buttonSize, buttonSize);
								listeMain.getChildren().addAll(bouton);
							}
						};
						b.setOnMouseClicked(eventActionMain);
						Main.addMainR1Ordre(o1);
						if(NbRobots == 2){
							Main.addMainR2Ordre(o2);
						}
						
						nb_mainFocus++;
					}
				}
				if(p1Focus == true){
					if(nb_p1Focus < 8){
						final Button b = ((Button) listeP1.getChildren().get(nb_p1Focus));
						
						b.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");
						b.setGraphic(imageViewSpe);
						
						EventHandler<MouseEvent> eventActionMain = new EventHandler<MouseEvent>() {
							
							@Override
							public void handle(MouseEvent event) {
								Main.suppP1R1Ordre(o1);
								if(NbRobots == 2){
									Main.suppP1R2Ordre(o2);
								}
								listeP1.getChildren().remove(b);
								if(nb_p1Focus >0)
									nb_p1Focus--;
								Button bouton = new Button();
								bouton.setMaxSize(buttonSize, buttonSize);
								listeP1.getChildren().addAll(bouton);
							}
						};
						b.setOnMouseClicked(eventActionMain);
						
						nb_p1Focus++;
						Main.addP1R1Ordre(o1);
						if(NbRobots == 2){
							Main.addP1R2Ordre(o2);
						}
					}
				}
				if(p2Focus == true){
					if(nb_p2Focus < 8){
						final Button b = ((Button) listeP2.getChildren().get(nb_p2Focus));
						
						b.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");
						b.setGraphic(imageViewSpe);
						
						EventHandler<MouseEvent> eventActionMain = new EventHandler<MouseEvent>() {
							
							@Override
							public void handle(MouseEvent event) {
								Main.suppP2R1Ordre(o1);
								if(NbRobots == 2){
									Main.suppP2R2Ordre(o2);
								}
								listeP2.getChildren().remove(b);
								if(nb_p2Focus >0)
									nb_p2Focus--;
								Button bouton = new Button();
								bouton.setMaxSize(buttonSize, buttonSize);
								listeP2.getChildren().addAll(bouton);
							}
						};
						b.setOnMouseClicked(eventActionMain);
						
						nb_p2Focus++;
						Main.addP2R1Ordre(o1);
						if(NbRobots == 2){
							Main.addP2R2Ordre(o2);
						}
					}
				}
			}
		};
		boutonAvancer.setOnMouseClicked(eventBoutonAvancer);
				
		final Button boutonTournerG = new Button();
		final Image image2 = new Image("./affichage/image_tournerG.png"); 
		final ImageView imageView2 = new ImageView(image2); 
		imageView2.setFitWidth(actionSize); 
		imageView2.setFitHeight(actionSize); 
		boutonTournerG.setGraphic(imageView2);
		boutonTournerG.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");

		EventHandler<MouseEvent> eventBoutonTournerG = new EventHandler<MouseEvent>() {
			public void handle(MouseEvent event) {
				final Ordre_bot o1 = new TournerGauche(couleurPreselec);
				final Ordre_bot o2 = new TournerGauche(couleurPreselec);
				final Image imageSpe = new Image("./affichage/image_tournerG.png"); 
				final ImageView imageViewSpe = new ImageView(imageSpe);
				imageViewSpe.setFitWidth(actionSize); 
				imageViewSpe.setFitHeight(actionSize); 
				if(mainFocus == true){
					if(nb_mainFocus < 12){
						final Button b = ((Button) listeMain.getChildren().get(nb_mainFocus));
						
						b.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");
						b.setGraphic(imageViewSpe);
						
						EventHandler<MouseEvent> eventActionMain = new EventHandler<MouseEvent>() {
							
							@Override
							public void handle(MouseEvent event) {
								Main.suppMainR1Ordre(o1);
								if(NbRobots == 2){
									Main.suppMainR2Ordre(o2);
								}
								listeMain.getChildren().remove(b);
								if(nb_mainFocus >0)
									nb_mainFocus--;
								Button bouton = new Button();
								bouton.setMaxSize(buttonSize, buttonSize);
								listeMain.getChildren().addAll(bouton);
							}
						};
						b.setOnMouseClicked(eventActionMain);
						Main.addMainR1Ordre(o1);
						if(NbRobots == 2){
							Main.addMainR2Ordre(o2);
						}
						
						nb_mainFocus++;
					}
				}
				if(p1Focus == true){
					if(nb_p1Focus < 8){
						final Button b = ((Button) listeP1.getChildren().get(nb_p1Focus));
						
						b.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");
						b.setGraphic(imageViewSpe);
						
						EventHandler<MouseEvent> eventActionMain = new EventHandler<MouseEvent>() {
							
							@Override
							public void handle(MouseEvent event) {
								Main.suppP1R1Ordre(o1);
								if(NbRobots == 2){
									Main.suppP1R2Ordre(o2);
								}
								listeP1.getChildren().remove(b);
								if(nb_p1Focus >0)
									nb_p1Focus--;
								Button bouton = new Button();
								bouton.setMaxSize(buttonSize, buttonSize);
								listeP1.getChildren().addAll(bouton);
							}
						};
						b.setOnMouseClicked(eventActionMain);
						
						nb_p1Focus++;
						Main.addP1R1Ordre(o1);
						if(NbRobots == 2){
							Main.addP1R2Ordre(o2);
						}
					}
				}
				if(p2Focus == true){
					if(nb_p2Focus < 8){
						final Button b = ((Button) listeP2.getChildren().get(nb_p2Focus));
						
						b.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");
						b.setGraphic(imageViewSpe);
						
						EventHandler<MouseEvent> eventActionMain = new EventHandler<MouseEvent>() {
							
							@Override
							public void handle(MouseEvent event) {
								Main.suppP2R1Ordre(o1);
								if(NbRobots == 2){
									Main.suppP2R2Ordre(o2);
								}
								listeP2.getChildren().remove(b);
								if(nb_p2Focus >0)
									nb_p2Focus--;
								Button bouton = new Button();
								bouton.setMaxSize(buttonSize, buttonSize);
								listeP2.getChildren().addAll(bouton);
							}
						};
						b.setOnMouseClicked(eventActionMain);
						
						nb_p2Focus++;
						Main.addP2R1Ordre(o1);
						if(NbRobots == 2){
							Main.addP2R2Ordre(o2);
						}
					}
				}
			}
		};
		boutonTournerG.setOnMouseClicked(eventBoutonTournerG);
		//
		final Button boutonTournerD = new Button();
		final Image image3 = new Image("./affichage/image_tournerD.png"); 
		final ImageView imageView3 = new ImageView(image3); 
		imageView3.setFitWidth(actionSize); 
		imageView3.setFitHeight(actionSize); 
		boutonTournerD.setGraphic(imageView3);
		boutonTournerD.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");

		EventHandler<MouseEvent> eventBoutonTournerD = new EventHandler<MouseEvent>() {
			public void handle(MouseEvent event) {
				final Ordre_bot o1 = new TournerDroite(couleurPreselec);
				final Ordre_bot o2 = new TournerDroite(couleurPreselec);
				final Image imageSpe = new Image("./affichage/image_tournerD.png"); 
				final ImageView imageViewSpe = new ImageView(imageSpe);
				imageViewSpe.setFitWidth(actionSize); 
				imageViewSpe.setFitHeight(actionSize);
				if(mainFocus == true){
					if(nb_mainFocus < 12){
						final Button b = ((Button) listeMain.getChildren().get(nb_mainFocus));
						
						b.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");
						b.setGraphic(imageViewSpe);
						
						EventHandler<MouseEvent> eventActionMain = new EventHandler<MouseEvent>() {
							
							@Override
							public void handle(MouseEvent event) {
								Main.suppMainR1Ordre(o1);
								if(NbRobots == 2){
									Main.suppMainR2Ordre(o2);
								}
								listeMain.getChildren().remove(b);
								if(nb_mainFocus >0)
									nb_mainFocus--;
								Button bouton = new Button();
								bouton.setMaxSize(buttonSize, buttonSize);
								listeMain.getChildren().addAll(bouton);
							}
						};
						b.setOnMouseClicked(eventActionMain);
						Main.addMainR1Ordre(o1);
						if(NbRobots == 2){
							Main.addMainR2Ordre(o2);
						}
						
						nb_mainFocus++;
					}
				}
				if(p1Focus == true){
					if(nb_p1Focus < 8){
						final Button b = ((Button) listeP1.getChildren().get(nb_p1Focus));
						
						b.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");
						b.setGraphic(imageViewSpe);
						
						EventHandler<MouseEvent> eventActionMain = new EventHandler<MouseEvent>() {
							
							@Override
							public void handle(MouseEvent event) {
								Main.suppP1R1Ordre(o1);
								if(NbRobots == 2){
									Main.suppP1R2Ordre(o2);
								}
								listeP1.getChildren().remove(b);
								if(nb_p1Focus >0)
									nb_p1Focus--;
								Button bouton = new Button();
								bouton.setMaxSize(buttonSize, buttonSize);
								listeP1.getChildren().addAll(bouton);
							}
						};
						b.setOnMouseClicked(eventActionMain);
						
						nb_p1Focus++;
						Main.addP1R1Ordre(o1);
						if(NbRobots == 2){
							Main.addP1R2Ordre(o2);
						}
					}
				}
				if(p2Focus == true){
					if(nb_p2Focus < 8){
						final Button b = ((Button) listeP2.getChildren().get(nb_p2Focus));
						
						b.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");
						b.setGraphic(imageViewSpe);
						
						EventHandler<MouseEvent> eventActionMain = new EventHandler<MouseEvent>() {
							
							@Override
							public void handle(MouseEvent event) {
								Main.suppP2R1Ordre(o1);
								if(NbRobots == 2){
									Main.suppP2R2Ordre(o2);
								}
								listeP2.getChildren().remove(b);
								if(nb_p2Focus >0)
									nb_p2Focus--;
								Button bouton = new Button();
								bouton.setMaxSize(buttonSize, buttonSize);
								listeP2.getChildren().addAll(bouton);
							}
						};
						b.setOnMouseClicked(eventActionMain);
						
						nb_p2Focus++;
						Main.addP2R1Ordre(o1);
						if(NbRobots == 2){
							Main.addP2R2Ordre(o2);
						}
					}
				}
			}
		};
		boutonTournerD.setOnMouseClicked(eventBoutonTournerD);
		//
		final Button boutonActiver = new Button();
		final Image image4 = new Image("./affichage/image_activer.png"); 
		final ImageView imageView4 = new ImageView(image4); 
		imageView4.setFitWidth(actionSize); 
		imageView4.setFitHeight(actionSize); 
		boutonActiver.setGraphic(imageView4);
		boutonActiver.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");

		EventHandler<MouseEvent> eventBoutonActiver = new EventHandler<MouseEvent>() {
			public void handle(MouseEvent event) {
				final Ordre_bot o1 = new Activer(couleurPreselec);
				final Ordre_bot o2 = new Activer(couleurPreselec);
				final Image imageSpe = new Image("./affichage/image_activer.png"); 
				final ImageView imageViewSpe = new ImageView(imageSpe);
				imageViewSpe.setFitWidth(actionSize); 
				imageViewSpe.setFitHeight(actionSize); 
				if(mainFocus == true){
					if(nb_mainFocus < 12){
						final Button b = ((Button) listeMain.getChildren().get(nb_mainFocus));
						
						b.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");
						b.setGraphic(imageViewSpe);
						
						EventHandler<MouseEvent> eventActionMain = new EventHandler<MouseEvent>() {
							
							@Override
							public void handle(MouseEvent event) {
								Main.suppMainR1Ordre(o1);
								if(NbRobots == 2){
									Main.suppMainR2Ordre(o2);
								}
								listeMain.getChildren().remove(b);
								if(nb_mainFocus >0)
									nb_mainFocus--;
								Button bouton = new Button();
								bouton.setMaxSize(buttonSize, buttonSize);
								listeMain.getChildren().addAll(bouton);
							}
						};
						b.setOnMouseClicked(eventActionMain);
						Main.addMainR1Ordre(o1);
						if(NbRobots == 2){
							Main.addMainR2Ordre(o2);
						}
						
						nb_mainFocus++;
					}
				}
				if(p1Focus == true){
					if(nb_p1Focus < 8){
						final Button b = ((Button) listeP1.getChildren().get(nb_p1Focus));
						
						b.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");
						b.setGraphic(imageViewSpe);
						
						EventHandler<MouseEvent> eventActionMain = new EventHandler<MouseEvent>() {
							
							@Override
							public void handle(MouseEvent event) {
								Main.suppP1R1Ordre(o1);
								if(NbRobots == 2){
									Main.suppP1R2Ordre(o2);
								}
								listeP1.getChildren().remove(b);
								if(nb_p1Focus >0)
									nb_p1Focus--;
								Button bouton = new Button();
								bouton.setMaxSize(buttonSize, buttonSize);
								listeP1.getChildren().addAll(bouton);
							}
						};
						b.setOnMouseClicked(eventActionMain);
						
						nb_p1Focus++;
						Main.addP1R1Ordre(o1);
						if(NbRobots == 2){
							Main.addP1R2Ordre(o2);
						}
					}
				}
				if(p2Focus == true){
					if(nb_p2Focus < 8){
						final Button b = ((Button) listeP2.getChildren().get(nb_p2Focus));
						
						b.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");
						b.setGraphic(imageViewSpe);
						
						EventHandler<MouseEvent> eventActionMain = new EventHandler<MouseEvent>() {
							
							@Override
							public void handle(MouseEvent event) {
								Main.suppP2R1Ordre(o1);
								if(NbRobots == 2){
									Main.suppP2R2Ordre(o2);
								}
								listeP2.getChildren().remove(b);
								if(nb_p2Focus >0)
									nb_p2Focus--;
								Button bouton = new Button();
								bouton.setMaxSize(buttonSize, buttonSize);
								listeP2.getChildren().addAll(bouton);
							}
						};
						b.setOnMouseClicked(eventActionMain);
						
						nb_p2Focus++;
						Main.addP2R1Ordre(o1);
						if(NbRobots == 2){
							Main.addP2R2Ordre(o2);
						}
					}
				}
			}
		};
		boutonActiver.setOnMouseClicked(eventBoutonActiver);
		//
		final Button boutonJump = new Button();
		final Image image5 = new Image("./affichage/image_sauter.png"); 
		final ImageView imageView5 = new ImageView(image5); 
		imageView5.setFitWidth(actionSize); 
		imageView5.setFitHeight(actionSize); 
		boutonJump.setGraphic(imageView5);
		boutonJump.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");

		EventHandler<MouseEvent> eventBoutonJump = new EventHandler<MouseEvent>() {
			public void handle(MouseEvent event) {
				final Ordre_bot o1 = new Sauter(couleurPreselec);
				final Ordre_bot o2 = new Sauter(couleurPreselec);
				final Image imageSpe = new Image("./affichage/image_sauter.png"); 
				final ImageView imageViewSpe = new ImageView(imageSpe);
				imageViewSpe.setFitWidth(actionSize); 
				imageViewSpe.setFitHeight(actionSize); 
				if(mainFocus == true){
					if(nb_mainFocus < 12){
						final Button b = ((Button) listeMain.getChildren().get(nb_mainFocus));
						
						b.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");
						b.setGraphic(imageViewSpe);
						
						EventHandler<MouseEvent> eventActionMain = new EventHandler<MouseEvent>() {
							
							@Override
							public void handle(MouseEvent event) {
								Main.suppMainR1Ordre(o1);
								if(NbRobots == 2){
									Main.suppMainR2Ordre(o2);
								}
								listeMain.getChildren().remove(b);
								if(nb_mainFocus >0)
									nb_mainFocus--;
								Button bouton = new Button();
								bouton.setMaxSize(buttonSize, buttonSize);
								listeMain.getChildren().addAll(bouton);
							}
						};
						b.setOnMouseClicked(eventActionMain);
						Main.addMainR1Ordre(o1);
						if(NbRobots == 2){
							Main.addMainR2Ordre(o2);
						}
						
						nb_mainFocus++;
					}
				}
				if(p1Focus == true){
					if(nb_p1Focus < 8){
						final Button b = ((Button) listeP1.getChildren().get(nb_p1Focus));
						
						b.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");
						b.setGraphic(imageViewSpe);
						
						EventHandler<MouseEvent> eventActionMain = new EventHandler<MouseEvent>() {
							
							@Override
							public void handle(MouseEvent event) {
								Main.suppP1R1Ordre(o1);
								if(NbRobots == 2){
									Main.suppP1R2Ordre(o2);
								}
								listeP1.getChildren().remove(b);
								if(nb_p1Focus >0)
									nb_p1Focus--;
								Button bouton = new Button();
								bouton.setMaxSize(buttonSize, buttonSize);
								listeP1.getChildren().addAll(bouton);
							}
						};
						b.setOnMouseClicked(eventActionMain);
						
						nb_p1Focus++;
						Main.addP1R1Ordre(o1);
						if(NbRobots == 2){
							Main.addP1R2Ordre(o2);
						}
					}
				}
				if(p2Focus == true){
					if(nb_p2Focus < 8){
						final Button b = ((Button) listeP2.getChildren().get(nb_p2Focus));
						
						b.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");
						b.setGraphic(imageViewSpe);
						
						EventHandler<MouseEvent> eventActionMain = new EventHandler<MouseEvent>() {
							
							@Override
							public void handle(MouseEvent event) {
								Main.suppP2R1Ordre(o1);
								if(NbRobots == 2){
									Main.suppP2R2Ordre(o2);
								}
								listeP2.getChildren().remove(b);
								if(nb_p2Focus >0)
									nb_p2Focus--;
								Button bouton = new Button();
								bouton.setMaxSize(buttonSize, buttonSize);
								listeP2.getChildren().addAll(bouton);
							}
						};
						b.setOnMouseClicked(eventActionMain);
						
						nb_p2Focus++;
						Main.addP2R1Ordre(o1);
						if(NbRobots == 2){
							Main.addP2R2Ordre(o2);
						}
					}
				}
			}
		};
		boutonJump.setOnMouseClicked(eventBoutonJump);
		
		
		final Button boutonBr = new Button();
		final Image image6 = new Image("./affichage/image_break.png"); 
		final ImageView imageView6 = new ImageView(image6); 
		imageView6.setFitWidth(actionSize); 
		imageView6.setFitHeight(actionSize); 
		boutonBr.setGraphic(imageView6);
		boutonBr.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");

		EventHandler<MouseEvent> eventBoutonBr = new EventHandler<MouseEvent>() {
			public void handle(MouseEvent event) {
				final Ordre_bot o1 = new Break(couleurPreselec);
				final Ordre_bot o2 = new Break(couleurPreselec);
				final Image imageSpe = new Image("./affichage/image_break.png"); 
				final ImageView imageViewSpe = new ImageView(imageSpe);
				imageViewSpe.setFitWidth(actionSize); 
				imageViewSpe.setFitHeight(actionSize); 
				if(mainFocus == true){
					if(nb_mainFocus < 12){
						final Button b = ((Button) listeMain.getChildren().get(nb_mainFocus));
						
						b.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");
						b.setGraphic(imageViewSpe);
						
						EventHandler<MouseEvent> eventActionMain = new EventHandler<MouseEvent>() {
							
							@Override
							public void handle(MouseEvent event) {
								Main.suppMainR1Ordre(o1);
								if(NbRobots == 2){
									Main.suppMainR2Ordre(o2);
								}
								listeMain.getChildren().remove(b);
								if(nb_mainFocus >0)
									nb_mainFocus--;
								Button bouton = new Button();
								bouton.setMaxSize(buttonSize, buttonSize);
								listeMain.getChildren().addAll(bouton);
							}
						};
						b.setOnMouseClicked(eventActionMain);
						Main.addMainR1Ordre(o1);
						if(NbRobots == 2){
							Main.addMainR2Ordre(o2);
						}
						
						nb_mainFocus++;
					}
				}
				if(p1Focus == true){
					if(nb_p1Focus < 8){
						final Button b = ((Button) listeP1.getChildren().get(nb_p1Focus));
						
						b.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");
						b.setGraphic(imageViewSpe);
						
						EventHandler<MouseEvent> eventActionMain = new EventHandler<MouseEvent>() {
							
							@Override
							public void handle(MouseEvent event) {
								Main.suppP1R1Ordre(o1);
								if(NbRobots == 2){
									Main.suppP1R2Ordre(o2);
								}
								listeP1.getChildren().remove(b);
								if(nb_p1Focus >0)
									nb_p1Focus--;
								Button bouton = new Button();
								bouton.setMaxSize(buttonSize, buttonSize);
								listeP1.getChildren().addAll(bouton);
							}
						};
						b.setOnMouseClicked(eventActionMain);
						
						nb_p1Focus++;
						Main.addP1R1Ordre(o1);
						if(NbRobots == 2){
							Main.addP1R2Ordre(o2);
						}
					}
				}
				if(p2Focus == true){
					if(nb_p2Focus < 8){
						final Button b = ((Button) listeP2.getChildren().get(nb_p2Focus));
						
						b.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");
						b.setGraphic(imageViewSpe);
						
						EventHandler<MouseEvent> eventActionMain = new EventHandler<MouseEvent>() {
							
							@Override
							public void handle(MouseEvent event) {
								Main.suppP2R1Ordre(o1);
								if(NbRobots == 2){
									Main.suppP2R2Ordre(o2);
								}
								listeP2.getChildren().remove(b);
								if(nb_p2Focus >0)
									nb_p2Focus--;
								Button bouton = new Button();
								bouton.setMaxSize(buttonSize, buttonSize);
								listeP2.getChildren().addAll(bouton);
							}
						};
						b.setOnMouseClicked(eventActionMain);
						
						nb_p2Focus++;
						Main.addP2R1Ordre(o1);
						if(NbRobots == 2){
							Main.addP2R2Ordre(o2);
						}
					}
				}
			}
		};
		boutonBr.setOnMouseClicked(eventBoutonBr);
		
		
		final Button boutonDest = new Button();
		final Image image15 = new Image("./affichage/image_destination.png"); 
		final ImageView imageView15 = new ImageView(image15); 
		imageView15.setFitWidth(actionSize); 
		imageView15.setFitHeight(actionSize); 
		boutonDest.setGraphic(imageView15);
		boutonDest.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");

		EventHandler<MouseEvent> eventBoutonDest = new EventHandler<MouseEvent>() {
			public void handle(MouseEvent event) {
				final Ordre_bot o1 = new Destination(couleurPreselec);
				final Ordre_bot o2 = new Destination(couleurPreselec);
				final Image imageSpe = new Image("./affichage/image_destination.png"); 
				final ImageView imageViewSpe = new ImageView(imageSpe);
				imageViewSpe.setFitWidth(actionSize); 
				imageViewSpe.setFitHeight(actionSize); 
				if(mainFocus == true){
					if(nb_mainFocus < 12){
						final Button b = ((Button) listeMain.getChildren().get(nb_mainFocus));
						
						b.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");
						b.setGraphic(imageViewSpe);
						
						EventHandler<MouseEvent> eventActionMain = new EventHandler<MouseEvent>() {
							
							@Override
							public void handle(MouseEvent event) {
								Main.suppMainR1Ordre(o1);
								if(NbRobots == 2){
									Main.suppMainR2Ordre(o2);
								}
								listeMain.getChildren().remove(b);
								if(nb_mainFocus >0)
									nb_mainFocus--;
								Button bouton = new Button();
								bouton.setMaxSize(buttonSize, buttonSize);
								listeMain.getChildren().addAll(bouton);
							}
						};
						b.setOnMouseClicked(eventActionMain);
						Main.addMainR1Ordre(o1);
						if(NbRobots == 2){
							Main.addMainR2Ordre(o2);
						}
						
						nb_mainFocus++;
					}
				}
				if(p1Focus == true){
					if(nb_p1Focus < 8){
						final Button b = ((Button) listeP1.getChildren().get(nb_p1Focus));
						
						b.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");
						b.setGraphic(imageViewSpe);
						
						EventHandler<MouseEvent> eventActionMain = new EventHandler<MouseEvent>() {
							
							@Override
							public void handle(MouseEvent event) {
								Main.suppP1R1Ordre(o1);
								if(NbRobots == 2){
									Main.suppP1R2Ordre(o2);
								}
								listeP1.getChildren().remove(b);
								if(nb_p1Focus >0)
									nb_p1Focus--;
								Button bouton = new Button();
								bouton.setMaxSize(buttonSize, buttonSize);
								listeP1.getChildren().addAll(bouton);
							}
						};
						b.setOnMouseClicked(eventActionMain);
						
						nb_p1Focus++;
						Main.addP1R1Ordre(o1);
						if(NbRobots == 2){
							Main.addP1R2Ordre(o2);
						}
					}
				}
				if(p2Focus == true){
					if(nb_p2Focus < 8){
						final Button b = ((Button) listeP2.getChildren().get(nb_p2Focus));
						
						b.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");
						b.setGraphic(imageViewSpe);
						
						EventHandler<MouseEvent> eventActionMain = new EventHandler<MouseEvent>() {
							
							@Override
							public void handle(MouseEvent event) {
								Main.suppP2R1Ordre(o1);
								if(NbRobots == 2){
									Main.suppP2R2Ordre(o2);
								}
								listeP2.getChildren().remove(b);
								if(nb_p2Focus >0)
									nb_p2Focus--;
								Button bouton = new Button();
								bouton.setMaxSize(buttonSize, buttonSize);
								listeP2.getChildren().addAll(bouton);
							}
						};
						b.setOnMouseClicked(eventActionMain);
						
						nb_p2Focus++;
						Main.addP2R1Ordre(o1);
						if(NbRobots == 2){
							Main.addP2R2Ordre(o2);
						}
					}
				}
			}
		};
		boutonDest.setOnMouseClicked(eventBoutonDest);
		
		
		final Button boutonFlash = new Button();
		final Image image16 = new Image("./affichage/image_flashback.png"); 
		final ImageView imageView16 = new ImageView(image16); 
		imageView16.setFitWidth(actionSize); 
		imageView16.setFitHeight(actionSize); 
		boutonFlash.setGraphic(imageView16);
		boutonFlash.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");

		EventHandler<MouseEvent> eventBoutonFlash = new EventHandler<MouseEvent>() {
			public void handle(MouseEvent event) {
				final Ordre_bot o1 = new Flashback(couleurPreselec);
				final Ordre_bot o2 = new Flashback(couleurPreselec);
				final Image imageSpe = new Image("./affichage/image_flashback.png"); 
				final ImageView imageViewSpe = new ImageView(imageSpe);
				imageViewSpe.setFitWidth(actionSize); 
				imageViewSpe.setFitHeight(actionSize); 
				if(mainFocus == true){
					if(nb_mainFocus < 12){
						final Button b = ((Button) listeMain.getChildren().get(nb_mainFocus));
						
						b.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");
						b.setGraphic(imageViewSpe);
						
						EventHandler<MouseEvent> eventActionMain = new EventHandler<MouseEvent>() {
							
							@Override
							public void handle(MouseEvent event) {
								Main.suppMainR1Ordre(o1);
								if(NbRobots == 2){
									Main.suppMainR2Ordre(o2);
								}
								listeMain.getChildren().remove(b);
								if(nb_mainFocus >0)
									nb_mainFocus--;
								Button bouton = new Button();
								bouton.setMaxSize(buttonSize, buttonSize);
								listeMain.getChildren().addAll(bouton);
							}
						};
						b.setOnMouseClicked(eventActionMain);
						Main.addMainR1Ordre(o1);
						if(NbRobots == 2){
							Main.addMainR2Ordre(o2);
						}
						
						nb_mainFocus++;
					}
				}
				if(p1Focus == true){
					if(nb_p1Focus < 8){
						final Button b = ((Button) listeP1.getChildren().get(nb_p1Focus));
						
						b.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");
						b.setGraphic(imageViewSpe);
						
						EventHandler<MouseEvent> eventActionMain = new EventHandler<MouseEvent>() {
							
							@Override
							public void handle(MouseEvent event) {
								Main.suppP1R1Ordre(o1);
								if(NbRobots == 2){
									Main.suppP1R2Ordre(o2);
								}
								listeP1.getChildren().remove(b);
								if(nb_p1Focus >0)
									nb_p1Focus--;
								Button bouton = new Button();
								bouton.setMaxSize(buttonSize, buttonSize);
								listeP1.getChildren().addAll(bouton);
							}
						};
						b.setOnMouseClicked(eventActionMain);
						
						nb_p1Focus++;
						Main.addP1R1Ordre(o1);
						if(NbRobots == 2){
							Main.addP1R2Ordre(o2);
						}
					}
				}
				if(p2Focus == true){
					if(nb_p2Focus < 8){
						final Button b = ((Button) listeP2.getChildren().get(nb_p2Focus));
						
						b.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");
						b.setGraphic(imageViewSpe);
						
						EventHandler<MouseEvent> eventActionMain = new EventHandler<MouseEvent>() {
							
							@Override
							public void handle(MouseEvent event) {
								Main.suppP2R1Ordre(o1);
								if(NbRobots == 2){
									Main.suppP2R2Ordre(o2);
								}
								listeP2.getChildren().remove(b);
								if(nb_p2Focus >0)
									nb_p2Focus--;
								Button bouton = new Button();
								bouton.setMaxSize(buttonSize, buttonSize);
								listeP2.getChildren().addAll(bouton);
							}
						};
						b.setOnMouseClicked(eventActionMain);
						
						nb_p2Focus++;
						Main.addP2R1Ordre(o1);
						if(NbRobots == 2){
							Main.addP2R2Ordre(o2);
						}
					}
				}
			}
		};
		boutonFlash.setOnMouseClicked(eventBoutonFlash);
		
		
		final Button boutonFor = new Button();
		final Image image10 = new Image("./affichage/image_for.png"); 
		final ImageView imageView10 = new ImageView(image10); 
		imageView10.setFitWidth(actionSize); 
		imageView10.setFitHeight(actionSize); 
		boutonFor.setGraphic(imageView10);
		boutonFor.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");

		EventHandler<MouseEvent> eventBoutonFor = new EventHandler<MouseEvent>() {
			public void handle(MouseEvent event) {
				final Ordre_bot o1 = new For(couleurPreselec);
				final Ordre_bot o2 = new For(couleurPreselec);
				final Image imageSpe = new Image("./affichage/image_for.png"); 
				final ImageView imageViewSpe = new ImageView(imageSpe);
				imageViewSpe.setFitWidth(actionSize); 
				imageViewSpe.setFitHeight(actionSize); 
				if(mainFocus == true){
					if(nb_mainFocus < 12){
						final Button b = ((Button) listeMain.getChildren().get(nb_mainFocus));
						
						b.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");
						b.setGraphic(imageViewSpe);
						
						EventHandler<MouseEvent> eventActionMain = new EventHandler<MouseEvent>() {
							
							@Override
							public void handle(MouseEvent event) {
								Main.suppMainR1Ordre(o1);
								if(NbRobots == 2){
									Main.suppMainR2Ordre(o2);
								}
								listeMain.getChildren().remove(b);
								if(nb_mainFocus >0)
									nb_mainFocus--;
								Button bouton = new Button();
								bouton.setMaxSize(buttonSize, buttonSize);
								listeMain.getChildren().addAll(bouton);
							}
						};
						b.setOnMouseClicked(eventActionMain);
						Main.addMainR1Ordre(o1);
						if(NbRobots == 2){
							Main.addMainR2Ordre(o2);
						}
						
						nb_mainFocus++;
					}
				}
				if(p1Focus == true){
					if(nb_p1Focus < 8){
						final Button b = ((Button) listeP1.getChildren().get(nb_p1Focus));
						
						b.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");
						b.setGraphic(imageViewSpe);
						
						EventHandler<MouseEvent> eventActionMain = new EventHandler<MouseEvent>() {
							
							@Override
							public void handle(MouseEvent event) {
								Main.suppP1R1Ordre(o1);
								if(NbRobots == 2){
									Main.suppP1R2Ordre(o2);
								}
								listeP1.getChildren().remove(b);
								if(nb_p1Focus >0)
									nb_p1Focus--;
								Button bouton = new Button();
								bouton.setMaxSize(buttonSize, buttonSize);
								listeP1.getChildren().addAll(bouton);
							}
						};
						b.setOnMouseClicked(eventActionMain);
						
						nb_p1Focus++;
						Main.addP1R1Ordre(o1);
						if(NbRobots == 2){
							Main.addP1R2Ordre(o2);
						}
					}
				}
				if(p2Focus == true){
					if(nb_p2Focus < 8){
						final Button b = ((Button) listeP2.getChildren().get(nb_p2Focus));
						
						b.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");
						b.setGraphic(imageViewSpe);
						
						EventHandler<MouseEvent> eventActionMain = new EventHandler<MouseEvent>() {
							
							@Override
							public void handle(MouseEvent event) {
								Main.suppP2R1Ordre(o1);
								if(NbRobots == 2){
									Main.suppP2R2Ordre(o2);
								}
								listeP2.getChildren().remove(b);
								if(nb_p2Focus >0)
									nb_p2Focus--;
								Button bouton = new Button();
								bouton.setMaxSize(buttonSize, buttonSize);
								listeP2.getChildren().addAll(bouton);
							}
						};
						b.setOnMouseClicked(eventActionMain);
						
						nb_p2Focus++;
						Main.addP2R1Ordre(o1);
						if(NbRobots == 2){
							Main.addP2R2Ordre(o2);
						}
					}
				}
			}
		};
		boutonFor.setOnMouseClicked(eventBoutonFor);
		
		final Button boutonNb = new Button();
		final Image image11 = new Image("./affichage/image_NB.png"); 
		final ImageView imageView11 = new ImageView(image11); 
		imageView11.setFitWidth(actionSize); 
		imageView11.setFitHeight(actionSize); 
		boutonNb.setGraphic(imageView11);
		boutonNb.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");
		
		EventHandler<MouseEvent> eventBoutonNb = new EventHandler<MouseEvent>() {
			public void handle(MouseEvent event) {
				
				if(mainFocus == true){
					if(nb_mainFocus < 12){
						
						final TextInputDialog dialog = new TextInputDialog();  
						final Image baymaxHead = new Image("./affichage/Baymax_head.png"); 
						final ImageView baymaxHeadView = new ImageView(baymaxHead);
						baymaxHeadView.setFitWidth(actionSize); 
						baymaxHeadView.setFitHeight(actionSize*0.75); 
						dialog.setTitle("Choix NB"); 
						dialog.setGraphic(baymaxHeadView);
						dialog.setHeaderText("Choisissez un nombre d'it�rations.");  
						dialog.setContentText("NB  =");  
						final Optional<String> result = dialog.showAndWait();  
						result.ifPresent(button -> {});						
						
						if((dialog.getResult()!=null) && (Integer.parseInt(dialog.getResult()) >1)){

							final Ordre_bot o1 = new Nombre(Integer.parseInt(dialog.getEditor().getText()));
							final Ordre_bot o2 = new Nombre(Integer.parseInt(dialog.getEditor().getText()));
							
							final Button b = ((Button) listeMain.getChildren().get(nb_mainFocus));
							
							b.setText(dialog.getEditor().getText());
							b.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";-fx-font-size:30px;");

							EventHandler<MouseEvent> eventActionMain = new EventHandler<MouseEvent>() {
								
								@Override
								public void handle(MouseEvent event) {
									Main.suppMainR1Ordre(o1);
									if(NbRobots == 2){
										Main.suppMainR2Ordre(o2);
									}
									listeMain.getChildren().remove(b);
									if(nb_mainFocus >0)
										nb_mainFocus--;
									Button bouton = new Button();
									bouton.setMaxSize(buttonSize, buttonSize);
									listeMain.getChildren().addAll(bouton);
								}
							};
							b.setOnMouseClicked(eventActionMain);
							Main.addMainR1Ordre(o1);
							if(NbRobots == 2){
								Main.addMainR2Ordre(o2);
							}
							
							nb_mainFocus++;
						}
					}
				}
				if(p1Focus == true){
					if(nb_p1Focus < 8){
						
						final TextInputDialog dialog = new TextInputDialog();  
						final Image baymaxHead = new Image("./affichage/Baymax_head.png"); 
						final ImageView baymaxHeadView = new ImageView(baymaxHead);
						baymaxHeadView.setFitWidth(actionSize); 
						baymaxHeadView.setFitHeight(actionSize*0.75); 
						dialog.setTitle("Choix NB"); 
						dialog.setGraphic(baymaxHeadView);
						dialog.setHeaderText("Choisissez un nombre d'it�rations.");  
						dialog.setContentText("NB  =");  
						final Optional<String> result = dialog.showAndWait();  
						result.ifPresent(button -> {});						
						if((dialog.getResult()!=null) && (Integer.parseInt(dialog.getResult()) >1)){
							
							final Ordre_bot o1 = new Nombre(Integer.parseInt(dialog.getEditor().getText()));
							final Ordre_bot o2 = new Nombre(Integer.parseInt(dialog.getEditor().getText()));
							
							final Button b = ((Button) listeP1.getChildren().get(nb_p1Focus));
							
							b.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";-fx-font-size:30px;");
							b.setText(dialog.getEditor().getText());

							EventHandler<MouseEvent> eventActionP1 = new EventHandler<MouseEvent>() {
								
								@Override
								public void handle(MouseEvent event) {
									Main.suppP1R1Ordre(o1);
									if(NbRobots == 2){
										Main.suppP1R2Ordre(o2);
									}
									listeP1.getChildren().remove(b);
									if(nb_p1Focus >0)
										nb_p1Focus--;
									Button bouton = new Button();
									bouton.setMaxSize(buttonSize, buttonSize);
									listeP1.getChildren().addAll(bouton);
								}
							};
							b.setOnMouseClicked(eventActionP1);
							
							nb_p1Focus++;
							Main.addP1R1Ordre(o1);
							if(NbRobots == 2){
								Main.addP1R2Ordre(o2);
							}
						}
					}
				}
				if(p2Focus == true){
					if(nb_p2Focus < 8){
						final TextInputDialog dialog = new TextInputDialog();  
						final Image baymaxHead = new Image("./affichage/Baymax_head.png"); 
						final ImageView baymaxHeadView = new ImageView(baymaxHead);
						baymaxHeadView.setFitWidth(actionSize); 
						baymaxHeadView.setFitHeight(actionSize*0.75); 
						dialog.setTitle("Choix NB"); 
						dialog.setGraphic(baymaxHeadView);
						dialog.setHeaderText("Choisissez un nombre d'it�rations.");  
						dialog.setContentText("NB  =");  
						final Optional<String> result = dialog.showAndWait();  
						result.ifPresent(button -> {});						
						if((dialog.getResult()!=null) && (Integer.parseInt(dialog.getResult()) >1)){
							
							final Ordre_bot o1 = new Nombre(Integer.parseInt(dialog.getEditor().getText()));
							final Ordre_bot o2 = new Nombre(Integer.parseInt(dialog.getEditor().getText()));
							final Button b = ((Button) listeP2.getChildren().get(nb_p2Focus));
							
							b.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";-fx-font-size:30px;");
							b.setText(dialog.getEditor().getText());

							EventHandler<MouseEvent> eventActionMain = new EventHandler<MouseEvent>() {
								
								@Override
								public void handle(MouseEvent event) {
									Main.suppP2R1Ordre(o1);
									if(NbRobots == 2){
										Main.suppP2R2Ordre(o2);
									}
									listeP2.getChildren().remove(b);
									if(nb_p2Focus >0)
										nb_p2Focus--;
									Button bouton = new Button();
									bouton.setMaxSize(buttonSize, buttonSize);
									listeP2.getChildren().addAll(bouton);
								}
							};
							b.setOnMouseClicked(eventActionMain);
							
							nb_p2Focus++;
							Main.addP2R1Ordre(o1);
							if(NbRobots == 2){
								Main.addP2R2Ordre(o2);
							}
						}
					}
				}
			}
		};
		boutonNb.setOnMouseClicked(eventBoutonNb);
		
		
		
		final Button boutonP1 = new Button();
		final Image image7 = new Image("./affichage/image_p1.png"); 
		final ImageView imageView7 = new ImageView(image7); 
		imageView7.setFitWidth(actionSize); 
		imageView7.setFitHeight(actionSize); 
		boutonP1.setGraphic(imageView7);
		boutonP1.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");
		
		EventHandler<MouseEvent> eventBoutonP1 = new EventHandler<MouseEvent>() {
			public void handle(MouseEvent event) {
				final Image imageSpe = new Image("./affichage/image_p1.png"); 
				final ImageView imageViewSpe = new ImageView(imageSpe);
				imageViewSpe.setFitWidth(actionSize); 
				imageViewSpe.setFitHeight(actionSize); 
				if(mainFocus == true){
					if(nb_mainFocus < 12){
						final Ordre_bot o1 = Main.addP1toMainR1(couleurPreselec);
						final Ordre_bot o2 = Main.addP1toMainR2(couleurPreselec);
						
						final Button b = ((Button) listeMain.getChildren().get(nb_mainFocus));
						
						b.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");
						b.setGraphic(imageViewSpe);
						
						EventHandler<MouseEvent> eventActionMain = new EventHandler<MouseEvent>() {
							
							@Override
							public void handle(MouseEvent event) {
								Main.suppMainR1Ordre(o1);
								if(NbRobots == 2){
									Main.suppMainR2Ordre(o2);
								}
								listeMain.getChildren().remove(b);
								if(nb_mainFocus >0)
									nb_mainFocus--;
								Button bouton = new Button();
								bouton.setMaxSize(buttonSize, buttonSize);
								listeMain.getChildren().addAll(bouton);
							}
						};
						b.setOnMouseClicked(eventActionMain);
						
						nb_mainFocus++;
					}
				}
				if(p1Focus == true){
					if(nb_p1Focus < 8){
						final Ordre_bot o1 = Main.addP1toP1R1(couleurPreselec);
						final Ordre_bot o2 = Main.addP1toP1R2(couleurPreselec);
						final Button b = ((Button) listeP1.getChildren().get(nb_p1Focus));
						
						b.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");
						b.setGraphic(imageViewSpe);
						
						EventHandler<MouseEvent> eventActionMain = new EventHandler<MouseEvent>() {
							
							@Override
							public void handle(MouseEvent event) {
								Main.suppP1R1Ordre(o1);
								if(NbRobots == 2){
									Main.suppP1R2Ordre(o2);
								}
								listeP1.getChildren().remove(b);
								if(nb_p1Focus >0)
									nb_p1Focus--;
								Button bouton = new Button();
								bouton.setMaxSize(buttonSize, buttonSize);
								listeP1.getChildren().addAll(bouton);
							}
						};
						b.setOnMouseClicked(eventActionMain);
						
						nb_p1Focus++;
					}
				}
				if(p2Focus == true){
					if(nb_p2Focus < 8){
						final Ordre_bot o1 = Main.addP1toP2R1(couleurPreselec);
						final Ordre_bot o2 = Main.addP1toP2R2(couleurPreselec);
						
						final Button b = ((Button) listeP2.getChildren().get(nb_p2Focus));
						
						b.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");
						b.setGraphic(imageViewSpe);
						
						EventHandler<MouseEvent> eventActionMain = new EventHandler<MouseEvent>() {
							
							@Override
							public void handle(MouseEvent event) {
								Main.suppP2R1Ordre(o1);
								if(NbRobots == 2){
									Main.suppP2R2Ordre(o2);
								}
								listeP2.getChildren().remove(b);
								if(nb_p2Focus >0)
									nb_p2Focus--;
								Button bouton = new Button();
								bouton.setMaxSize(buttonSize, buttonSize);
								listeP2.getChildren().addAll(bouton);
							}
						};
						b.setOnMouseClicked(eventActionMain);
						
						nb_p2Focus++;
					}
				}
			}
		};
		boutonP1.setOnMouseClicked(eventBoutonP1);
		
		
		final Button boutonP2 = new Button();
		final Image image8 = new Image("./affichage/image_p2.png"); 
		final ImageView imageView8 = new ImageView(image8); 
		imageView8.setFitWidth(actionSize); 
		imageView8.setFitHeight(actionSize); 
		boutonP2.setGraphic(imageView8);
		boutonP2.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");

		EventHandler<MouseEvent> eventBoutonP2 = new EventHandler<MouseEvent>() {
			public void handle(MouseEvent event) {
				final Image imageSpe = new Image("./affichage/image_p2.png"); 
				final ImageView imageViewSpe = new ImageView(imageSpe);
				imageViewSpe.setFitWidth(actionSize); 
				imageViewSpe.setFitHeight(actionSize); 
				if(mainFocus == true){
					if(nb_mainFocus < 12){
						final Ordre_bot o1 = Main.addP2toMainR1(couleurPreselec);
						final Ordre_bot o2 = Main.addP2toMainR2(couleurPreselec);
						
						final Button b = ((Button) listeMain.getChildren().get(nb_mainFocus));
						
						b.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");
						b.setGraphic(imageViewSpe);
						
						EventHandler<MouseEvent> eventActionMain = new EventHandler<MouseEvent>() {
							
							@Override
							public void handle(MouseEvent event) {
								Main.suppMainR1Ordre(o1);
								if(NbRobots == 2){
									Main.suppMainR2Ordre(o2);
								}
								listeMain.getChildren().remove(b);
								if(nb_mainFocus >0)
									nb_mainFocus--;
								Button bouton = new Button();
								bouton.setMaxSize(buttonSize, buttonSize);
								listeMain.getChildren().addAll(bouton);
							}
						};
						b.setOnMouseClicked(eventActionMain);
						
						nb_mainFocus++;
					}
				}
				if(p1Focus == true){
					if(nb_p1Focus < 8){
						final Ordre_bot o1 = Main.addP2toP1R1(couleurPreselec);
						final Ordre_bot o2 = Main.addP2toP1R2(couleurPreselec);
						
						final Button b = ((Button) listeP1.getChildren().get(nb_p1Focus));
						
						b.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");
						b.setGraphic(imageViewSpe);
						
						EventHandler<MouseEvent> eventActionMain = new EventHandler<MouseEvent>() {
							
							@Override
							public void handle(MouseEvent event) {
								Main.suppP1R1Ordre(o1);
								if(NbRobots == 2){
									Main.suppP1R2Ordre(o2);
								}
								listeP1.getChildren().remove(b);
								if(nb_p1Focus >0)
									nb_p1Focus--;
								Button bouton = new Button();
								bouton.setMaxSize(buttonSize, buttonSize);
								listeP1.getChildren().addAll(bouton);
							}
						};
						b.setOnMouseClicked(eventActionMain);
						
						nb_p1Focus++;
					}
				}
				if(p2Focus == true){
					if(nb_p2Focus < 8){
						final Ordre_bot o1 = Main.addP2toP2R1(couleurPreselec);
						final Ordre_bot o2 = Main.addP2toP2R2(couleurPreselec);
						
						final Button b = ((Button) listeP2.getChildren().get(nb_p2Focus));
						
						b.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");
						b.setGraphic(imageViewSpe);
						
						EventHandler<MouseEvent> eventActionMain = new EventHandler<MouseEvent>() {
							
							@Override
							public void handle(MouseEvent event) {
								Main.suppP2R1Ordre(o1);
								if(NbRobots == 2){
									Main.suppP2R2Ordre(o2);
								}
								listeP2.getChildren().remove(b);
								if(nb_p2Focus >0)
									nb_p2Focus--;
								Button bouton = new Button();
								bouton.setMaxSize(buttonSize, buttonSize);
								listeP2.getChildren().addAll(bouton);
							}
						};
						b.setOnMouseClicked(eventActionMain);
						
						nb_p2Focus++;
					}
				}
			}
		};
		boutonP2.setOnMouseClicked(eventBoutonP2);
		
		final Button boutonMusic = new Button();
		final Image image14 = new Image("./affichage/image_musicOn.png"); 
		final ImageView imageView14 = new ImageView(image14); 
		imageView14.setFitWidth(actionSize); 
		imageView14.setFitHeight(actionSize); 
		boutonMusic.setGraphic(imageView14);
		boutonMusic.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");
		
		final File file = new File("src/affichage/NerdSchool.mp3"); 
        final Media media = new Media(file.toURI().toString()); 
        final MediaPlayer mediaPlayer = new MediaPlayer(media);
        mediaPlayer.setOnEndOfMedia(new Runnable() {
            public void run() {
              mediaPlayer.seek(Duration.ZERO);
            }
        });
    	mediaPlayer.play();
    	
		EventHandler<MouseEvent> eventboutonMusic = new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {

				if(mediaPlayer.getStatus() == Status.PLAYING){
					if(mediaPlayer.getVolume() != 0){
						mediaPlayer.setVolume(0);
						final Image image14 = new Image("./affichage/image_musicOff.png"); 
						final ImageView imageView14 = new ImageView(image14); 
						imageView14.setFitWidth(actionSize); 
						imageView14.setFitHeight(actionSize); 
						boutonMusic.setGraphic(imageView14);
					}
					else{
						mediaPlayer.setVolume(100);
						final Image image14 = new Image("./affichage/image_musicOn.png"); 
						final ImageView imageView14 = new ImageView(image14); 
						imageView14.setFitWidth(actionSize); 
						imageView14.setFitHeight(actionSize); 
						boutonMusic.setGraphic(imageView14);
					}
		        }
			}
		};
		boutonMusic.setOnMouseClicked(eventboutonMusic);
		
		final Button boutonPlay = new Button();
		final Image image9 = new Image("./affichage/image_play.png"); 
		final ImageView imageView9 = new ImageView(image9); 
		imageView9.setFitWidth(actionSize); 
		imageView9.setFitHeight(actionSize); 
		boutonPlay.setGraphic(imageView9);
		boutonPlay.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");

		EventHandler<MouseEvent> eventBoutonPlay = new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				try {
					Main.runMain();
				} catch (OutOfBoundBot e) {
					
				}
				//World.getRobot(0).refreshBaymax().play();
			}
		};
		boutonPlay.setOnMouseClicked(eventBoutonPlay);
		
		boutonAColorier = boutonPlay;
		
		final Button boutonRestart = new Button();
		final Image image12 = new Image("./affichage/image_restart.png"); 
		final ImageView imageView12 = new ImageView(image12); 
		imageView12.setFitWidth(actionSize); 
		imageView12.setFitHeight(actionSize); 
		boutonRestart.setGraphic(imageView12);
		boutonRestart.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");

		EventHandler<MouseEvent> eventBoutonRestart = new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				boutonPlay.setStyle("-fx-base: #"+Color.CYAN.toString().split("x")[1]+";");
				Main.init();
			}
		};
		boutonRestart.setOnMouseClicked(eventBoutonRestart);
		
		final Button boutonBack = new Button();
		final Image image13 = new Image("./affichage/image_exit.png"); 
		final ImageView imageView13 = new ImageView(image13); 
		imageView13.setFitWidth(actionSize); 
		imageView13.setFitHeight(actionSize); 
		boutonBack.setGraphic(imageView13);
		boutonBack.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");

		EventHandler<MouseEvent> eventboutonBack = new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
			    mediaPlayer.stop();
			    Main.vider();
		    	primaryStage.hide();
			}
		};
		boutonBack.setOnMouseClicked(eventboutonBack);
		
		
		//
		couleurActions.setMaxWidth(nb_couleurs*tailleCouleur);
		couleurActions.setMaxHeight(tailleCouleur);
		couleurActions.setCursor(Cursor.HAND);
		//
		final Rectangle boutonCyan = new Rectangle(25,25);
		boutonCyan.setFill(Color.CYAN);
		EventHandler<MouseEvent> eventBoutonCyan = new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				couleurPreselec = Color.CYAN;
				boutonAvancer.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");
				boutonTournerD.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");
				boutonTournerG.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");
				boutonActiver.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");
				boutonJump.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");
				boutonBr.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");
				boutonP1.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");
				boutonP2.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");
				boutonDest.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");
				boutonFlash.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");
				boutonFor.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");
			}
		};
		boutonCyan.setOnMouseClicked(eventBoutonCyan);
		//
		final Rectangle boutonRouge = new Rectangle(25,25);
		boutonRouge.setFill(Color.RED);
		EventHandler<MouseEvent> eventBoutonRouge = new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				couleurPreselec = Color.RED;
				boutonAvancer.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");
				boutonTournerD.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");
				boutonTournerG.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");
				boutonActiver.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");
				boutonJump.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");
				boutonBr.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");
				boutonP1.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");
				boutonP2.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");
				boutonDest.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");
				boutonFlash.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");
				boutonFor.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");
			}
		};
		boutonRouge.setOnMouseClicked(eventBoutonRouge);
		//
		final Rectangle boutonJaune = new Rectangle(25,25);
		boutonJaune.setFill(Color.YELLOW);
		EventHandler<MouseEvent> eventBoutonJaune = new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				couleurPreselec = Color.YELLOW;
				boutonAvancer.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");
				boutonTournerD.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");
				boutonTournerG.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");
				boutonActiver.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");
				boutonJump.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");
				boutonBr.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");
				boutonP1.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");
				boutonP2.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");
				boutonDest.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");
				boutonFlash.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");
				boutonFor.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");
			}
		};
		boutonJaune.setOnMouseClicked(eventBoutonJaune);
		//
		final Rectangle boutonGris = new Rectangle(25,25);
		boutonGris.setFill(Color.GRAY);
		EventHandler<MouseEvent> eventBoutonGris = new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				couleurPreselec = Color.GRAY;
				boutonAvancer.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");
				boutonTournerD.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");
				boutonTournerG.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");
				boutonActiver.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");
				boutonJump.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");
				boutonBr.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");
				boutonP1.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");
				boutonP2.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");
				boutonDest.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");
				boutonFlash.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");
				boutonFor.setStyle("-fx-base: #"+couleurPreselec.toString().split("x")[1]+";");
			}
		};
		boutonGris.setOnMouseClicked(eventBoutonGris);
		//
		VBox groupeActions = new VBox();
		groupeActions.paddingProperty().set(new Insets(listeActionsHeight/6,0,0,listeActionsWidth/6));

		couleurActions.setTranslateX(listeActionsWidth*0.28);
		
		groupeActions.setStyle("-fx-background-image: url(./affichage/couleur_fond.png);");

		SubScene subListeActions = new SubScene(groupeActions, listeActionsWidth, listeActionsHeight, true, SceneAntialiasing.BALANCED);

		GridPane.setConstraints(subListeActions, 0, 1); 
		couleurActions.getChildren().addAll(boutonCyan, boutonRouge, boutonJaune, boutonGris);
		listeActions.getChildren().addAll(boutonAvancer, boutonTournerG,boutonTournerD, boutonActiver, boutonJump,boutonBr,boutonDest, boutonFlash,boutonFor,boutonNb,boutonP1, boutonP2);
		groupeActions.getChildren().addAll(couleurActions, listeActions);
		
		HBox groupeOptions = new HBox();
		groupeOptions.setCursor(Cursor.HAND);

		groupeOptions.setStyle("-fx-background-image: url(./affichage/couleur_fond.png);");

		double listeOptionsWidth = winWidth/3;
		double listeOptionsHeight = (winHeight/4) + (winHeight/25);
		groupeOptions.paddingProperty().set(new Insets(listeOptionsHeight/4,0,0,listeOptionsWidth/6));

		groupeOptions.getChildren().addAll(boutonMusic,boutonPlay, boutonRestart, boutonBack);
		SubScene subListeOptions = new SubScene(groupeOptions, listeOptionsWidth, listeOptionsHeight, true, SceneAntialiasing.BALANCED);
		GridPane.setConstraints(subListeOptions, 1, 1);
		root.getChildren().addAll(subListeActions,subListeOptions);
		//
		for(int i =0; i<nbMainActions; i++){
			final Button actionMain = new Button();
			actionMain.setMaxSize(buttonSize, buttonSize);
			listeMain.getChildren().addAll(actionMain);

		}
		//
		for(int i =1; i<=nbPActions; i++){
			Button actionP1 = new Button();
			actionP1.setMaxSize(buttonSize, buttonSize);
			listeP1.getChildren().addAll(actionP1);

		}
		
		for(int i = 1; i<=nbPActions; i++){
			Button actionP2 = new Button();
			actionP2.setMaxSize(buttonSize, buttonSize);
			listeP2.getChildren().addAll(actionP2);

		}
		VBox ListeMainProcedures = new VBox();
		ListeMainProcedures.paddingProperty().set(new Insets(listeOptionsHeight/4,0,0,listeOptionsWidth/6));
		ListeMainProcedures.setStyle("-fx-background-image: url(./affichage/couleur_fond2.png);");

		
		double listeMainProceduresWidth = winWidth/3;
		double listeMainProceduresHeight = winHeight*0.75;

		ListeMainProcedures.getChildren().addAll(textMain,listeMain,textP1, listeP1,textP2, listeP2);
		SubScene subListeMainProcedures = new SubScene(ListeMainProcedures, listeMainProceduresWidth, listeMainProceduresHeight, true, SceneAntialiasing.BALANCED);
		GridPane.setConstraints(subListeMainProcedures, 1, 0); 
		root.getChildren().addAll(subListeMainProcedures);

		//Caméra
		zoom.addListener(new ChangeListener<Number>() {
			@Override
			public void changed(ObservableValue<? extends Number> ov, Number t, Number t1) {
				field.scaleXProperty().bind(zoom);
				field.scaleYProperty().bind(zoom);
				field.scaleZProperty().bind(zoom);
			}
		});
		Rotate rx = new Rotate(-ROTATION, Rotate.X_AXIS);
		Rotate ry = new Rotate(-ROTATION, Rotate.Y_AXIS);
		Rotate rz = new Rotate(-ROTATION, Rotate.Z_AXIS);
		final Camera camera = new PerspectiveCamera();
		camera.getTransforms().addAll(rx,ry,rz);
		final Rotate camRotG = new Rotate(15, centreX, centreY, centreZ, Rotate.Y_AXIS);
		final Rotate camRotD = new Rotate(-15, centreX, centreY, centreZ, Rotate.Y_AXIS);

		final Rotate camRotVP = new Rotate(10, Rotate.X_AXIS);
		final Rotate camRotVM = new Rotate(-10, Rotate.X_AXIS);

		EventHandler<KeyEvent> events = new EventHandler<KeyEvent>() {
			boolean fullScren = false;
			@Override
			public void handle(KeyEvent event) {
				if (event.getCode() == KeyCode.UP){
					camera.setTranslateX(camera.getTranslateX() - TRANSFORMATION);
					camera.setTranslateZ(camera.getTranslateZ() + TRANSFORMATION/2);
				}
				if (event.getCode() == KeyCode.DOWN){
					camera.setTranslateX(camera.getTranslateX() + TRANSFORMATION);
					camera.setTranslateZ(camera.getTranslateZ() - TRANSFORMATION/2);
				}
				if (event.getCode() == KeyCode.LEFT){
					camera.setTranslateZ(camera.getTranslateZ() - 2*TRANSFORMATION);

				}
				if (event.getCode() == KeyCode.RIGHT){
					camera.setTranslateZ(camera.getTranslateZ() + 2*TRANSFORMATION);
				}
				if(event.getCode() == KeyCode.W){
					field.getTransforms().addAll(camRotG);
				}
				if(event.getCode() == KeyCode.X){
					field.getTransforms().addAll(camRotD);
				}
				if(event.getCode() == KeyCode.Z){
					camera.getTransforms().add(camRotVM);
				}
				if(event.getCode() == KeyCode.S){
					camera.getTransforms().add(camRotVP);
				}
				if(event.getCode() == KeyCode.ENTER){
					if(!fullScren){
						fullScren = true;
					}
					else{
						fullScren = false;
					}
						primaryStage.setFullScreen(fullScren);
				}

			} 
		};
		// Configuration de la scène. 
		Scene scene = new Scene(root, winWidth, winHeight, false, SceneAntialiasing.BALANCED);
		scene.setOnKeyPressed(events);

		root.setOnScroll(new EventHandler<ScrollEvent>() {
			public void handle(ScrollEvent event) {
				if (event.getDeltaY() > 0) {
					zoomIn();
				} else {
					zoomOut();
				}
			}
		});

		double subWidth = 4*winWidth/6;
		double subHeight = 3*winHeight/4;
		SubScene sub = new SubScene(field, subWidth , subHeight, true, SceneAntialiasing.BALANCED);

		field.setStyle("-fx-background-color : transparent;");
		GridPane.setConstraints(sub, 0, 0); 
		root.getChildren().add(sub);
		// Configuration de la fenêtre. 
		primaryStage.setTitle("LightBot");
		primaryStage.setScene(scene);
		sub.setDepthTest(DepthTest.ENABLE);
		sub.setCamera(camera);
		primaryStage.show();

	}

	public void zoomIn() {
		double zoomValue = DEFAULT_ZOOM * Math.pow(zoomDelta.get(), deltaCount + 1);
		if (zoomValue > zoomMax.get()) {
			setZoom(zoomMax.get());
			return;
		}

		deltaCount++;
		setZoom(zoomValue);

	}

	public void zoomOut() {
		double zoomValue = DEFAULT_ZOOM * Math.pow(zoomDelta.get(), deltaCount - 1);
		if (zoomValue < zoomMin.get()) {
			setZoom(zoomMin.get());
			return;
		}

		deltaCount--;
		setZoom(zoomValue);
	}

	public void setZoom(double zoomValue) {
		zoom.set(zoomValue);
	}

	public static void run(){
		launch();
	}

	public static RotateTransition refreshPartieRotate(int angle){
		return World.getRobot(1).refreshBaymaxRotate(angle);
	}

	public static Timeline refreshPartie(){
		return World.getRobot(1).refreshBaymax();
	}

	public static Group getField() {
		return TheField;
	}

}

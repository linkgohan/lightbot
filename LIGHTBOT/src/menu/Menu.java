package menu;

import interfaces.Editor;

import java.io.File;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.util.Duration;

import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;

import world.World;
import affichage.AffichagePartie;


public class Menu extends Application{
	
	public static void run(){		
		Application.launch(Menu.class);
	}
	
	private static Stage stg;
	private static Pane  root = new Pane();
    private static Scene scene;
   	private static Pane root2 = new Pane();
	private static Scene scn2;
	private static Pane root3 = new Pane();
	private static Scene scn3;
	private static Pane root4;
	private static Scene scn4;
	private static Pane root5 = new Pane();
	private static Scene scn5;
	private static Pane root6 = new Pane();
	private static Scene scn6;
	private static Pane root7 = new Pane();
	private static Scene scn7;
	private static Pane root8 = new Pane();
	private static Scene scn8;
	private static Pane root9 = new Pane();
	private static Scene scn9;
	private static Pane root10 = new Pane();
	private static Scene scn10;
	private static Pane root11 = new Pane();
	private static 	Scene scn11;
	private static Pane root12 = new Pane();
	private static Scene scn12;
	private static Pane root13 = new Pane();
	private static Scene scn13;
	private static Pane root14 = new Pane();
	private static Scene scn14;
	
    static MediaPlayer mediaPlayer = null;
	double winWidth = Screen.getPrimary().getVisualBounds().getWidth();
	double winHeight = Screen.getPrimary().getVisualBounds().getHeight();
//    private double buttonWidth = winWidth/30;
//    private double buttonHeight= winHeight/30;

    
    public void start(final Stage primaryStage) {

		final File file = new File("src/menu/Microbots.mp3"); 
        final Media media = new Media(file.toURI().toString()); 
        mediaPlayer = new MediaPlayer(media);
        mediaPlayer.setOnEndOfMedia(new Runnable() {
            public void run() {
              mediaPlayer.seek(Duration.ZERO);
            }
        });
    	mediaPlayer.play();    	
    	scene = new Scene(root, winWidth, winHeight); 
    	primaryStage.setScene(scene);
    	primaryStage.setTitle("Lightbot - Baymax Edition");
    	
    	primaryStage.getIcons().setAll(new Image(getClass().getResource("icon48.png").toExternalForm()), new Image(getClass().getResource("Baymax_head.png").toExternalForm()));

    	GridPane menuPrincipal = new GridPane();
        VBox listeOptions = new VBox();
        listeOptions.setCursor(Cursor.HAND);
        listeOptions.setMaxSize(winWidth/4, winHeight/4);
		listeOptions.setTranslateX(winWidth/5);
		listeOptions.setTranslateY(-winHeight/12);
		listeOptions.paddingProperty().set(new Insets(winHeight/50,winWidth/50,winHeight/50,winWidth/50));

		listeOptions.setStyle("-fx-content-display: top;-fx-border-color:white;-fx-border-width: 10;-fx-border-radius: 45 45 45 45;");
        listeOptions.setSpacing(winHeight/20);
//        primaryStage.getScene().getRoot().setStyle("-fx-background-image: url(./menu/fond.jpg);-fx-background-repeat: no-repeat;");
        primaryStage.getScene().getRoot().setStyle("-fx-background-image: url(./menu/couleur_fond2.png);");

//    	listeOptions.setHgap(30); // espace horizontal
//    	listeOptions.setVgap(30);  //espace vertical

//        Font.loadFont(Menu.class.getResource("BATMAN.ttf").toExternalForm(),24);
//        Font.loadFont(Menu.class.getResource("TRANSFORMERS.ttf").toExternalForm(),24);
//        Font.loadFont(Menu.class.getResource("TRON.ttf").toExternalForm(),24);

		final Image imageTitle = new Image("./menu/Title.png"); 

		final Image image1 = new Image("./menu/Baymax_coucou.png"); 
		final Image image2 = new Image("./menu/Baymax_Armor_Wings_Render.png"); 
		final Image image3 = new Image("./menu/Baymax_Inflight_Render.png"); 
		final Image image4 = new Image("./menu/Hiro_Baymax.png"); 
		final Image image5 = new Image("./menu/Baymax_lollipop.png"); 

		final ImageView imageView = new ImageView(image5); 
		imageView.setFitWidth(winWidth/2);
		imageView.setFitHeight(winHeight);
		final ImageView titleView = new ImageView(imageTitle); 
		titleView.setTranslateX(winWidth/25);
		titleView.setTranslateY(winHeight/25);
		titleView.setFitHeight(winHeight/5);
		titleView.setFitWidth(winWidth/2);

		GridPane.setConstraints(titleView, 0, 0); 

		GridPane.setConstraints(imageView, 1, 1); 
		final Image jouerImage1 = new Image("./menu/bouton_Jouer1.png"); 
		final Image jouerImage2 = new Image("./menu/bouton_Jouer2.png"); 
		final Image editImage1 = new Image("./menu/bouton_Edit1.png"); 
		final Image editImage2 = new Image("./menu/bouton_Edit2.png"); 
		final Image creditsImage1 = new Image("./menu/bouton_Credits1.png"); 
		final Image creditsImage2 = new Image("./menu/bouton_Credits2.png"); 
		final Image quitImage1 = new Image("./menu/bouton_Quit1.png"); 
		final Image quitImage2 = new Image("./menu/bouton_Quit2.png"); 
		
		final ImageView jouerView = new ImageView(jouerImage1); 
		jouerView.setFitWidth(winWidth/5);
		jouerView.setFitHeight(winHeight/20);
		final ImageView editView = new ImageView(editImage1); 
		editView.setFitWidth(winWidth/5);
		editView.setFitHeight(winHeight/20);
		final ImageView creditsView = new ImageView(creditsImage1); 
		creditsView.setFitWidth(winWidth/5);
		creditsView.setFitHeight(winHeight/20);
		final ImageView quitView = new ImageView(quitImage1); 
		quitView.setFitWidth(winWidth/5);
		quitView.setFitHeight(winHeight/20);

        Button buttonLevels = new Button();
//		buttonLevels.setMaxWidth(475);
		buttonLevels.setGraphic(jouerView);
        buttonLevels.setStyle("-fx-background-color: white;-fx-font-size:" + winWidth/40 + "px;-fx-border-radius: 45 45 45 45;-fx-background-radius: 45 45 45 45;");
        buttonLevels.setOnMouseClicked(new EventHandler<MouseEvent>() { 
			  
        	public void handle(MouseEvent actionEvent) { 
//        		primaryStage.setFullScreen(false);
        		Menu.choisirDifficulte(primaryStage);
		    } 
		});
        
        buttonLevels.setOnMouseEntered(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				jouerView.setImage(jouerImage2);
				imageView.setTranslateX(winWidth/35);
				imageView.setTranslateY(-winWidth/40);

				imageView.setImage(image3);			
			}
		});
        
        buttonLevels.setOnMouseExited(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				jouerView.setImage(jouerImage1);
				imageView.setImage(image5);
				imageView.setTranslateX(0);
				imageView.setTranslateY(0);

			}
		});
        
        
        
        Button buttonEdit = new Button();
//		buttonEdit.setMaxWidth(475);
		buttonEdit.setGraphic(editView);


//-fx-font-family:TRON
        buttonEdit.setStyle("-fx-background-color: white;-fx-font-size:" + winWidth/40 + "px;-fx-border-radius: 45 45 45 45;-fx-background-radius: 45 45 45 45;");
      
        buttonEdit.setOnMouseClicked(new EventHandler<MouseEvent>() { 
			   
		    public void handle(MouseEvent actionEvent) { 
		        try {
					lancerEditor();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		    }
		}); 
        
        buttonEdit.setOnMouseEntered(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				editView.setImage(editImage2);
				imageView.setTranslateX(winWidth/35);
				imageView.setImage(image2);			
			}
		});
        
        buttonEdit.setOnMouseExited(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				editView.setImage(editImage1);	
				imageView.setImage(image5);	
				imageView.setTranslateX(0);

			}
		});
        
        Button buttonCredits = new Button();
//		buttonCredits.setMaxWidth(475);
		buttonCredits.setGraphic(creditsView);


        buttonCredits.setStyle("-fx-background-color: white;-fx-font-size:" + winWidth/40 + "px;-fx-border-radius: 45 45 45 45;-fx-background-radius: 45 45 45 45;");

        buttonCredits.setOnMouseClicked(new EventHandler<MouseEvent>() { 
			  
        	public void handle(MouseEvent actionEvent) { 
//        		primaryStage.setFullScreen(false);
		        Menu.credits(primaryStage);
		    } 
		});
        
        buttonCredits.setOnMouseEntered(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				creditsView.setImage(creditsImage2);			
				imageView.setImage(image4);			
			}
		});
        
        buttonCredits.setOnMouseExited(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				creditsView.setImage(creditsImage1);			
				imageView.setImage(image5);			
			}
		});
        
        
        
        Button buttonQuit = new Button();
//		buttonQuit.setMaxWidth(475);
		buttonQuit.setGraphic(quitView);

        buttonQuit.setStyle("-fx-background-color: white;-fx-font-size:" + winWidth/40 + "px;-fx-border-radius: 45 45 45 45;-fx-background-radius: 45 45 45 45;");

        buttonQuit.setOnMouseClicked(new EventHandler<MouseEvent>() { 
			  
        	public void handle(MouseEvent actionEvent) { 
				quitView.setImage(quitImage2);			

        		Alert dialogC = new Alert(AlertType.CONFIRMATION);
        		dialogC.setTitle("Quitter ?");

        		final Image baymaxHead = new Image("./affichage/Baymax_head.png"); 
				final ImageView baymaxHeadView = new ImageView(baymaxHead);
				baymaxHeadView.setFitWidth((Screen.getPrimary().getVisualBounds().getWidth())/30); 
				baymaxHeadView.setFitHeight(((Screen.getPrimary().getVisualBounds().getWidth())/30)*0.75); 

        		dialogC.setHeaderText(null);
        		dialogC.setContentText("Etes-vous sur de vouloir quitter?");
        		dialogC.setGraphic(baymaxHeadView);

        		Optional<ButtonType> answer = dialogC.showAndWait();
        		if (answer.get() == ButtonType.OK) {
        			primaryStage.close();
        		}
        		else
    				quitView.setImage(quitImage1);			
        		//primaryStage.setFullScreen(false);
		    } 
		});
        
        buttonQuit.setOnMouseEntered(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				quitView.setImage(quitImage2);	
				imageView.setTranslateX(winWidth/35);
				imageView.setImage(image1);			
			}
		});
        
        buttonQuit.setOnMouseExited(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				quitView.setImage(quitImage1);			
				imageView.setImage(image5);	
				imageView.setTranslateX(0);

			}
		});
        
        
		GridPane.setConstraints(listeOptions, 0, 1); 
        
        listeOptions.getChildren().setAll(buttonLevels,buttonEdit,buttonCredits,buttonQuit); 
        menuPrincipal.getChildren().setAll(titleView,listeOptions, imageView); 

        root.getChildren().addAll(menuPrincipal);
        stg = primaryStage;
        primaryStage.show();
//		primary//stage.setFullScreen(true);

    }

	public static void choisirDifficulte(final Stage stage){
		
		double winWidth = Screen.getPrimary().getVisualBounds().getWidth();
		double winHeight = Screen.getPrimary().getVisualBounds().getHeight();
    	if(scn2 == null)
    		scn2 = new Scene(root2, winWidth, winHeight);
    	
    	final Image imageRetour1 = new Image("./menu/bouton_Retour1.png"); 
    	final Image imageRetour2 = new Image("./menu/bouton_Retour2.png"); 

		final ImageView buttonReturn = new ImageView(imageRetour1);
		buttonReturn.setFitWidth(winWidth/7);
		buttonReturn.setFitHeight(winHeight/5);
		buttonReturn.setTranslateX(winWidth*0.05);
		buttonReturn.setTranslateY(winHeight*0.033);

		
		buttonReturn.setOnMouseEntered(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				buttonReturn.setImage(imageRetour2);			
			}
		});
        
		buttonReturn.setOnMouseExited(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				buttonReturn.setImage(imageRetour1);	

			}
		});
		    	buttonReturn.setOnMouseClicked(new EventHandler<MouseEvent>() { 
 		   
		    public void handle(MouseEvent actionEvent) { 
		    	stage.setTitle("Lightbot - Baymax Edition");
		        stage.setScene(scene);
		        stg = stage;
		        stage.show();
				//stage.setFullScreen(true);

		        
		    }
		});
    	VBox vbox = new VBox();
    	vbox.setCursor(Cursor.HAND);
    	
    	
		final Image facileImage1 = new Image("./menu/bouton_Facile1.png"); 
		final Image facileImage2 = new Image("./menu/bouton_Facile2.png"); 
		final Image moyenImage1 = new Image("./menu/bouton_Moyen1.png"); 
		final Image moyenImage2 = new Image("./menu/bouton_Moyen2.png"); 
		final Image difficileImage1 = new Image("./menu/bouton_Difficile1.png"); 
		final Image difficileImage2 = new Image("./menu/bouton_Difficile2.png"); 
		final Image customImage1 = new Image("./menu/bouton_Custom1.png"); 
		final Image customImage2 = new Image("./menu/bouton_Custom2.png"); 
		
		final ImageView facileView = new ImageView(facileImage1); 
		facileView.setFitWidth(winWidth/5);
		facileView.setFitHeight(winHeight/20);
		final ImageView moyenView = new ImageView(moyenImage1); 
		moyenView.setFitWidth(winWidth/5);
		moyenView.setFitHeight(winHeight/20);
		final ImageView difficileView = new ImageView(difficileImage1); 
		difficileView.setFitWidth(winWidth/5);
		difficileView.setFitHeight(winHeight/20);
		final ImageView customView = new ImageView(customImage1); 
		customView.setFitWidth(winWidth/5);
		customView.setFitHeight(winHeight/20);

    	Button buttonFacile = new Button();
//		buttonFacile.setMaxWidth(475);
		buttonFacile.setGraphic(facileView);
		buttonFacile.setStyle("-fx-background-color: white;-fx-font-size:" + winWidth/40 + "px;-fx-border-radius: 45 45 45 45;-fx-background-radius: 45 45 45 45;");
	 	buttonFacile.setOnMouseClicked(new EventHandler<MouseEvent>() { 
	 		   
		    public void handle(MouseEvent actionEvent) { 
//				stage.setFullScreen(false);
		    	choisirModeFacile(stage);
		    }
		});
	 	
        buttonFacile.setOnMouseEntered(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				facileView.setImage(facileImage2);			
			}
		});
        
        buttonFacile.setOnMouseExited(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				facileView.setImage(facileImage1);	

			}
		});
	 	
	 	Button buttonMoyen = new Button();
//	 	buttonMoyen.setMaxWidth(475);
	 	buttonMoyen.setGraphic(moyenView);
	 	buttonMoyen.setStyle("-fx-background-color: white;-fx-font-size:" + winWidth/40 + "px;-fx-border-radius: 45 45 45 45;-fx-background-radius: 45 45 45 45;");
	 	buttonMoyen.setOnMouseClicked(new EventHandler<MouseEvent>() { 
	 		   
		    public void handle(MouseEvent actionEvent) { 
//				stage.setFullScreen(false);
		    	choisirModeMoyen(stage);
		    }
		});
	 	buttonMoyen.setOnMouseEntered(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				moyenView.setImage(moyenImage2);			
			}
		});
        
	 	buttonMoyen.setOnMouseExited(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				moyenView.setImage(moyenImage1);	

			}
		});
	 	
	 	Button buttonDifficile = new Button();
//	 	buttonDifficile.setMaxWidth(475);
	 	buttonDifficile.setGraphic(difficileView);
	 	buttonDifficile.setStyle("-fx-background-color: white;-fx-font-size:" + winWidth/40 + "px;-fx-border-radius: 45 45 45 45;-fx-background-radius: 45 45 45 45;");
	 	
	 	buttonDifficile.setOnMouseClicked(new EventHandler<MouseEvent>() { 
	 		   
		    public void handle(MouseEvent actionEvent) { 
//				stage.setFullScreen(false);

		    	niveauxDifficiles(stage);
		    }
		});
	 	buttonDifficile.setOnMouseEntered(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				difficileView.setImage(difficileImage2);			
			}
		});
        
	 	buttonDifficile.setOnMouseExited(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				difficileView.setImage(difficileImage1);	

			}
		});
	 	
	 	Button buttonCustom = new Button();
//	 	buttonCustom.setMaxWidth(475);
	 	buttonCustom.setGraphic(customView);
	 	buttonCustom.setStyle("-fx-background-color: white;-fx-font-size:" + winWidth/40 + "px;-fx-border-radius: 45 45 45 45;-fx-background-radius: 45 45 45 45;");
	 	buttonCustom.setOnMouseClicked(new EventHandler<MouseEvent>() {    
		    public void handle(MouseEvent actionEvent) { 
//				stage.setFullScreen(false);
		    	niveauxCustom(stage);
		    	
		    }
		});
	 	buttonCustom.setOnMouseEntered(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				customView.setImage(customImage2);			
			}
		});
        
	 	buttonCustom.setOnMouseExited(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				customView.setImage(customImage1);	

			}
		});

	 	vbox.getChildren().addAll(buttonFacile, buttonMoyen, buttonDifficile, buttonCustom, buttonReturn);
	 	
		final Image imageTitle = new Image("./menu/choix_Difficulte.png"); 
		final ImageView titleView = new ImageView(imageTitle); 
		titleView.setTranslateX(winWidth*0.2);
		titleView.setTranslateY(winHeight/7);

		titleView.setFitHeight(winHeight/14);
		titleView.setFitWidth(winWidth/2);
	 	
    	root2.getChildren().addAll(titleView, vbox);
    	
    	vbox.setTranslateY(winHeight/3);
     	vbox.setTranslateX(winWidth/3);
    	vbox.setSpacing(20);
     	
    	stage.setTitle("Lightbot - Baymax Edition");
     	stage.setScene(scn2);
        stage.getScene().getRoot().setStyle("-fx-background-image: url(./menu/couleur_fond2.png);");
        stg = stage;
     	stage.show();
     	
//		stage.setFullScreen(true);

    }
	
	public static void niveauxCustom(final Stage stage){
    	root14 = new Pane();
    	double winWidth = Screen.getPrimary().getVisualBounds().getWidth();
    	double winHeight = Screen.getPrimary().getVisualBounds().getHeight();
    	scn14 = new Scene(root14, winWidth, winHeight);

    	org.jdom2.Document document = null; 
    	Element racine;
    	int nb_maps = 0;
    	SAXBuilder sxb = new SAXBuilder();
		try
		{ 
			document = sxb.build(new File("./Niveaux/Bonus/sauv.xml"));
		}
		catch(Exception e){};
		
		racine = document.getRootElement();
		
		Element courant;
		List listDebloque = racine.getChildren("nb_maps");
		Iterator iteDebloque = listDebloque.iterator();
		while(iteDebloque.hasNext())
		{
			courant = (Element)iteDebloque.next();
			nb_maps = Integer.parseInt(courant.getAttributeValue("nb"));
		}
		/////////////////////////////////////////
		/////////////////////////////////////////
		
		GridPane gridMain = new GridPane();
		
     	VBox vbox = new VBox();
     	vbox.setCursor(Cursor.HAND);
     	vbox.setTranslateY(winHeight*0.4);
     	vbox.setSpacing(winHeight/8);
     	
    	final Image imageNivs = new Image("./menu/custom_levels.png"); 

		final ImageView nivView = new ImageView(imageNivs);
		nivView.setFitWidth(winWidth/2);
		nivView.setFitHeight(winHeight/2);
		nivView.setTranslateX(winWidth*0.325);
		nivView.setTranslateY(winHeight*0.3);

		GridPane.setConstraints(vbox, 0, 0);
	 	GridPane.setConstraints(nivView, 1, 0);
     	
		final Image imageTitle = new Image("./menu/choix_Niveau.png"); 
		final ImageView titleView = new ImageView(imageTitle); 
		titleView.setTranslateX(winWidth*0.2);
		titleView.setTranslateY(winHeight/7);

		titleView.setFitHeight(winHeight/14);
		titleView.setFitWidth(winWidth/2);
    	
    	final Image imageRetour1 = new Image("./menu/bouton_Retour1.png"); 
    	final Image imageRetour2 = new Image("./menu/bouton_Retour2.png"); 

		final ImageView buttonReturn = new ImageView(imageRetour1);
		buttonReturn.setFitWidth(winWidth/7);
		buttonReturn.setFitHeight(winHeight/5);
		
		buttonReturn.setOnMouseEntered(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				buttonReturn.setImage(imageRetour2);			
			}
		});
        
		buttonReturn.setOnMouseExited(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				buttonReturn.setImage(imageRetour1);	

			}
		});
		    
    	buttonReturn.setOnMouseClicked(new EventHandler<MouseEvent>() { 
		   
		    public void handle(MouseEvent actionEvent) { 
				nivView.setImage(null);	
		    	stage.setTitle("Lightbot - Baymax Edition");
		        stage.setScene(scn2);
		        stg = stage;
		        stage.show();
				//stage.setFullScreen(true);

		    }
		});
	 	
	 	final MenuButton menuButton = new MenuButton ();
	 	menuButton.setStyle("-fx-background-color: transparent;-fx-mark-color: white;-fx-border-color: white;-fx-border-insets: 5;-fx-border-width:" +winWidth/200+ ";-fx-border-style: solid;");
    	final Image menuButton1 = new Image("./menu/bouton_menuButton1.png"); 
		final ImageView menuButtonView = new ImageView(menuButton1);
		menuButtonView.setFitWidth(winWidth/5);
		menuButtonView.setFitHeight(winHeight/15);
	 	menuButton.setGraphic(menuButtonView);
	 	for(int i=1; i<=nb_maps;i++){
	 		String j = Integer.toString(i);
	 		MenuItem item = new MenuItem(j);
	 		item.setStyle("-fx-padding: 0em "+winWidth/110.5+"em 0em "+winWidth/8.95+";-fx-background-color:transparent;-fx-text-fill: black;");
		 	item.setOnAction(new EventHandler<ActionEvent>() { 
		 		  
		 	    @Override 
		 	public void handle(ActionEvent actionEvent) { 
		 	   	try {
					World.init("./Niveaux/Bonus/Bonus"+j+".xml", "", -1);
					lancerLevels(-1);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		 	    }
		 	});
		 	menuButton.getItems().add(item);
	 	}

		vbox.setTranslateX(winWidth*0.14);
     	buttonReturn.setTranslateY(winHeight*0.175);
     	vbox.getChildren().addAll(menuButton, buttonReturn);
     	gridMain.getChildren().addAll(vbox, nivView);
     	root14.getChildren().addAll(titleView,gridMain);
     	
     	stage.setTitle("Lightbot - Baymax Edition - Choix du niveau (Custom)");
     	stage.setScene(scn14);
        stage.getScene().getRoot().setStyle("-fx-background-image: url(./menu/couleur_fond2.png);");
        stg = stage;
        stage.show();
    }
    
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
    public static void choisirModeFacile(final Stage stage){
    	
		//stage.setFullScreen(true);

		double winWidth = Screen.getPrimary().getVisualBounds().getWidth();
		double winHeight = Screen.getPrimary().getVisualBounds().getHeight();
    	if(scn3 == null)
    		scn3 = new Scene(root3, winWidth, winHeight);
    	
    	VBox vbox = new VBox();
    	vbox.setCursor(Cursor.HAND);
    	////Bouton retour////
    	final Image imageRetour1 = new Image("./menu/bouton_Retour1.png"); 
    	final Image imageRetour2 = new Image("./menu/bouton_Retour2.png"); 

		final ImageView buttonReturn = new ImageView(imageRetour1);
		buttonReturn.setFitWidth(winWidth/7);
		buttonReturn.setFitHeight(winHeight/5);
		buttonReturn.setTranslateX(winWidth*0.05);
		buttonReturn.setTranslateY(winHeight*0.033);

		
		buttonReturn.setOnMouseEntered(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				buttonReturn.setImage(imageRetour2);			
			}
		});
        
		buttonReturn.setOnMouseExited(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				buttonReturn.setImage(imageRetour1);	

			}
		});
		    	buttonReturn.setOnMouseClicked(new EventHandler<MouseEvent>() { 
		   
		    public void handle(MouseEvent actionEvent) { 
		    	stage.setTitle("Lightbot - Baymax Edition");
		        stage.setScene(scn2);
		        stg = stage;
		        stage.show();
				//stage.setFullScreen(true);

		    }
		});

		final Image imageTitle = new Image("./menu/choix_Mode.png"); 
		final ImageView titleView = new ImageView(imageTitle); 
		titleView.setTranslateX(winWidth*0.29);
		titleView.setTranslateY(winHeight*0.17);

		titleView.setFitHeight(winHeight*0.04);
		titleView.setFitWidth(winWidth*0.32);
    	
    	
		final Image basiqueImage1 = new Image("./menu/bouton_Basique1.png"); 
		final Image basiqueImage2 = new Image("./menu/bouton_Basique2.png"); 
		final Image teleportImage1 = new Image("./menu/bouton_Teleport1.png"); 
		final Image teleportImage2 = new Image("./menu/bouton_Teleport2.png"); 
		final Image ifthenelseImage1 = new Image("./menu/bouton_Ifthenelse1.png"); 
		final Image ifthenelseImage2 = new Image("./menu/bouton_Ifthenelse2.png"); 
		final Image breakImage1 = new Image("./menu/bouton_Break1.png"); 
		final Image breakImage2 = new Image("./menu/bouton_Break2.png"); 
		
		final ImageView basiqueView = new ImageView(basiqueImage1); 
		basiqueView.setFitWidth(winWidth/5);
		basiqueView.setFitHeight(winHeight/20);
		final ImageView teleportView = new ImageView(teleportImage1); 
		teleportView.setFitWidth(winWidth/5);
		teleportView.setFitHeight(winHeight/20);
		final ImageView ifthenelseView = new ImageView(ifthenelseImage1); 
		ifthenelseView.setFitWidth(winWidth/5);
		ifthenelseView.setFitHeight(winHeight/20);
		final ImageView breakView = new ImageView(breakImage1); 
		breakView.setFitWidth(winWidth/5);
		breakView.setFitHeight(winHeight/20);
    	
    	
    	Button buttonBasique = new Button();
//    	buttonBasique.setMaxWidth(475);
    	buttonBasique.setGraphic(basiqueView);
    	buttonBasique.setStyle("-fx-background-color: white;-fx-font-size:" + winWidth/40 + "px;-fx-border-radius: 45 45 45 45;-fx-background-radius: 45 45 45 45;");

	 	buttonBasique.setOnMouseEntered(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				basiqueView.setImage(basiqueImage2);			
			}
		});
        
	 	buttonBasique.setOnMouseExited(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				basiqueView.setImage(basiqueImage1);	

			}
		});
	 	
	 	buttonBasique.setOnMouseClicked(new EventHandler<MouseEvent>() {
		    public void handle(MouseEvent actionEvent) { 
		    	niveauxFacilesBase(stage);
		    }
		});
	 	
	 	
	 	
	 	Button buttonTeleport = new Button();
//	 	buttonTeleport.setMaxWidth(475);
	 	buttonTeleport.setGraphic(teleportView);
	 	buttonTeleport.setStyle("-fx-background-color: white;-fx-font-size:" + winWidth/40 + "px;-fx-border-radius: 45 45 45 45;-fx-background-radius: 45 45 45 45;");

	 	buttonTeleport.setOnMouseEntered(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				teleportView.setImage(teleportImage2);			
			}
		});
        
	 	buttonTeleport.setOnMouseExited(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				teleportView.setImage(teleportImage1);	

			}
		});
	 	
	 	buttonTeleport.setOnMouseClicked(new EventHandler<MouseEvent>() { 
		   
		    public void handle(MouseEvent actionEvent) { 
		    	niveauxFacilesTeleport(stage);
		    }
		});
	 	
	 	Button buttonIfthenelse = new Button();

//	 	buttonIfthenelse.setMaxWidth(475);
	 	buttonIfthenelse.setGraphic(ifthenelseView);
	 	buttonIfthenelse.setStyle("-fx-background-color: white;-fx-font-size:" + winWidth/40 + "px;-fx-border-radius: 45 45 45 45;-fx-background-radius: 45 45 45 45;");

	 	buttonIfthenelse.setOnMouseEntered(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				ifthenelseView.setImage(ifthenelseImage2);			
			}
		});
        
	 	buttonIfthenelse.setOnMouseExited(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				ifthenelseView.setImage(ifthenelseImage1);	

			}
		});
	 	
	 	
	 	buttonIfthenelse.setOnMouseClicked(new EventHandler<MouseEvent>() { 
		   
		    public void handle(MouseEvent actionEvent) {
		    	niveauxFacilesIf(stage);
		    }
		});
	 	
	 	Button buttonBreak = new Button();

//	 	buttonBreak.setMaxWidth(475);
	 	buttonBreak.setGraphic(breakView);
	 	buttonBreak.setStyle("-fx-background-color: white;-fx-font-size:" + winWidth/40 + "px;-fx-border-radius: 45 45 45 45;-fx-background-radius: 45 45 45 45;");

	 	buttonBreak.setOnMouseEntered(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				breakView.setImage(breakImage2);			
			}
		});
        
	 	buttonBreak.setOnMouseExited(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				breakView.setImage(breakImage1);	

			}
		});
	 	
	 	buttonBreak.setOnMouseClicked(new EventHandler<MouseEvent>() { 
		   
		    public void handle(MouseEvent actionEvent) { 
		    	niveauxFacilesBreak(stage);
		    }
		});
     	vbox.getChildren().addAll(buttonBasique,buttonTeleport,buttonIfthenelse,buttonBreak, buttonReturn);
     	root3.getChildren().addAll(titleView,vbox);
     	
    	vbox.setTranslateY(winHeight/3);
     	vbox.setTranslateX(winWidth/3);
    	vbox.setSpacing(20);

     	stage.setTitle("Lightbot - Baymax Edition");
     	
     	stage.setScene(scn3);
     	
        stage.getScene().getRoot().setStyle("-fx-background-image: url(./menu/couleur_fond2.png);");
        stg = stage;
     	stage.show();
    }
    
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 
    public static void choisirModeMoyen(final Stage stage){
    	
		//stage.setFullScreen(true);
		double winWidth = Screen.getPrimary().getVisualBounds().getWidth();
		double winHeight = Screen.getPrimary().getVisualBounds().getHeight();
    	if(scn4 == null){
    		root4 = new Pane();
    		scn4 = new Scene(root4, winWidth, winHeight);
    	}

		final Image imageTitle = new Image("./menu/choix_Mode.png"); 
		final ImageView titleView = new ImageView(imageTitle); 
		titleView.setTranslateX(winWidth*0.3);
		titleView.setTranslateY(winHeight*0.17);

		titleView.setFitHeight(winHeight*0.05);
		titleView.setFitWidth(winWidth*0.3);
		
    	VBox vbox = new VBox();
    	vbox.setCursor(Cursor.HAND);
    	////Bouton retour////
    	
    	final Image imageRetour1 = new Image("./menu/bouton_Retour1.png"); 
    	final Image imageRetour2 = new Image("./menu/bouton_Retour2.png"); 

		final ImageView buttonReturn = new ImageView(imageRetour1);
		buttonReturn.setFitWidth(winWidth/7);
		buttonReturn.setFitHeight(winHeight/5);
		buttonReturn.setTranslateX(winWidth*0.05);
		buttonReturn.setTranslateY(winHeight*0.145);

		
		buttonReturn.setOnMouseEntered(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				buttonReturn.setImage(imageRetour2);			
			}
		});
        
		buttonReturn.setOnMouseExited(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				buttonReturn.setImage(imageRetour1);	

			}
		});
		
		
    	buttonReturn.setOnMouseClicked(new EventHandler<MouseEvent>() { 
		   
		    public void handle(MouseEvent actionEvent) { 
		    	stage.setTitle("Lightbot - Baymax Edition");
		        stage.setScene(scn2);
		        stg = stage;
		        stage.show();
				//stage.setFullScreen(true);

		    }
		});

		final Image flashbackImage1 = new Image("./menu/bouton_Flashback1.png"); 
		final Image flashbackImage2 = new Image("./menu/bouton_Flashback2.png"); 
		final Image deuxbotsImage1 = new Image("./menu/bouton_2bots1.png"); 
		final Image deuxbotsImage2 = new Image("./menu/bouton_2bots2.png"); 
		final Image forImage1 = new Image("./menu/bouton_For1.png"); 
		final Image forImage2 = new Image("./menu/bouton_For2.png"); 
		
		final ImageView flashbackView = new ImageView(flashbackImage1); 
		flashbackView.setFitWidth(winWidth/5);
		flashbackView.setFitHeight(winHeight/20);
		final ImageView deuxbotsView = new ImageView(deuxbotsImage1); 
		deuxbotsView.setFitWidth(winWidth/5);
		deuxbotsView.setFitHeight(winHeight/20);
		final ImageView forView = new ImageView(forImage1); 
		forView.setFitWidth(winWidth/5);
		forView.setFitHeight(winHeight/20);
    	
    	
    	Button buttonFlashback = new Button();

    	buttonFlashback.setGraphic(flashbackView);
    	buttonFlashback.setStyle("-fx-background-color: white;-fx-font-size:" + winWidth/40 + "px;-fx-border-radius: 45 45 45 45;-fx-background-radius: 45 45 45 45;");
    	
    	buttonFlashback.setOnMouseEntered(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				flashbackView.setImage(flashbackImage2);			
			}
		});
        
    	buttonFlashback.setOnMouseExited(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				flashbackView.setImage(flashbackImage1);	

			}
		});
    	
    	
	 	buttonFlashback.setOnMouseClicked(new EventHandler<MouseEvent>() {
		    public void handle(MouseEvent actionEvent) { 
		    	niveauxMoyensFlashback(stage);
		    }
		});
	 	
	 	
	 	Button buttonFor = new Button();

	 	buttonFor.setGraphic(forView);
	 	buttonFor.setStyle("-fx-background-color: white;-fx-font-size:" + winWidth/40 + "px;-fx-border-radius: 45 45 45 45;-fx-background-radius: 45 45 45 45;");
    	
	 	buttonFor.setOnMouseEntered(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				forView.setImage(forImage2);			
			}
		});
        
	 	buttonFor.setOnMouseExited(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				forView.setImage(forImage1);	

			}
		});
	 	
	 	
	 	buttonFor.setOnMouseClicked(new EventHandler<MouseEvent>() { 
		   
		    public void handle(MouseEvent actionEvent) { 
		    	niveauxMoyensFor(stage);
		    }
		});
	 	
	 	
	 	Button button2bots = new Button();

	 	
	 	button2bots.setGraphic(deuxbotsView);
	 	button2bots.setStyle("-fx-background-color: white;-fx-font-size:" + winWidth/40 + "px;-fx-border-radius: 45 45 45 45;-fx-background-radius: 45 45 45 45;");
    	
	 	button2bots.setOnMouseEntered(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				deuxbotsView.setImage(deuxbotsImage2);			
			}
		});
        
	 	button2bots.setOnMouseExited(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				deuxbotsView.setImage(deuxbotsImage1);	

			}
		});
	 	
	 	
	 	button2bots.setOnMouseClicked(new EventHandler<MouseEvent>() { 
		   
		    public void handle(MouseEvent actionEvent) { 
		    	niveauxMoyens2Bots(stage);
		    }
		});
     	vbox.getChildren().setAll(buttonFlashback,buttonFor,button2bots,buttonReturn);
     	root4.getChildren().addAll(titleView,vbox);
     	
    	vbox.setTranslateY(winHeight/3);
     	vbox.setTranslateX(winWidth/3);
    	vbox.setSpacing(20);

     	stage.setTitle("Lightbot - Baymax Edition");

     	stage.setScene(scn4);
        stage.getScene().getRoot().setStyle("-fx-background-image: url(./menu/couleur_fond2.png);");
        stg = stage;
     	stage.show();
    }
    
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public static void niveauxFacilesBase(final Stage stage){
    	root5 = new Pane();
		//stage.setFullScreen(true);
    	double winWidth = Screen.getPrimary().getVisualBounds().getWidth();
    	double winHeight = Screen.getPrimary().getVisualBounds().getHeight();
     	scn5 = new Scene(root5, winWidth, winHeight);
    	org.jdom2.Document document = null; 
    	Element racine;
    	int nb_maps = 0;
    	SAXBuilder sxb = new SAXBuilder();
		try
		{ 
			document = sxb.build(new File("./Niveaux/Facile/Base/sauv.xml"));
		}
		catch(Exception e){};
		
		racine = document.getRootElement();
		
		Element courant;
		List listDebloque = racine.getChildren("nb_maps_debloques");
		Iterator iteDebloque = listDebloque.iterator();
		while(iteDebloque.hasNext())
		{
			courant = (Element)iteDebloque.next();
			nb_maps = Integer.parseInt(courant.getAttributeValue("nb"));
		}
		
		/////////////////////////////////////////
		/////////////////////////////////////////
		
		GridPane gridMain = new GridPane();
		
     	VBox vbox = new VBox();
     	vbox.setCursor(Cursor.HAND);
     	vbox.setTranslateY(winHeight*0.4);
     	vbox.setSpacing(winHeight/8);
     	
    	final Image imageNivs = new Image("./menu/basique_levels.png"); 
    	final Image imageNiv1 = new Image("./menu/basique_level1.png"); 
    	final Image imageNiv2 = new Image("./menu/basique_level2.png"); 
    	final Image imageNiv3 = new Image("./menu/basique_level3.png"); 
    	final Image imageNiv4 = new Image("./menu/basique_level4.png"); 

		final ImageView nivView = new ImageView(imageNivs);
		nivView.setFitWidth(winWidth/2);
		nivView.setFitHeight(winHeight/2);
		nivView.setTranslateX(winWidth*0.325);
		nivView.setTranslateY(winHeight*0.3);

		GridPane.setConstraints(vbox, 0, 0);
	 	GridPane.setConstraints(nivView, 1, 0);
     	
    	GridPane grid5 = new GridPane();
    	
		final Image imageTitle = new Image("./menu/choix_Niveau.png"); 
		final ImageView titleView = new ImageView(imageTitle); 
		titleView.setTranslateX(winWidth*0.2);
		titleView.setTranslateY(winHeight/7);

		titleView.setFitHeight(winHeight/14);
		titleView.setFitWidth(winWidth/2);
    	
    	final Image imageRetour1 = new Image("./menu/bouton_Retour1.png"); 
    	final Image imageRetour2 = new Image("./menu/bouton_Retour2.png"); 
		final ImageView buttonReturn = new ImageView(imageRetour1);
		buttonReturn.setFitWidth(winWidth/7);
		buttonReturn.setFitHeight(winHeight/5);
		buttonReturn.setTranslateX(winWidth*0.14);
		
		buttonReturn.setOnMouseEntered(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				buttonReturn.setImage(imageRetour2);			
			}
		});
        
		buttonReturn.setOnMouseExited(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				buttonReturn.setImage(imageRetour1);	

			}
		});
		    
    	buttonReturn.setOnMouseClicked(new EventHandler<MouseEvent>() { 
		   
		    public void handle(MouseEvent actionEvent) { 
				nivView.setImage(null);	
		    	stage.setTitle("Lightbot - Baymax Edition");
		        stage.setScene(scn3);
		        stg = stage;
		        stage.show();
				//stage.setFullScreen(true);

		    }
		});
    	
		final Image bouton1Image1 = new Image("./menu/bouton_01_desactive.png"); 
		final Image bouton1Image2 = new Image("./menu/bouton_01_active.png"); 
		final Image bouton2Image1 = new Image("./menu/bouton_02_desactive.png"); 
		final Image bouton2Image2 = new Image("./menu/bouton_02_active.png"); 
		final Image bouton3Image1 = new Image("./menu/bouton_03_desactive.png"); 
		final Image bouton3Image2 = new Image("./menu/bouton_03_active.png"); 
		final Image bouton4Image1 = new Image("./menu/bouton_04_desactive.png"); 
		final Image bouton4Image2 = new Image("./menu/bouton_04_active.png"); 
		
		final ImageView button1 = new ImageView(bouton1Image1); 
		button1.setFitWidth(winWidth/12);
		button1.setFitHeight(winHeight/8);


		final ImageView button2 = new ImageView(bouton2Image1); 
		button2.setFitWidth(winWidth/12);
		button2.setFitHeight(winHeight/8);

		
		final ImageView button3 = new ImageView(bouton3Image1); 
		button3.setFitWidth(winWidth/12);
		button3.setFitHeight(winHeight/8);

		
		final ImageView button4 = new ImageView(bouton4Image1); 
		button4.setFitWidth(winWidth/12);
		button4.setFitHeight(winHeight/8);

		
	 	GridPane.setConstraints(button1, 0, 0);
	 	GridPane.setConstraints(button2, 1, 0);
	 	GridPane.setConstraints(button3, 0, 1);
	 	GridPane.setConstraints(button4, 1, 1);
	 	
	 	button1.setDisable(true);
	 	button2.setDisable(true);
	 	button3.setDisable(true);
	 	button4.setDisable(true);
	 	
	 	
	 	
	 	grid5.setHgap(30); 
	 	grid5.setVgap(30);

	 	
	 	button1.setOnMouseClicked(new EventHandler<MouseEvent>() {
		    public void handle(MouseEvent actionEvent) { 
		    	try {
					World.init("./Niveaux/Facile/Base/Base1.xml", "./Niveaux/Facile/Base/", 1);
					lancerLevels(1);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		    }
		});
	 	
	 	button1.setOnMouseEntered(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				nivView.setImage(null);
				nivView.setImage(imageNiv1);			
			}
		});
        
	 	button1.setOnMouseExited(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				nivView.setImage(imageNivs);	

			}
		});
	 	
     	button2.setOnMouseClicked(new EventHandler<MouseEvent>() { 
    	    public void handle(MouseEvent actionEvent) { 
		    	try {
					World.init("./Niveaux/Facile/Base/Base2.xml", "./Niveaux/Facile/Base/", 2);
					lancerLevels(1);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		    }
		});
     	
	 	button2.setOnMouseEntered(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				nivView.setImage(imageNiv2);			
			}
		});
        
	 	button2.setOnMouseExited(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				nivView.setImage(imageNivs);	

			}
		});
     	
     	
     	button3.setOnMouseClicked(new EventHandler<MouseEvent>() { 
    	    public void handle(MouseEvent actionEvent) { 
		    	try {
					World.init("./Niveaux/Facile/Base/Base3.xml", "./Niveaux/Facile/Base/", 3);
					lancerLevels(1);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		    }
		});
     	
	 	button3.setOnMouseEntered(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				nivView.setImage(imageNiv3);			
			}
		});
        
	 	button3.setOnMouseExited(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				nivView.setImage(imageNivs);	

			}
		});
     	
     	button4.setOnMouseClicked(new EventHandler<MouseEvent>() { 
    	    public void handle(MouseEvent actionEvent) { 
		    	try {
					World.init("./Niveaux/Facile/Base/Base4.xml", "./Niveaux/Facile/Base/", 4);
					lancerLevels(1);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		    }
		});
     	
	 	button4.setOnMouseEntered(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				nivView.setImage(imageNiv4);			
			}
		});
        
	 	button4.setOnMouseExited(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				nivView.setImage(imageNivs);	

			}
		});
     	
     	grid5.getChildren().setAll(button1,button2,button3,button4);
     	
     	for(int i =0; i<nb_maps;i++){
     		System.out.println("DEBLOQUE");
	 		grid5.getChildren().get(i).setDisable(false);
	 	}
     	
		if(!button1.isDisable()){
			button1.setImage(bouton1Image2);			
		}
		if(!button2.isDisable()){
			button2.setImage(bouton2Image2);			
		}
		if(!button3.isDisable()){
			button3.setImage(bouton3Image2);			
		}
		if(!button4.isDisable()){
			button4.setImage(bouton4Image2);			
		}
     	
     	System.out.println(""+nb_maps);
     	System.out.println("taille "+grid5.getChildren().size());
     	

     	grid5.setTranslateX(winWidth/8);
     	vbox.getChildren().addAll(grid5, buttonReturn);
     	gridMain.getChildren().addAll(vbox, nivView);
     	root5.getChildren().addAll(titleView,gridMain);
     	
     	stage.setTitle("Lightbot - Baymax Edition - Choix du niveau (basique)");
     	stage.setScene(scn5);
        stage.getScene().getRoot().setStyle("-fx-background-image: url(./menu/couleur_fond2.png);");
        stg = stage;
        stage.show();
    }
    
    
    ///////////////////////////////
    /////////////////////////////
    //////////////////////////////
    /////////////////////////////////
    
    public static void niveauxFacilesIf(final Stage stage){
    	root6 = new Pane();
		//stage.setFullScreen(true);
		double winWidth = Screen.getPrimary().getVisualBounds().getWidth();
		double winHeight = Screen.getPrimary().getVisualBounds().getHeight();
		scn6 = new Scene(root6, winWidth, winHeight);
    	
    	org.jdom2.Document document = null; 
    	Element racine;
    	int nb_maps = 0;
    	SAXBuilder sxb = new SAXBuilder();
		try
		{ 
			document = sxb.build(new File("./Niveaux/Facile/If/sauv.xml"));
		}
		catch(Exception e){};
		
		racine = document.getRootElement();
		
		Element courant;
		List listDebloque = racine.getChildren("nb_maps_debloques");
		Iterator iteDebloque = listDebloque.iterator();
		while(iteDebloque.hasNext())
		{
			courant = (Element)iteDebloque.next();
			nb_maps = Integer.parseInt(courant.getAttributeValue("nb"));
		}
		////////////////////////
		///////////////////////
		//////////////////////////
		
		GridPane gridMain = new GridPane();
		
     	VBox vbox = new VBox();
     	vbox.setCursor(Cursor.HAND);
     	vbox.setTranslateY(winHeight*0.4);
     	vbox.setSpacing(winHeight/8);
     	
    	final Image imageNivs = new Image("./menu/if_levels.png"); 
    	final Image imageNiv1 = new Image("./menu/if_level1.png"); 
    	final Image imageNiv2 = new Image("./menu/if_level2.png"); 
    	final Image imageNiv3 = new Image("./menu/if_level3.png"); 
    	final Image imageNiv4 = new Image("./menu/if_level4.png"); 

		final ImageView nivView = new ImageView(imageNivs);
		nivView.setFitWidth(winWidth/2);
		nivView.setFitHeight(winHeight/2);
		nivView.setTranslateX(winWidth*0.325);
		nivView.setTranslateY(winHeight*0.3);

		GridPane.setConstraints(vbox, 0, 0);
	 	GridPane.setConstraints(nivView, 1, 0);
	 	
		GridPane grid6 = new GridPane();
		
		final Image imageTitle = new Image("./menu/choix_Niveau.png"); 
		final ImageView titleView = new ImageView(imageTitle); 
		titleView.setTranslateX(winWidth*0.2);
		titleView.setTranslateY(winHeight/7);

		titleView.setFitHeight(winHeight/14);
		titleView.setFitWidth(winWidth/2);
		
    	final Image imageRetour1 = new Image("./menu/bouton_Retour1.png"); 
    	final Image imageRetour2 = new Image("./menu/bouton_Retour2.png"); 
		final ImageView buttonReturn = new ImageView(imageRetour1);
		buttonReturn.setFitWidth(winWidth/7);
		buttonReturn.setFitHeight(winHeight/5);
		buttonReturn.setTranslateX(winWidth*0.14);

		
		buttonReturn.setOnMouseEntered(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				buttonReturn.setImage(imageRetour2);			
			}
		});
        
		buttonReturn.setOnMouseExited(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				buttonReturn.setImage(imageRetour1);	

			}
		});

    	buttonReturn.setOnMouseClicked(new EventHandler<MouseEvent>() { 
		   
		    public void handle(MouseEvent actionEvent) { 
		    	stage.setTitle("Lightbot - Baymax Edition");
		        stage.setScene(scn3);
		        stg = stage;
		        stage.show();
				//stage.setFullScreen(true);

		    }
		});
    	
		final Image bouton1Image1 = new Image("./menu/bouton_01_desactive.png"); 
		final Image bouton1Image2 = new Image("./menu/bouton_01_active.png"); 
		final Image bouton2Image1 = new Image("./menu/bouton_02_desactive.png"); 
		final Image bouton2Image2 = new Image("./menu/bouton_02_active.png"); 
		final Image bouton3Image1 = new Image("./menu/bouton_03_desactive.png"); 
		final Image bouton3Image2 = new Image("./menu/bouton_03_active.png"); 
		final Image bouton4Image1 = new Image("./menu/bouton_04_desactive.png"); 
		final Image bouton4Image2 = new Image("./menu/bouton_04_active.png"); 
		
		final ImageView button1 = new ImageView(bouton1Image1); 
		button1.setFitWidth(winWidth/12);
		button1.setFitHeight(winHeight/8);


		final ImageView button2 = new ImageView(bouton2Image1); 
		button2.setFitWidth(winWidth/12);
		button2.setFitHeight(winHeight/8);

		
		final ImageView button3 = new ImageView(bouton3Image1); 
		button3.setFitWidth(winWidth/12);
		button3.setFitHeight(winHeight/8);

		
		final ImageView button4 = new ImageView(bouton4Image1); 
		button4.setFitWidth(winWidth/12);
		button4.setFitHeight(winHeight/8);

		
	 	GridPane.setConstraints(button1, 0, 0);
	 	GridPane.setConstraints(button2, 1, 0);
	 	GridPane.setConstraints(button3, 0, 1);
	 	GridPane.setConstraints(button4, 1, 1);
	 	
	 	button1.setDisable(true);
	 	button2.setDisable(true);
	 	button3.setDisable(true);
	 	button4.setDisable(true);
	 	
	 	grid6.setHgap(30); 
	 	grid6.setVgap(30);

	 	button1.setOnMouseClicked(new EventHandler<MouseEvent>() {
		    public void handle(MouseEvent actionEvent) { 
		    	try {
					World.init("./Niveaux/Facile/If/If1.xml", "./Niveaux/Facile/If/", 1);
					lancerLevels(2);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		    }
		});
	 	button1.setOnMouseEntered(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				nivView.setImage(null);
				nivView.setImage(imageNiv1);			
			}
		});
        
	 	button1.setOnMouseExited(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				nivView.setImage(imageNivs);	

			}
		});
	 	
     	button2.setOnMouseClicked(new EventHandler<MouseEvent>() { 
    	    public void handle(MouseEvent actionEvent) { 
		    	try {
					World.init("./Niveaux/Facile/If/If2.xml", "./Niveaux/Facile/If/", 2);
					lancerLevels(2);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		    }
		});
	 	button2.setOnMouseEntered(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				nivView.setImage(imageNiv2);			
			}
		});
        
	 	button2.setOnMouseExited(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				nivView.setImage(imageNivs);	

			}
		});
	 	
	 	
     	button3.setOnMouseClicked(new EventHandler<MouseEvent>() { 
    	    public void handle(MouseEvent actionEvent) { 
		    	try {
					World.init("./Niveaux/Facile/If/If3.xml", "./Niveaux/Facile/If/", 3);
					lancerLevels(2);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		    }
		});
	 	button3.setOnMouseEntered(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				nivView.setImage(imageNiv3);			
			}
		});
        
	 	button3.setOnMouseExited(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				nivView.setImage(imageNivs);	

			}
		});
	 	
	 	
     	button4.setOnMouseClicked(new EventHandler<MouseEvent>() { 
    	    public void handle(MouseEvent actionEvent) { 
		    	try {
					World.init("./Niveaux/Facile/If/If4.xml", "./Niveaux/Facile/If/", 4);
					lancerLevels(2);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		    }
		});
	 	button4.setOnMouseEntered(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				nivView.setImage(imageNiv4);			
			}
		});
        
	 	button4.setOnMouseExited(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				nivView.setImage(imageNivs);	

			}
		});
	 	
	 	
     	grid6.getChildren().setAll(button1,button2,button3,button4);
     	
     	for(int i =0; i<nb_maps;i++){
     		grid6.getChildren().get(i).setDisable(false);
	 	}
     	
		if(!button1.isDisable()){
			button1.setImage(bouton1Image2);			
		}
		if(!button2.isDisable()){
			button2.setImage(bouton2Image2);			
		}
		if(!button3.isDisable()){
			button3.setImage(bouton3Image2);			
		}
		if(!button4.isDisable()){
			button4.setImage(bouton4Image2);			
		}
     	
     	grid6.setTranslateX(winWidth/8);
     	vbox.getChildren().addAll(grid6, buttonReturn);
     	gridMain.getChildren().addAll(vbox, nivView);
     	root6.getChildren().addAll(titleView,gridMain);
     	
     	stage.setTitle("Lightbot - Baymax Edition - Choix du niveau (If then else)");
     	stage.setScene(scn6);
        stage.getScene().getRoot().setStyle("-fx-background-image: url(./menu/couleur_fond2.png);");
     	stg = stage;
     	stage.show();
    }
  
    public static void niveauxFacilesTeleport(final Stage stage){
    	
		//stage.setFullScreen(true);
    	root7 = new Pane();
		double winWidth = Screen.getPrimary().getVisualBounds().getWidth();
		double winHeight = Screen.getPrimary().getVisualBounds().getHeight();
		scn7 = new Scene(root7, winWidth, winHeight);
		
    	org.jdom2.Document document = null; 
    	Element racine;
    	int nb_maps = 0;
    	SAXBuilder sxb = new SAXBuilder();
		try
		{ 
			document = sxb.build(new File("./Niveaux/Facile/Teleport/sauv.xml"));
		}
		catch(Exception e){};
		
		racine = document.getRootElement();
		
		Element courant;
		List listDebloque = racine.getChildren("nb_maps_debloques");
		Iterator iteDebloque = listDebloque.iterator();
		while(iteDebloque.hasNext())
		{
			courant = (Element)iteDebloque.next();
			nb_maps = Integer.parseInt(courant.getAttributeValue("nb"));
		}
		//////////////////////////////
		///////////////////////////////
		//////////////////////////////
		//////////////////////
		/////////////////////////////////////
		
		GridPane gridMain = new GridPane();
		
     	VBox vbox = new VBox();
     	vbox.setCursor(Cursor.HAND);
     	vbox.setTranslateY(winHeight*0.4);
     	vbox.setSpacing(winHeight/8);
     	
    	final Image imageNivs = new Image("./menu/teleport_levels.png"); 
    	final Image imageNiv1 = new Image("./menu/teleport_level1.png"); 
    	final Image imageNiv2 = new Image("./menu/teleport_level2.png"); 
    	final Image imageNiv3 = new Image("./menu/teleport_level3.png"); 
    	final Image imageNiv4 = new Image("./menu/teleport_level4.png"); 

		final ImageView nivView = new ImageView(imageNivs);
		nivView.setFitWidth(winWidth/2);
		nivView.setFitHeight(winHeight/2);
		nivView.setTranslateX(winWidth*0.325);
		nivView.setTranslateY(winHeight*0.3);

		GridPane.setConstraints(vbox, 0, 0);
	 	GridPane.setConstraints(nivView, 1, 0);
	 	
    	GridPane grid7 = new GridPane();

		final Image imageTitle = new Image("./menu/choix_Niveau.png"); 
		final ImageView titleView = new ImageView(imageTitle); 
		titleView.setTranslateX(winWidth*0.2);
		titleView.setTranslateY(winHeight/7);

		titleView.setFitHeight(winHeight/14);
		titleView.setFitWidth(winWidth/2);
		
    	final Image imageRetour1 = new Image("./menu/bouton_Retour1.png"); 
    	final Image imageRetour2 = new Image("./menu/bouton_Retour2.png"); 
		final ImageView buttonReturn = new ImageView(imageRetour1);
		buttonReturn.setFitWidth(winWidth/7);
		buttonReturn.setFitHeight(winHeight/5);
		buttonReturn.setTranslateX(winWidth*0.14);
		
		buttonReturn.setOnMouseEntered(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				buttonReturn.setImage(imageRetour2);			
			}
		});
        
		buttonReturn.setOnMouseExited(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				buttonReturn.setImage(imageRetour1);	

			}
		});
		
    	buttonReturn.setOnMouseClicked(new EventHandler<MouseEvent>() { 
		   
		    public void handle(MouseEvent actionEvent) { 
		    	stage.setTitle("Lightbot - Baymax Edition");
		        stage.setScene(scn3);
		        stg = stage;
		        stage.show();
		    }
		});
		final Image bouton1Image1 = new Image("./menu/bouton_01_desactive.png"); 
		final Image bouton1Image2 = new Image("./menu/bouton_01_active.png"); 
		final Image bouton2Image1 = new Image("./menu/bouton_02_desactive.png"); 
		final Image bouton2Image2 = new Image("./menu/bouton_02_active.png"); 
		final Image bouton3Image1 = new Image("./menu/bouton_03_desactive.png"); 
		final Image bouton3Image2 = new Image("./menu/bouton_03_active.png"); 
		final Image bouton4Image1 = new Image("./menu/bouton_04_desactive.png"); 
		final Image bouton4Image2 = new Image("./menu/bouton_04_active.png"); 
		
		final ImageView button1 = new ImageView(bouton1Image1); 
		button1.setFitWidth(winWidth/12);
		button1.setFitHeight(winHeight/8);


		final ImageView button2 = new ImageView(bouton2Image1); 
		button2.setFitWidth(winWidth/12);
		button2.setFitHeight(winHeight/8);

		
		final ImageView button3 = new ImageView(bouton3Image1); 
		button3.setFitWidth(winWidth/12);
		button3.setFitHeight(winHeight/8);

		
		final ImageView button4 = new ImageView(bouton4Image1); 
		button4.setFitWidth(winWidth/12);
		button4.setFitHeight(winHeight/8);

		
	 	GridPane.setConstraints(button1, 0, 0);
	 	GridPane.setConstraints(button2, 1, 0);
	 	GridPane.setConstraints(button3, 0, 1);
	 	GridPane.setConstraints(button4, 1, 1);
	 	
	 	button1.setDisable(true);
	 	button2.setDisable(true);
	 	button3.setDisable(true);
	 	button4.setDisable(true);
	 	
	 	grid7.setHgap(30); 
	 	grid7.setVgap(30);
	 	button1.setOnMouseClicked(new EventHandler<MouseEvent>() {
		    public void handle(MouseEvent actionEvent) { 
		    	try {
					World.init("./Niveaux/Facile/Teleport/Teleport1.xml", "./Niveaux/Facile/Teleport/", 1);
					lancerLevels(3);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		    }
		});

	 	button1.setOnMouseEntered(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				nivView.setImage(null);
				nivView.setImage(imageNiv1);			
			}
		});
        
	 	button1.setOnMouseExited(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				nivView.setImage(imageNivs);	

			}
		});
	 	
     	button2.setOnMouseClicked(new EventHandler<MouseEvent>() { 
    	    public void handle(MouseEvent actionEvent) { 
		    	try {
					World.init("./Niveaux/Facile/Teleport/Teleport2.xml", "./Niveaux/Facile/Teleport/", 2);
					lancerLevels(3);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		    }
		});
	 	button2.setOnMouseEntered(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				nivView.setImage(imageNiv2);			
			}
		});
        
	 	button2.setOnMouseExited(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				nivView.setImage(imageNivs);	

			}
		});
	 	
     	button3.setOnMouseClicked(new EventHandler<MouseEvent>() { 
    	    public void handle(MouseEvent actionEvent) { 
		    	try {
					World.init("./Niveaux/Facile/Teleport/Teleport3.xml", "./Niveaux/Facile/Teleport/", 3);
					lancerLevels(3);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		    }
		});
	 	button3.setOnMouseEntered(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				nivView.setImage(imageNiv3);			
			}
		});
        
	 	button3.setOnMouseExited(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				nivView.setImage(imageNivs);	

			}
		});
	 	
     	button4.setOnMouseClicked(new EventHandler<MouseEvent>() { 
    	    public void handle(MouseEvent actionEvent) { 
		    	try {
					World.init("./Niveaux/Facile/Teleport/Teleport4.xml", "./Niveaux/Facile/Teleport/", 4);
					lancerLevels(3);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		    }
		});
	 	button4.setOnMouseEntered(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				nivView.setImage(imageNiv4);			
			}
		});
        
	 	button4.setOnMouseExited(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				nivView.setImage(imageNivs);	

			}
		});
	 	
     	grid7.getChildren().setAll(button1,button2,button3,button4);
     	
     	for(int i =0; i<nb_maps;i++){
     		grid7.getChildren().get(i).setDisable(false);
	 	}
		if(!button1.isDisable()){
			button1.setImage(bouton1Image2);			
		}
		if(!button2.isDisable()){
			button2.setImage(bouton2Image2);			
		}
		if(!button3.isDisable()){
			button3.setImage(bouton3Image2);			
		}
		if(!button4.isDisable()){
			button4.setImage(bouton4Image2);			
		}
     	
     	grid7.setTranslateX(winWidth/8);
     	vbox.getChildren().addAll(grid7, buttonReturn);
     	gridMain.getChildren().addAll(vbox, nivView);
     	root7.getChildren().addAll(titleView,gridMain);
     	
     	stage.setTitle("Lightbot - Baymax Edition - Choix du niveau (Teleportation)");
     	stage.setScene(scn7);
        stage.getScene().getRoot().setStyle("-fx-background-image: url(./menu/couleur_fond2.png);");
     	stg = stage;
     	stage.show();
    }
    
    public static void niveauxFacilesBreak(final Stage stage){
    	root8 = new Pane();
    	//stage.setFullScreen(true);
		double winWidth = Screen.getPrimary().getVisualBounds().getWidth();
		double winHeight = Screen.getPrimary().getVisualBounds().getHeight();
		scn8 = new Scene(root8, winWidth, winHeight);
    	org.jdom2.Document document = null; 
    	Element racine;
    	int nb_maps = 0;
    	SAXBuilder sxb = new SAXBuilder();
		try
		{ 
			document = sxb.build(new File("./Niveaux/Facile/Break/sauv.xml"));
		}
		catch(Exception e){};
		
		racine = document.getRootElement();
		
		Element courant;
		List listDebloque = racine.getChildren("nb_maps_debloques");
		Iterator iteDebloque = listDebloque.iterator();
		while(iteDebloque.hasNext())
		{
			courant = (Element)iteDebloque.next();
			nb_maps = Integer.parseInt(courant.getAttributeValue("nb"));
		}
    	/////////////////////
		/////////////
		/////////////////////

		GridPane gridMain = new GridPane();
		
     	VBox vbox = new VBox();
     	vbox.setCursor(Cursor.HAND);
     	vbox.setTranslateY(winHeight*0.4);
     	vbox.setSpacing(winHeight/8);
     	
    	final Image imageNivs = new Image("./menu/break_levels.png"); 
    	final Image imageNiv1 = new Image("./menu/break_level1.png"); 
    	final Image imageNiv2 = new Image("./menu/break_level2.png"); 
    	final Image imageNiv3 = new Image("./menu/break_level3.png"); 
    	final Image imageNiv4 = new Image("./menu/break_level4.png"); 

		final ImageView nivView = new ImageView(imageNivs);
		nivView.setFitWidth(winWidth/2);
		nivView.setFitHeight(winHeight/2);
		nivView.setTranslateX(winWidth*0.325);
		nivView.setTranslateY(winHeight*0.3);

		GridPane.setConstraints(vbox, 0, 0);
	 	GridPane.setConstraints(nivView, 1, 0);
	 	
    	GridPane grid8 = new GridPane();

		final Image imageTitle = new Image("./menu/choix_Niveau.png"); 
		final ImageView titleView = new ImageView(imageTitle); 
		titleView.setTranslateX(winWidth*0.2);
		titleView.setTranslateY(winHeight/7);
		titleView.setFitHeight(winHeight/14);
		titleView.setFitWidth(winWidth/2);
		
    	final Image imageRetour1 = new Image("./menu/bouton_Retour1.png"); 
    	final Image imageRetour2 = new Image("./menu/bouton_Retour2.png"); 
		final ImageView buttonReturn = new ImageView(imageRetour1);
		buttonReturn.setFitWidth(winWidth/7);
		buttonReturn.setFitHeight(winHeight/5);
		buttonReturn.setTranslateX(winWidth*0.14);
		buttonReturn.setOnMouseEntered(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				buttonReturn.setImage(imageRetour2);			
			}
		});
        
		buttonReturn.setOnMouseExited(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				buttonReturn.setImage(imageRetour1);	

			}
		});
		
    	buttonReturn.setOnMouseClicked(new EventHandler<MouseEvent>() { 
		   
		    public void handle(MouseEvent actionEvent) { 
		    	stage.setTitle("Lightbot - Baymax Edition");
		        stage.setScene(scn3);
		        stg = stage;
		        stage.show();
		    }
		});
		final Image bouton1Image1 = new Image("./menu/bouton_01_desactive.png"); 
		final Image bouton1Image2 = new Image("./menu/bouton_01_active.png"); 
		final Image bouton2Image1 = new Image("./menu/bouton_02_desactive.png"); 
		final Image bouton2Image2 = new Image("./menu/bouton_02_active.png"); 
		final Image bouton3Image1 = new Image("./menu/bouton_03_desactive.png"); 
		final Image bouton3Image2 = new Image("./menu/bouton_03_active.png"); 
		final Image bouton4Image1 = new Image("./menu/bouton_04_desactive.png"); 
		final Image bouton4Image2 = new Image("./menu/bouton_04_active.png"); 
		
		final ImageView button1 = new ImageView(bouton1Image1); 
		button1.setFitWidth(winWidth/12);
		button1.setFitHeight(winHeight/8);


		final ImageView button2 = new ImageView(bouton2Image1); 
		button2.setFitWidth(winWidth/12);
		button2.setFitHeight(winHeight/8);

		
		final ImageView button3 = new ImageView(bouton3Image1); 
		button3.setFitWidth(winWidth/12);
		button3.setFitHeight(winHeight/8);

		
		final ImageView button4 = new ImageView(bouton4Image1); 
		button4.setFitWidth(winWidth/12);
		button4.setFitHeight(winHeight/8);

		
	 	GridPane.setConstraints(button1, 0, 0);
	 	GridPane.setConstraints(button2, 1, 0);
	 	GridPane.setConstraints(button3, 0, 1);
	 	GridPane.setConstraints(button4, 1, 1);
	 	
	 	button1.setDisable(true);
	 	button2.setDisable(true);
	 	button3.setDisable(true);
	 	button4.setDisable(true);
	 	grid8.setHgap(30); 
	 	grid8.setVgap(30);
	 	button1.setOnMouseClicked(new EventHandler<MouseEvent>() {
		    public void handle(MouseEvent actionEvent) { 
		    	try {
					World.init("./Niveaux/Facile/Break/Break1.xml", "./Niveaux/Facile/Break/", 1);
					lancerLevels(4);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		    }
		});
	 	button1.setOnMouseEntered(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				nivView.setImage(null);
				nivView.setImage(imageNiv1);			
			}
		});
        
	 	button1.setOnMouseExited(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				nivView.setImage(imageNivs);	

			}
		});
	 	
     	button2.setOnMouseClicked(new EventHandler<MouseEvent>() { 
    	    public void handle(MouseEvent actionEvent) { 
		    	try {
					World.init("./Niveaux/Facile/Break/Break2.xml", "./Niveaux/Facile/Break/", 2);
					lancerLevels(4);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		    }
		});
	 	button2.setOnMouseEntered(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				nivView.setImage(imageNiv2);			
			}
		});
        
	 	button2.setOnMouseExited(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				nivView.setImage(imageNivs);	

			}
		});
	 	
     	button3.setOnMouseClicked(new EventHandler<MouseEvent>() { 
    	    public void handle(MouseEvent actionEvent) { 
		    	try {
					World.init("./Niveaux/Facile/Break/Break3.xml", "./Niveaux/Facile/Break/", 3);
					lancerLevels(4);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		    }
		});
	 	button3.setOnMouseEntered(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				nivView.setImage(imageNiv3);			
			}
		});
        
	 	button3.setOnMouseExited(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				nivView.setImage(imageNivs);	

			}
		});
	 	
     	button4.setOnMouseClicked(new EventHandler<MouseEvent>() { 
    	    public void handle(MouseEvent actionEvent) { 
		    	try {
					World.init("./Niveaux/Facile/Break/Break4.xml", "./Niveaux/Facile/Break/", 4);
					lancerLevels(4);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		    }
		});
	 	button4.setOnMouseEntered(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				nivView.setImage(imageNiv4);			
			}
		});
        
	 	button4.setOnMouseExited(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				nivView.setImage(imageNivs);	

			}
		});
	 	
     	grid8.getChildren().setAll(button1,button2,button3,button4);
     	
     	for(int i =0; i<nb_maps;i++){
     		grid8.getChildren().get(i).setDisable(false);
	 	}

		if(!button1.isDisable()){
			button1.setImage(bouton1Image2);			
		}
		if(!button2.isDisable()){
			button2.setImage(bouton2Image2);			
		}
		if(!button3.isDisable()){
			button3.setImage(bouton3Image2);			
		}
		if(!button4.isDisable()){
			button4.setImage(bouton4Image2);			
		}
     	
     	grid8.setTranslateX(winWidth/8);
     	vbox.getChildren().addAll(grid8, buttonReturn);
     	gridMain.getChildren().addAll(vbox, nivView);
     	root8.getChildren().addAll(titleView,gridMain);
     	stage.setTitle("Lightbot - Baymax Edition - Choix du niveau (Break)");
     	stage.setScene(scn8);
        stage.getScene().getRoot().setStyle("-fx-background-image: url(./menu/couleur_fond2.png);");
     	stg = stage;
     	stage.show();
    }
  
    public static void niveauxMoyensFlashback(final Stage stage){
		//stage.setFullScreen(true);
    	root9 = new Pane();
		double winWidth = Screen.getPrimary().getVisualBounds().getWidth();
		double winHeight = Screen.getPrimary().getVisualBounds().getHeight();
		scn9 = new Scene(root9, winWidth, winHeight);
    	
    	org.jdom2.Document document = null; 
    	Element racine;
    	int nb_maps = 0;
    	SAXBuilder sxb = new SAXBuilder();
		try
		{ 
			document = sxb.build(new File("./Niveaux/Moyen/Flashback/sauv.xml"));
		}
		catch(Exception e){};
		
		racine = document.getRootElement();
		
		Element courant;
		List listDebloque = racine.getChildren("nb_maps_debloques");
		Iterator iteDebloque = listDebloque.iterator();
		while(iteDebloque.hasNext())
		{
			courant = (Element)iteDebloque.next();
			nb_maps = Integer.parseInt(courant.getAttributeValue("nb"));
		}

		GridPane gridMain = new GridPane();
		
     	VBox vbox = new VBox();
     	vbox.setCursor(Cursor.HAND);
     	vbox.setTranslateY(winHeight*0.4);
     	vbox.setSpacing(winHeight/8);
     	
    	final Image imageNivs = new Image("./menu/flashback_levels.png"); 
    	final Image imageNiv1 = new Image("./menu/flashback_level1.png"); 
    	final Image imageNiv2 = new Image("./menu/flashback_level2.png"); 
    	final Image imageNiv3 = new Image("./menu/flashback_level3.png"); 
    	final Image imageNiv4 = new Image("./menu/flashback_level4.png"); 

		final ImageView nivView = new ImageView(imageNivs);
		nivView.setFitWidth(winWidth/2);
		nivView.setFitHeight(winHeight/2);
		nivView.setTranslateX(winWidth*0.325);
		nivView.setTranslateY(winHeight*0.3);

		GridPane.setConstraints(vbox, 0, 0);
	 	GridPane.setConstraints(nivView, 1, 0);
    	
		GridPane grid9 = new GridPane();
    	

		final Image imageTitle = new Image("./menu/choix_Niveau.png"); 
		final ImageView titleView = new ImageView(imageTitle); 
		titleView.setTranslateX(winWidth*0.2);
		titleView.setTranslateY(winHeight/7);
		titleView.setFitHeight(winHeight/14);
		titleView.setFitWidth(winWidth/2);
		
    	final Image imageRetour1 = new Image("./menu/bouton_Retour1.png"); 
    	final Image imageRetour2 = new Image("./menu/bouton_Retour2.png"); 
		final ImageView buttonReturn = new ImageView(imageRetour1);
		buttonReturn.setFitWidth(winWidth/7);
		buttonReturn.setFitHeight(winHeight/5);
		buttonReturn.setTranslateX(winWidth*0.14);
		buttonReturn.setOnMouseEntered(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				buttonReturn.setImage(imageRetour2);			
			}
		});
        
		buttonReturn.setOnMouseExited(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				buttonReturn.setImage(imageRetour1);	

			}
		});
		
    	buttonReturn.setOnMouseClicked(new EventHandler<MouseEvent>() { 
		   
		    public void handle(MouseEvent actionEvent) { 
		    	stage.setTitle("Lightbot - Choix du mode");
		        stage.setScene(scn4);
		        stg = stage;
		        stage.show();
		    }
		});
		final Image bouton1Image1 = new Image("./menu/bouton_01_desactive.png"); 
		final Image bouton1Image2 = new Image("./menu/bouton_01_active.png"); 
		final Image bouton2Image1 = new Image("./menu/bouton_02_desactive.png"); 
		final Image bouton2Image2 = new Image("./menu/bouton_02_active.png"); 
		final Image bouton3Image1 = new Image("./menu/bouton_03_desactive.png"); 
		final Image bouton3Image2 = new Image("./menu/bouton_03_active.png"); 
		final Image bouton4Image1 = new Image("./menu/bouton_04_desactive.png"); 
		final Image bouton4Image2 = new Image("./menu/bouton_04_active.png"); 
		
		final ImageView button1 = new ImageView(bouton1Image1); 
		button1.setFitWidth(winWidth/12);
		button1.setFitHeight(winHeight/8);


		final ImageView button2 = new ImageView(bouton2Image1); 
		button2.setFitWidth(winWidth/12);
		button2.setFitHeight(winHeight/8);

		
		final ImageView button3 = new ImageView(bouton3Image1); 
		button3.setFitWidth(winWidth/12);
		button3.setFitHeight(winHeight/8);

		
		final ImageView button4 = new ImageView(bouton4Image1); 
		button4.setFitWidth(winWidth/12);
		button4.setFitHeight(winHeight/8);

		
	 	GridPane.setConstraints(button1, 0, 0);
	 	GridPane.setConstraints(button2, 1, 0);
	 	GridPane.setConstraints(button3, 0, 1);
	 	GridPane.setConstraints(button4, 1, 1);
	 	
	 	button1.setDisable(true);
	 	button2.setDisable(true);
	 	button3.setDisable(true);
	 	button4.setDisable(true);
	 	grid9.setHgap(30); 
	 	grid9.setVgap(30);
	 	button1.setOnMouseClicked(new EventHandler<MouseEvent>() {
		    public void handle(MouseEvent actionEvent) { 
		    	try {
					World.init("./Niveaux/Moyen/Flashback/Flashback1.xml", "./Niveaux/Moyen/Flashback/", 1);
					lancerLevels(5);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		    }
		});
	 	button1.setOnMouseEntered(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				nivView.setImage(null);
				nivView.setImage(imageNiv1);			
			}
		});
        
	 	button1.setOnMouseExited(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				nivView.setImage(imageNivs);	

			}
		});
	 	
     	button2.setOnMouseClicked(new EventHandler<MouseEvent>() { 
    	    public void handle(MouseEvent actionEvent) { 
		    	try {
					World.init("./Niveaux/Moyen/Flashback/Flashback2.xml", "./Niveaux/Moyen/Flashback/", 2);
					lancerLevels(5);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		    }
		});
	 	button2.setOnMouseEntered(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				nivView.setImage(imageNiv2);			
			}
		});
        
	 	button2.setOnMouseExited(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				nivView.setImage(imageNivs);	

			}
		});
	 	
     	button3.setOnMouseClicked(new EventHandler<MouseEvent>() { 
    	    public void handle(MouseEvent actionEvent) { 
		    	try {
					World.init("./Niveaux/Moyen/Flashback/Flashback3.xml", "./Niveaux/Moyen/Flashback/", 3);
					lancerLevels(5);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		    }
		});
	 	button3.setOnMouseEntered(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				nivView.setImage(imageNiv3);			
			}
		});
        
	 	button3.setOnMouseExited(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				nivView.setImage(imageNivs);	

			}
		});
	 	
     	button4.setOnMouseClicked(new EventHandler<MouseEvent>() { 
    	    public void handle(MouseEvent actionEvent) { 
		    	try {
					World.init("./Niveaux/Moyen/Flashback/Flashback4.xml", "./Niveaux/Moyen/Flashback/", 4);
					lancerLevels(5);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		    }
		});
	 	button4.setOnMouseEntered(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				nivView.setImage(imageNiv4);			
			}
		});
        
	 	button4.setOnMouseExited(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				nivView.setImage(imageNivs);	

			}
		});
	 	
     	grid9.getChildren().setAll(button1,button2,button3,button4);
     	
     	for(int i =0; i<nb_maps;i++){
     		grid9.getChildren().get(i).setDisable(false);
	 	}


		if(!button1.isDisable()){
			button1.setImage(bouton1Image2);			
		}
		if(!button2.isDisable()){
			button2.setImage(bouton2Image2);			
		}
		if(!button3.isDisable()){
			button3.setImage(bouton3Image2);			
		}
		if(!button4.isDisable()){
			button4.setImage(bouton4Image2);			
		}
     	
     	grid9.setTranslateX(winWidth/8);
     	vbox.getChildren().addAll(grid9, buttonReturn);
     	gridMain.getChildren().addAll(vbox, nivView);
     	root9.getChildren().addAll(titleView,gridMain);
     	stage.setTitle("Lightbot - Baymax Edition - Choix du niveau (Flashback)");
     	stage.setScene(scn9);
        stage.getScene().getRoot().setStyle("-fx-background-image: url(./menu/couleur_fond2.png);");
     	stg = stage;
     	stage.show();
    }
    
    public static void niveauxMoyensFor(final Stage stage){
		//stage.setFullScreen(true);
    	root10 = new Pane();
		double winWidth = Screen.getPrimary().getVisualBounds().getWidth();
		double winHeight = Screen.getPrimary().getVisualBounds().getHeight();
		scn10 = new Scene(root10, winWidth, winHeight);
    	
    	org.jdom2.Document document = null; 
    	Element racine;
    	int nb_maps = 0;
    	SAXBuilder sxb = new SAXBuilder();
		try
		{ 
			document = sxb.build(new File("./Niveaux/Moyen/For/sauv.xml"));
		}
		catch(Exception e){};
		
		racine = document.getRootElement();
		
		Element courant;
		List listDebloque = racine.getChildren("nb_maps_debloques");
		Iterator iteDebloque = listDebloque.iterator();
		while(iteDebloque.hasNext())
		{
			courant = (Element)iteDebloque.next();
			nb_maps = Integer.parseInt(courant.getAttributeValue("nb"));
		}
    	///////////////////////
		////////////////////
		///////////////////////

		GridPane gridMain = new GridPane();
		
     	VBox vbox = new VBox();
     	vbox.setCursor(Cursor.HAND);
     	vbox.setTranslateY(winHeight*0.4);
     	vbox.setSpacing(winHeight/8);
     	
    	final Image imageNivs = new Image("./menu/for_levels.png"); 
    	final Image imageNiv1 = new Image("./menu/for_level1.png"); 
    	final Image imageNiv2 = new Image("./menu/for_level2.png"); 
    	final Image imageNiv3 = new Image("./menu/for_level3.png"); 
    	final Image imageNiv4 = new Image("./menu/for_level4.png"); 

		final ImageView nivView = new ImageView(imageNivs);
		nivView.setFitWidth(winWidth/2);
		nivView.setFitHeight(winHeight/2);
		nivView.setTranslateX(winWidth*0.325);
		nivView.setTranslateY(winHeight*0.3);

		GridPane.setConstraints(vbox, 0, 0);
	 	GridPane.setConstraints(nivView, 1, 0);
	 	
    	GridPane grid10 = new GridPane();

		final Image imageTitle = new Image("./menu/choix_Niveau.png"); 
		final ImageView titleView = new ImageView(imageTitle); 
		titleView.setTranslateX(winWidth*0.2);
		titleView.setTranslateY(winHeight/7);
		titleView.setFitHeight(winHeight/14);
		titleView.setFitWidth(winWidth/2);
		
    	final Image imageRetour1 = new Image("./menu/bouton_Retour1.png"); 
    	final Image imageRetour2 = new Image("./menu/bouton_Retour2.png"); 
		final ImageView buttonReturn = new ImageView(imageRetour1);
		buttonReturn.setFitWidth(winWidth/7);
		buttonReturn.setFitHeight(winHeight/5);
		buttonReturn.setTranslateX(winWidth*0.14);
		buttonReturn.setOnMouseEntered(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				buttonReturn.setImage(imageRetour2);			
			}
		});
        
		buttonReturn.setOnMouseExited(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				buttonReturn.setImage(imageRetour1);	

			}
		});
		
    	buttonReturn.setOnMouseClicked(new EventHandler<MouseEvent>() { 
		   
		    public void handle(MouseEvent actionEvent) { 
		    	stage.setTitle("Lightbot - Baymax Edition");
		        stage.setScene(scn4);
		        stg = stage;
		        stage.show();
		    }
		});

		final Image bouton1Image1 = new Image("./menu/bouton_01_desactive.png"); 
		final Image bouton1Image2 = new Image("./menu/bouton_01_active.png"); 
		final Image bouton2Image1 = new Image("./menu/bouton_02_desactive.png"); 
		final Image bouton2Image2 = new Image("./menu/bouton_02_active.png"); 
		final Image bouton3Image1 = new Image("./menu/bouton_03_desactive.png"); 
		final Image bouton3Image2 = new Image("./menu/bouton_03_active.png"); 
		final Image bouton4Image1 = new Image("./menu/bouton_04_desactive.png"); 
		final Image bouton4Image2 = new Image("./menu/bouton_04_active.png"); 
		
		final ImageView button1 = new ImageView(bouton1Image1); 
		button1.setFitWidth(winWidth/12);
		button1.setFitHeight(winHeight/8);


		final ImageView button2 = new ImageView(bouton2Image1); 
		button2.setFitWidth(winWidth/12);
		button2.setFitHeight(winHeight/8);

		
		final ImageView button3 = new ImageView(bouton3Image1); 
		button3.setFitWidth(winWidth/12);
		button3.setFitHeight(winHeight/8);

		
		final ImageView button4 = new ImageView(bouton4Image1); 
		button4.setFitWidth(winWidth/12);
		button4.setFitHeight(winHeight/8);

		
	 	GridPane.setConstraints(button1, 0, 0);
	 	GridPane.setConstraints(button2, 1, 0);
	 	GridPane.setConstraints(button3, 0, 1);
	 	GridPane.setConstraints(button4, 1, 1);
	 	
	 	button1.setDisable(true);
	 	button2.setDisable(true);
	 	button3.setDisable(true);
	 	button4.setDisable(true);
	 	grid10.setHgap(30); 
	 	grid10.setVgap(30);
	 	button1.setOnMouseClicked(new EventHandler<MouseEvent>() {
		    public void handle(MouseEvent actionEvent) { 
		    	try {
					World.init("./Niveaux/Moyen/For/For1.xml", "./Niveaux/Moyen/For/", 1);
					lancerLevels(6);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		    }
		});
	 	button1.setOnMouseEntered(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				nivView.setImage(null);
				nivView.setImage(imageNiv1);			
			}
		});
        
	 	button1.setOnMouseExited(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				nivView.setImage(imageNivs);	

			}
		});
	 	
     	button2.setOnMouseClicked(new EventHandler<MouseEvent>() { 
    	    public void handle(MouseEvent actionEvent) { 
		    	try {
					World.init("./Niveaux/Moyen/For/For2.xml", "./Niveaux/Moyen/For/", 2);
					lancerLevels(6);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		    }
		});
	 	button2.setOnMouseEntered(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				nivView.setImage(imageNiv2);			
			}
		});
        
	 	button2.setOnMouseExited(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				nivView.setImage(imageNivs);	

			}
		});
	 	
     	button3.setOnMouseClicked(new EventHandler<MouseEvent>() { 
    	    public void handle(MouseEvent actionEvent) { 
		    	try {
					World.init("./Niveaux/Moyen/For/For3.xml", "./Niveaux/Moyen/For/", 3);
					lancerLevels(6);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		    }
		});
	 	button3.setOnMouseEntered(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				nivView.setImage(imageNiv3);			
			}
		});
        
	 	button3.setOnMouseExited(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				nivView.setImage(imageNivs);	

			}
		});
	 	
     	button4.setOnMouseClicked(new EventHandler<MouseEvent>() { 
    	    public void handle(MouseEvent actionEvent) { 
		    	try {
					World.init("./Niveaux/Moyen/For/For4.xml", "./Niveaux/Moyen/For/", 4);
					lancerLevels(6);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		    }
		});
	 	button4.setOnMouseEntered(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				nivView.setImage(imageNiv4);			
			}
		});
        
	 	button4.setOnMouseExited(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				nivView.setImage(imageNivs);	

			}
		});
	 	
     	grid10.getChildren().setAll(button1,button2,button3,button4);
     	
     	for(int i =0; i<nb_maps;i++){
     		grid10.getChildren().get(i).setDisable(false);
	 	}


		if(!button1.isDisable()){
			button1.setImage(bouton1Image2);			
		}
		if(!button2.isDisable()){
			button2.setImage(bouton2Image2);			
		}
		if(!button3.isDisable()){
			button3.setImage(bouton3Image2);			
		}
		if(!button4.isDisable()){
			button4.setImage(bouton4Image2);			
		}
     	
     	grid10.setTranslateX(winWidth/8);
     	vbox.getChildren().addAll(grid10, buttonReturn);
     	gridMain.getChildren().addAll(vbox, nivView);
     	root10.getChildren().addAll(titleView,gridMain);
     	stage.setTitle("Lightbot - Baymax Edition - Choix du niveau (For)");
     	stage.setScene(scn10);
        stage.getScene().getRoot().setStyle("-fx-background-image: url(./menu/couleur_fond2.png);");
     	stg = stage;
     	stage.show();
    }
    
    public static void niveauxMoyens2Bots(final Stage stage){
    	
		//stage.setFullScreen(true);
    	root11 = new Pane();
		double winWidth = Screen.getPrimary().getVisualBounds().getWidth();
		double winHeight = Screen.getPrimary().getVisualBounds().getHeight();
		scn11 = new Scene(root11, winWidth, winHeight);
    	
    	org.jdom2.Document document = null; 
    	Element racine;
    	int nb_maps = 0;
    	SAXBuilder sxb = new SAXBuilder();
		try
		{ 
			document = sxb.build(new File("./Niveaux/Moyen/2bots/sauv.xml"));
		}
		catch(Exception e){};
		
		racine = document.getRootElement();
		
		Element courant;
		List listDebloque = racine.getChildren("nb_maps_debloques");
		Iterator iteDebloque = listDebloque.iterator();
		while(iteDebloque.hasNext())
		{
			courant = (Element)iteDebloque.next();
			nb_maps = Integer.parseInt(courant.getAttributeValue("nb"));
		}
    	/////////////////////////////////////
		///////////////////////
		////////////////////////////////////

		GridPane gridMain = new GridPane();
		
     	VBox vbox = new VBox();
     	vbox.setCursor(Cursor.HAND);
     	vbox.setTranslateY(winHeight*0.4);
     	vbox.setSpacing(winHeight/8);
     	
    	final Image imageNivs = new Image("./menu/2bots_levels.png"); 
    	final Image imageNiv1 = new Image("./menu/2bots_level1.png"); 
    	final Image imageNiv2 = new Image("./menu/2bots_level2.png"); 
    	final Image imageNiv3 = new Image("./menu/2bots_level3.png"); 
    	final Image imageNiv4 = new Image("./menu/2bots_level4.png"); 

		final ImageView nivView = new ImageView(imageNivs);
		nivView.setFitWidth(winWidth/2);
		nivView.setFitHeight(winHeight/2);
		nivView.setTranslateX(winWidth*0.325);
		nivView.setTranslateY(winHeight*0.3);

		GridPane.setConstraints(vbox, 0, 0);
	 	GridPane.setConstraints(nivView, 1, 0);
	 	
    	GridPane grid11 = new GridPane();

		final Image imageTitle = new Image("./menu/choix_Niveau.png"); 
		final ImageView titleView = new ImageView(imageTitle); 
		titleView.setTranslateX(winWidth*0.2);
		titleView.setTranslateY(winHeight/7);
		titleView.setFitHeight(winHeight/14);
		titleView.setFitWidth(winWidth/2);
		
    	final Image imageRetour1 = new Image("./menu/bouton_Retour1.png"); 
    	final Image imageRetour2 = new Image("./menu/bouton_Retour2.png"); 
		final ImageView buttonReturn = new ImageView(imageRetour1);
		buttonReturn.setFitWidth(winWidth/7);
		buttonReturn.setFitHeight(winHeight/5);
		buttonReturn.setTranslateX(winWidth*0.14);
		buttonReturn.setOnMouseEntered(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				buttonReturn.setImage(imageRetour2);			
			}
		});
        
		buttonReturn.setOnMouseExited(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				buttonReturn.setImage(imageRetour1);	

			}
		});
		
    	buttonReturn.setOnMouseClicked(new EventHandler<MouseEvent>() { 
		   
		    public void handle(MouseEvent actionEvent) { 
		    	stage.setTitle("Lightbot - Baymax Edition");
		        stage.setScene(scn4);
		        stg = stage;
		        stage.show();
		    }
		});
		final Image bouton1Image1 = new Image("./menu/bouton_01_desactive.png"); 
		final Image bouton1Image2 = new Image("./menu/bouton_01_active.png"); 
		final Image bouton2Image1 = new Image("./menu/bouton_02_desactive.png"); 
		final Image bouton2Image2 = new Image("./menu/bouton_02_active.png"); 
		final Image bouton3Image1 = new Image("./menu/bouton_03_desactive.png"); 
		final Image bouton3Image2 = new Image("./menu/bouton_03_active.png"); 
		final Image bouton4Image1 = new Image("./menu/bouton_04_desactive.png"); 
		final Image bouton4Image2 = new Image("./menu/bouton_04_active.png"); 
		
		final ImageView button1 = new ImageView(bouton1Image1); 
		button1.setFitWidth(winWidth/12);
		button1.setFitHeight(winHeight/8);


		final ImageView button2 = new ImageView(bouton2Image1); 
		button2.setFitWidth(winWidth/12);
		button2.setFitHeight(winHeight/8);

		
		final ImageView button3 = new ImageView(bouton3Image1); 
		button3.setFitWidth(winWidth/12);
		button3.setFitHeight(winHeight/8);

		
		final ImageView button4 = new ImageView(bouton4Image1); 
		button4.setFitWidth(winWidth/12);
		button4.setFitHeight(winHeight/8);

		
	 	GridPane.setConstraints(button1, 0, 0);
	 	GridPane.setConstraints(button2, 1, 0);
	 	GridPane.setConstraints(button3, 0, 1);
	 	GridPane.setConstraints(button4, 1, 1);
	 	
	 	button1.setDisable(true);
	 	button2.setDisable(true);
	 	button3.setDisable(true);
	 	button4.setDisable(true);
	 	grid11.setHgap(30); 
	 	grid11.setVgap(30);
	 	button1.setOnMouseClicked(new EventHandler<MouseEvent>() {
		    public void handle(MouseEvent actionEvent) { 
		    	try {
					World.init("./Niveaux/Moyen/2bots/2bots1.xml", "./Niveaux/Moyen/2bots/", 1);
					lancerLevels(7);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		    }
		});
	 	button1.setOnMouseEntered(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				nivView.setImage(null);
				nivView.setImage(imageNiv1);			
			}
		});
        
	 	button1.setOnMouseExited(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				nivView.setImage(imageNivs);	

			}
		});
	 	
     	button2.setOnMouseClicked(new EventHandler<MouseEvent>() { 
    	    public void handle(MouseEvent actionEvent) { 
		    	try {
					World.init("./Niveaux/Moyen/2bots/2bots2.xml", "./Niveaux/Moyen/2bots/", 2);
					lancerLevels(7);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		    }
		});
	 	button2.setOnMouseEntered(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				nivView.setImage(imageNiv2);			
			}
		});
        
	 	button2.setOnMouseExited(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				nivView.setImage(imageNivs);	

			}
		});
	 	
     	button3.setOnMouseClicked(new EventHandler<MouseEvent>() { 
    	    public void handle(MouseEvent actionEvent) { 
		    	try {
					World.init("./Niveaux/Moyen/2bots/2bots3.xml", "./Niveaux/Moyen/2bots/", 3);
					lancerLevels(7);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		    }
		});
	 	button3.setOnMouseEntered(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				nivView.setImage(imageNiv3);			
			}
		});
        
	 	button3.setOnMouseExited(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				nivView.setImage(imageNivs);	

			}
		});
	 	
     	button4.setOnMouseClicked(new EventHandler<MouseEvent>() { 
    	    public void handle(MouseEvent actionEvent) { 
		    	try {
					World.init("./Niveaux/Moyen/2bots/2bots4.xml", "./Niveaux/Moyen/2bots/", 4);
					lancerLevels(7);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		    }
		});
	 	button4.setOnMouseEntered(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				nivView.setImage(imageNiv4);			
			}
		});
        
	 	button4.setOnMouseExited(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				nivView.setImage(imageNivs);	

			}
		});
     	grid11.getChildren().setAll(button1,button2,button3,button4);
     	
     	for(int i =0; i<nb_maps;i++){
     		grid11.getChildren().get(i).setDisable(false);
	 	}

		if(!button1.isDisable()){
			button1.setImage(bouton1Image2);			
		}
		if(!button2.isDisable()){
			button2.setImage(bouton2Image2);			
		}
		if(!button3.isDisable()){
			button3.setImage(bouton3Image2);			
		}
		if(!button4.isDisable()){
			button4.setImage(bouton4Image2);			
		}
     	
     	grid11.setTranslateX(winWidth/8);
     	vbox.getChildren().addAll(grid11, buttonReturn);
     	gridMain.getChildren().addAll(vbox, nivView);
     	root11.getChildren().addAll(titleView,gridMain);
     	stage.setTitle("Lightbot - Baymax Edition - Choix du niveau (2 Bots)");
     	stage.setScene(scn11);
        stage.getScene().getRoot().setStyle("-fx-background-image: url(./menu/couleur_fond2.png);");
     	stg = stage;
     	stage.show();
    }
    
    public static void niveauxDifficiles(final Stage stage){
    	
		//stage.setFullScreen(true);
    	root12 = new Pane();
		double winWidth = Screen.getPrimary().getVisualBounds().getWidth();
		double winHeight = Screen.getPrimary().getVisualBounds().getHeight();
		scn12 = new Scene(root12, winWidth, winHeight);
    	
    	org.jdom2.Document document = null; 
    	Element racine;
    	int nb_maps = 0;
    	SAXBuilder sxb = new SAXBuilder();
		try
		{ 
			document = sxb.build(new File("./Niveaux/Difficile/sauv.xml"));
		}
		catch(Exception e){};
		
		racine = document.getRootElement();
		
		Element courant;
		List listDebloque = racine.getChildren("nb_maps_debloques");
		Iterator iteDebloque = listDebloque.iterator();
		while(iteDebloque.hasNext())
		{
			courant = (Element)iteDebloque.next();
			nb_maps = Integer.parseInt(courant.getAttributeValue("nb"));
		}
    	/////////////////////
		///////////////////
		////////////////////////

		GridPane gridMain = new GridPane();
		
     	VBox vbox = new VBox();
     	vbox.setCursor(Cursor.HAND);
     	vbox.setTranslateY(winHeight*0.4);
     	vbox.setSpacing(winHeight/8);
     	
    	final Image imageNivs = new Image("./menu/difficile_levels.png"); 
    	final Image imageNiv1 = new Image("./menu/difficile_level1.png"); 
    	final Image imageNiv2 = new Image("./menu/2bots_level2.png"); 
    	final Image imageNiv3 = new Image("./menu/2bots_level3.png"); 
    	final Image imageNiv4 = new Image("./menu/2bots_level4.png"); 

		final ImageView nivView = new ImageView(imageNivs);
		nivView.setFitWidth(winWidth/2);
		nivView.setFitHeight(winHeight/2);
		nivView.setTranslateX(winWidth*0.325);
		nivView.setTranslateY(winHeight*0.3);

		GridPane.setConstraints(vbox, 0, 0);
	 	GridPane.setConstraints(nivView, 1, 0);
	 	
	 	
		GridPane grid12 = new GridPane();

		final Image imageTitle = new Image("./menu/choix_Niveau.png"); 
		final ImageView titleView = new ImageView(imageTitle); 
		titleView.setTranslateX(winWidth*0.2);
		titleView.setTranslateY(winHeight/7);
		titleView.setFitHeight(winHeight/14);
		titleView.setFitWidth(winWidth/2);
		
    	final Image imageRetour1 = new Image("./menu/bouton_Retour1.png"); 
    	final Image imageRetour2 = new Image("./menu/bouton_Retour2.png"); 
		final ImageView buttonReturn = new ImageView(imageRetour1);
		buttonReturn.setFitWidth(winWidth/7);
		buttonReturn.setFitHeight(winHeight/5);
		buttonReturn.setTranslateX(winWidth*0.14);
		
		buttonReturn.setOnMouseEntered(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				buttonReturn.setImage(imageRetour2);			
			}
		});
        
		buttonReturn.setOnMouseExited(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				buttonReturn.setImage(imageRetour1);	

			}
		});
		
    	buttonReturn.setOnMouseClicked(new EventHandler<MouseEvent>() { 
		   
		    public void handle(MouseEvent actionEvent) { 
		    	stage.setTitle("Lightbot - Baymax Edition");
		        stage.setScene(scn2);
		        stg = stage;
		        stage.show();
		    }
		});
		final Image bouton1Image1 = new Image("./menu/bouton_01_desactive.png"); 
		final Image bouton1Image2 = new Image("./menu/bouton_01_active.png"); 
		final Image bouton2Image1 = new Image("./menu/bouton_02_desactive.png"); 
		final Image bouton2Image2 = new Image("./menu/bouton_02_active.png"); 
		final Image bouton3Image1 = new Image("./menu/bouton_03_desactive.png"); 
		final Image bouton3Image2 = new Image("./menu/bouton_03_active.png"); 
		final Image bouton4Image1 = new Image("./menu/bouton_04_desactive.png"); 
		final Image bouton4Image2 = new Image("./menu/bouton_04_active.png"); 
		
		final ImageView button1 = new ImageView(bouton1Image1); 
		button1.setFitWidth(winWidth/12);
		button1.setFitHeight(winHeight/8);


		final ImageView button2 = new ImageView(bouton2Image1); 
		button2.setFitWidth(winWidth/12);
		button2.setFitHeight(winHeight/8);

		
		final ImageView button3 = new ImageView(bouton3Image1); 
		button3.setFitWidth(winWidth/12);
		button3.setFitHeight(winHeight/8);

		
		final ImageView button4 = new ImageView(bouton4Image1); 
		button4.setFitWidth(winWidth/12);
		button4.setFitHeight(winHeight/8);

		
	 	GridPane.setConstraints(button1, 0, 0);
	 	GridPane.setConstraints(button2, 1, 0);
	 	GridPane.setConstraints(button3, 0, 1);
	 	GridPane.setConstraints(button4, 1, 1);
	 	
	 	button1.setDisable(true);
	 	button2.setDisable(true);
	 	button3.setDisable(true);
	 	button4.setDisable(true);
	 	grid12.setHgap(30); 
	 	grid12.setVgap(30);
	 	button1.setOnMouseClicked(new EventHandler<MouseEvent>() {
		    public void handle(MouseEvent actionEvent) { 
		    	try {
					World.init("./Niveaux/Difficile/Difficile1.xml", "./Niveaux/Difficile/", 1);
					lancerLevels(8);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		    }
		});
	 	button1.setOnMouseEntered(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				nivView.setImage(null);
				nivView.setImage(imageNiv1);			
			}
		});
        
	 	button1.setOnMouseExited(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				nivView.setImage(imageNivs);	

			}
		});
	 	
     	button2.setOnMouseClicked(new EventHandler<MouseEvent>() { 
    	    public void handle(MouseEvent actionEvent) { 
		    	try {
					World.init("./Niveaux/Difficile/Difficile2.xml", "./Niveaux/Difficile/", 2);
					lancerLevels(8);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		    }
		});
	 	button2.setOnMouseEntered(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				nivView.setImage(imageNiv2);			
			}
		});
        
	 	button2.setOnMouseExited(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				nivView.setImage(imageNivs);	

			}
		});
	 	
     	button3.setOnMouseClicked(new EventHandler<MouseEvent>() { 
    	    public void handle(MouseEvent actionEvent) { 
		    	try {
					World.init("./Niveaux/Difficile/Difficile3.xml", "./Niveaux/Difficile/", 3);
					lancerLevels(8);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		    }
		});
	 	button3.setOnMouseEntered(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				nivView.setImage(imageNiv3);			
			}
		});
        
	 	button3.setOnMouseExited(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				nivView.setImage(imageNivs);	

			}
		});
	 	
     	button4.setOnMouseClicked(new EventHandler<MouseEvent>() { 
    	    public void handle(MouseEvent actionEvent) { 
		    	try {
					World.init("./Niveaux/Difficile/Difficile4.xml", "./Niveaux/Difficile/", 4);
					lancerLevels(8);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		    }
		});
	 	button4.setOnMouseEntered(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				nivView.setImage(imageNiv4);			
			}
		});
        
	 	button4.setOnMouseExited(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				nivView.setImage(imageNivs);	

			}
		});
	 	
     	grid12.getChildren().setAll(button1,button2,button3,button4);
     	
     	for(int i =0; i<nb_maps;i++){
     		grid12.getChildren().get(i).setDisable(false);
	 	}

		if(!button1.isDisable()){
			button1.setImage(bouton1Image2);			
		}
		if(!button2.isDisable()){
			button2.setImage(bouton2Image2);			
		}
		if(!button3.isDisable()){
			button3.setImage(bouton3Image2);			
		}
		if(!button4.isDisable()){
			button4.setImage(bouton4Image2);			
		}
     	
     	grid12.setTranslateX(winWidth/8);
     	vbox.getChildren().addAll(grid12, buttonReturn);
     	gridMain.getChildren().addAll(vbox, nivView);
     	root12.getChildren().addAll(titleView,gridMain);
     	stage.setTitle("Lightbot - Baymax Edition - Choix de la diffcult� (Difficile)");
     	stage.setScene(scn12);
        stage.getScene().getRoot().setStyle("-fx-background-image: url(./menu/couleur_fond2.png);");
     	stg = stage;
     	stage.show();
    }
    
    public static void credits(final Stage stage) {

		//stage.setFullScreen(true);
		double winWidth = Screen.getPrimary().getVisualBounds().getWidth();
		double winHeight = Screen.getPrimary().getVisualBounds().getHeight();
		if(scn13 == null)
			scn13 = new Scene(root13, winWidth, winHeight);
    	
		root13.setCursor(Cursor.DEFAULT);
    	final Image imageCredits = new Image("./menu/image_credits.png"); 
		final ImageView creditsView = new ImageView(imageCredits);
		creditsView.setFitWidth(winWidth*0.85);
		creditsView.setFitHeight(winHeight*0.85);
		creditsView.setTranslateX(winWidth*0.08);

    	final Image imageRetour1 = new Image("./menu/bouton_Retour1.png"); 
    	final Image imageRetour2 = new Image("./menu/bouton_Retour2.png"); 

		final ImageView buttonReturn = new ImageView(imageRetour1);
		buttonReturn.setFitWidth(winWidth/8);
		buttonReturn.setFitHeight(winHeight/6);
		buttonReturn.setTranslateX(winWidth*0.05);
		buttonReturn.setTranslateY(winHeight*0.82);
		buttonReturn.setCursor(Cursor.HAND);
		buttonReturn.setOnMouseEntered(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				buttonReturn.setImage(imageRetour2);			 
			}
		});
        
		buttonReturn.setOnMouseExited(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				buttonReturn.setImage(imageRetour1);	

			}
		});
		
    	buttonReturn.setOnMouseClicked(new EventHandler<MouseEvent>() { 
		   
		    public void handle(MouseEvent actionEvent) { 
		    	stage.setTitle("Lightbot - Baymax Edition");
		        stage.setScene(scene);
		        stg = stage;
		        stage.show();
		    }
		});
    	root13.getChildren().addAll(creditsView, buttonReturn);
    	stage.setTitle("Lightbot - Baymax Edition - Credits");
        stage.setScene(scn13);
        stage.getScene().getRoot().setStyle("-fx-background-image: url(./menu/couleur_fond2.png);");
        stg = stage;
        stage.show();
	} 
    
    public void lancerEditor() throws Exception {
    	Application app2 = Editor.class.newInstance(); 
    	final Stage anotherStage = new Stage();
    	app2.start(anotherStage);
    	mediaPlayer.stop();
    	stg.hide();
    	
    	anotherStage.setOnHidden(new EventHandler<WindowEvent>() {
            @Override
            public void handle(final WindowEvent event) {
            	anotherStage.close();
                mediaPlayer.setOnEndOfMedia(new Runnable() {
                    public void run() {
                      mediaPlayer.seek(Duration.ZERO);
                    }
                });
            	mediaPlayer.play();                
            	stg.show();
            }
        });
    }

    public static void lancerLevels(int i) throws Exception {
    	Application app3 = AffichagePartie.class.newInstance(); 
    	Stage anotherStage = new Stage();
    	app3.start(anotherStage);
    	mediaPlayer.stop();
		final File file = new File("src/menu/balalalala.mp3"); 
        final Media media2 = new Media(file.toURI().toString()); 
        mediaPlayer = new MediaPlayer(media2);
    	mediaPlayer.play();
    	stg.hide();
    	
    	anotherStage.setOnHidden(new EventHandler<WindowEvent>() {
            @Override
            public void handle(final WindowEvent event) {
        		final File file = new File("src/menu/Microbots.mp3"); 
                final Media media = new Media(file.toURI().toString()); 
                mediaPlayer = new MediaPlayer(media);
                mediaPlayer.setOnEndOfMedia(new Runnable() {
                    public void run() {
                      mediaPlayer.seek(Duration.ZERO);
                    }
                });
            	mediaPlayer.play();
            	anotherStage.close();
            	
            	switch(i){
            	case 1 : niveauxFacilesBase(stg); break;
            	case 2 : niveauxFacilesIf(stg); break;
            	case 3 : niveauxFacilesTeleport(stg); break;
            	case 4 : niveauxFacilesBreak(stg); break;
            	case 5 : niveauxMoyensFlashback(stg); break;
            	case 6 : niveauxMoyensFor(stg); break;
            	case 7 : niveauxMoyens2Bots(stg); break;
            	case 8 : niveauxDifficiles(stg); break;
            	default : stg.show();
            	}
            }
        });
    }
}
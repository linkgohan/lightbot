package actions;

import javafx.scene.paint.Color;
import ordre_bot.Ordre_bot;
import lightbotException.ElectrocutionBot;
import lightbotException.OutOfBoundBot;
import main.Main;
import cases.CasesPleine;
import robot.Robot;
import world.World;

public class Activer extends Ordre_bot{
	public Activer(Color c){
		clr = c;
	}
	
	public Activer(){
		clr = Color.CYAN;
		code = 0;
	}
	
	public void run_exe(Robot bot) throws OutOfBoundBot {
		try {
			CasesPleine laCase = (CasesPleine) World.get_case(bot.getPosition().get_x(), bot.getPosition().get_y());
			try {
				Main.addT(bot.getRobotId(), laCase.run(bot));
			} catch (ElectrocutionBot e) {
				return;
			}
		} catch (OutOfBoundBot e) {
			return;
		}
		return;
	}
}

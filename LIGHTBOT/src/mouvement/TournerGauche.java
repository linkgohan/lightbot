package mouvement;
import main.Main;
import javafx.animation.RotateTransition;
import javafx.scene.paint.Color;
import javafx.util.Duration;
import direction.Direction;
import robot.Robot;

public class TournerGauche extends Mouvements{
	public TournerGauche(Color c){
		super(c);
		code = 0;
	}
	
	public TournerGauche(){
		super();
	}
	
	public void run_exe(Robot bot){
		RotateTransition rt = new RotateTransition(Duration.millis(500),bot.getRobot3D());
		switch(bot.getDirection().getCap()){
		case Nord:
		{
			bot.setDirection(Direction.capType.Ouest);
			break;
		}
		case Ouest:
		{
			bot.setDirection(Direction.capType.Sud);
			break;
		}
		case Est:
		{
			bot.setDirection(Direction.capType.Nord);
			break;
		}
		case Sud:
		{
			bot.setDirection(Direction.capType.Est);
			break;
		}
		default:
			System.out.println("erreur direction");
		}
		rt.setByAngle(-90);
		Main.addT(bot.getRobotId(), rt);
	}
	
}
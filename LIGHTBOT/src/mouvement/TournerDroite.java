package mouvement;
import main.Main;
import javafx.animation.RotateTransition;
import javafx.scene.paint.Color;
import javafx.util.Duration;
import direction.*;
import robot.*;

public class TournerDroite extends Mouvements{
	
	public TournerDroite(Color c){
		super(c);
		code = 0;
	}
	
	public TournerDroite(){
		super();
	}
	
	public void run_exe(Robot bot){
		RotateTransition rt = new RotateTransition(Duration.millis(500),bot.getRobot3D());
		switch(bot.getDirection().getCap()){
		case Nord:
		{
			bot.setDirection(Direction.capType.Est);
			break;
		}
		case Ouest:
		{
			bot.setDirection(Direction.capType.Nord);
			break;
		}
		case Est:
		{
			bot.setDirection(Direction.capType.Sud);
			break;
		}
		case Sud:
		{
			bot.setDirection(Direction.capType.Ouest);
			break;
		}
		default:
			System.out.println("erreur direction");
		}
		rt.setByAngle(90);
		Main.addT(bot.getRobotId(), rt);
	}
}

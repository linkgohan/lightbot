package mouvement;

import lightbotException.OutOfBoundBot;
import main.Main;
import cases.Cases;
import javafx.animation.SequentialTransition;
import javafx.animation.TranslateTransition;
import javafx.scene.paint.Color;
import javafx.util.Duration;
import robot.*;
import world.*;

public class Avancer extends Mouvements{
	
	
	public Avancer(Color c){
		super(c);
		code = 0;
	}
	
	public Avancer(){
		super();
	}
	
	public void run_exe(Robot bot){
		SequentialTransition s = new SequentialTransition();
		TranslateTransition t1 = new TranslateTransition(Duration.millis(250),bot.getRobot3D());
		TranslateTransition t2 = new TranslateTransition(Duration.millis(250),bot.getRobot3D());
		int taille = 0;
		switch(bot.getDirection().getCap()){
		case Nord:
		{
			if(World.can_step_on(bot,bot.getPosition().get_x()-1,bot.getPosition().get_y())){
				try {
					if(bot.getPosition().get_z() < World.get_case(bot.getPosition().get_x()-1, bot.getPosition().get_y()).getPos().get_z()){
						taille = -1;
					}
				} catch (OutOfBoundBot e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				try {
					if(bot.getPosition().get_z() > World.get_case(bot.getPosition().get_x()-1, bot.getPosition().get_y()).getPos().get_z()){
						taille = 1;
					}
				} catch (OutOfBoundBot e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				try {
					if(bot.getPosition().get_z() == World.get_case(bot.getPosition().get_x()-1, bot.getPosition().get_y()).getPos().get_z()){
						taille = 0;
					}
				} catch (OutOfBoundBot e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				bot.getPosition().set_x(bot.getPosition().get_x() - 1);
				try {
					bot.getPosition().set_z(World.get_case(bot.getPosition().get_x(), bot.getPosition().get_y()).getPos().get_z());
				} catch (OutOfBoundBot e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				try {
					bot.getPosition().set_z(World.get_case(bot.getPosition().get_x(), bot.getPosition().get_y()).getPos().get_z());
				} catch (OutOfBoundBot e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				if(taille == -1){
					t1.setToX(bot.getPosition().get_x()*Cases.SIZE);
					t2.setToY(-bot.getPosition().get_z()*Cases.SIZE+bot.tailleR());
					s.getChildren().addAll(t2,t1);
				}
				
				if(taille == 1){
					t1.setToX(bot.getPosition().get_x()*Cases.SIZE);
					t2.setToY(-bot.getPosition().get_z()*Cases.SIZE+bot.tailleR());
					s.getChildren().addAll(t1,t2);
				}
				
				if(taille == 0){
					t1 = new TranslateTransition(Duration.millis(500),bot.getRobot3D());
					t1.setToX(bot.getPosition().get_x()*Cases.SIZE);
					s.getChildren().addAll(t1);
				}
				Main.addT(bot.getRobotId(), s);
			}
			else{
				Main.addT(bot.getRobotId(), new TranslateTransition(Duration.millis(500),bot.getRobot3D()));
			}
			break;
		}
		case Ouest:
		{
			if(World.can_step_on(bot,bot.getPosition().get_x(),bot.getPosition().get_y()-1)){
				try {
					if(bot.getPosition().get_z() < World.get_case(bot.getPosition().get_x(), bot.getPosition().get_y()-1).getPos().get_z()){
						taille = -1;
					}
				} catch (OutOfBoundBot e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				try {
					if(bot.getPosition().get_z() > World.get_case(bot.getPosition().get_x(), bot.getPosition().get_y()-1).getPos().get_z()){
						taille = 1;
					}
				} catch (OutOfBoundBot e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				try {
					if(bot.getPosition().get_z() == World.get_case(bot.getPosition().get_x(), bot.getPosition().get_y()-1).getPos().get_z()){
						taille = 0;
					}
				} catch (OutOfBoundBot e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				bot.getPosition().set_y(bot.getPosition().get_y() - 1);
				try {
					bot.getPosition().set_z(World.get_case(bot.getPosition().get_x(), bot.getPosition().get_y()).getPos().get_z());
				} catch (OutOfBoundBot e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				try {
					bot.getPosition().set_z(World.get_case(bot.getPosition().get_x(), bot.getPosition().get_y()).getPos().get_z());
				} catch (OutOfBoundBot e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				if(taille == -1){
					t1.setToZ(bot.getPosition().get_y()*Cases.SIZE);
					t2.setToY(-bot.getPosition().get_z()*Cases.SIZE+bot.tailleR());
					s.getChildren().addAll(t2,t1);
				}
				
				if(taille == 1){
					t1.setToZ(bot.getPosition().get_y()*Cases.SIZE);
					t2.setToY(-bot.getPosition().get_z()*Cases.SIZE+bot.tailleR());
					s.getChildren().addAll(t1,t2);
				}
				
				if(taille == 0){
					t1 = new TranslateTransition(Duration.millis(500),bot.getRobot3D());
					t1.setToZ(bot.getPosition().get_y()*Cases.SIZE);
					s.getChildren().addAll(t1);
				}
				Main.addT(bot.getRobotId(), s);
			}
			else{
				Main.addT(bot.getRobotId(), new TranslateTransition(Duration.millis(500),bot.getRobot3D()));
			}
			break;
		}
		case Est:
		{	
			if(World.can_step_on(bot,bot.getPosition().get_x(),bot.getPosition().get_y()+1)){
				try {
					if(bot.getPosition().get_z() < World.get_case(bot.getPosition().get_x(), bot.getPosition().get_y()+1).getPos().get_z()){
						taille = -1;
					}
				} catch (OutOfBoundBot e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				try {
					if(bot.getPosition().get_z() > World.get_case(bot.getPosition().get_x(), bot.getPosition().get_y()+1).getPos().get_z()){
						taille = 1;
					}
				} catch (OutOfBoundBot e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				try {
					if(bot.getPosition().get_z() == World.get_case(bot.getPosition().get_x(), bot.getPosition().get_y()+1).getPos().get_z()){
						taille = 0;
					}
				} catch (OutOfBoundBot e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				bot.getPosition().set_y(bot.getPosition().get_y() + 1);
				try {
					bot.getPosition().set_z(World.get_case(bot.getPosition().get_x(), bot.getPosition().get_y()).getPos().get_z());
				} catch (OutOfBoundBot e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				try {
					bot.getPosition().set_z(World.get_case(bot.getPosition().get_x(), bot.getPosition().get_y()).getPos().get_z());
				} catch (OutOfBoundBot e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				if(taille == -1){
					t1.setToZ(bot.getPosition().get_y()*Cases.SIZE);
					t2.setToY(-bot.getPosition().get_z()*Cases.SIZE+bot.tailleR());
					s.getChildren().addAll(t2,t1);
				}
				
				if(taille == 1){
					t1.setToZ(bot.getPosition().get_y()*Cases.SIZE);
					t2.setToY(-bot.getPosition().get_z()*Cases.SIZE+bot.tailleR());
					s.getChildren().addAll(t1,t2);
				}
				
				if(taille == 0){
					t1 = new TranslateTransition(Duration.millis(500),bot.getRobot3D());
					t1.setToZ(bot.getPosition().get_y()*Cases.SIZE);
					s.getChildren().addAll(t1);
				}
				Main.addT(bot.getRobotId(), s);
			}
			else{
				Main.addT(bot.getRobotId(), new TranslateTransition(Duration.millis(500),bot.getRobot3D()));
			}
			break;
		}
		case Sud:
		{
			if(World.can_step_on(bot,bot.getPosition().get_x()+1,bot.getPosition().get_y())){
				try {
					if(bot.getPosition().get_z() < World.get_case(bot.getPosition().get_x()+1, bot.getPosition().get_y()).getPos().get_z()){
						taille = -1;
					}
				} catch (OutOfBoundBot e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				try {
					if(bot.getPosition().get_z() > World.get_case(bot.getPosition().get_x()+1, bot.getPosition().get_y()).getPos().get_z()){
						taille = 1;
					}
				} catch (OutOfBoundBot e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				try {
					if(bot.getPosition().get_z() == World.get_case(bot.getPosition().get_x()+1, bot.getPosition().get_y()).getPos().get_z()){
						taille = 0;
					}
				} catch (OutOfBoundBot e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				bot.getPosition().set_x(bot.getPosition().get_x()+1);
				try {
					bot.getPosition().set_z(World.get_case(bot.getPosition().get_x(), bot.getPosition().get_y()).getPos().get_z());
				} catch (OutOfBoundBot e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				try {
					bot.getPosition().set_z(World.get_case(bot.getPosition().get_x(), bot.getPosition().get_y()).getPos().get_z());
				} catch (OutOfBoundBot e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				if(taille == -1){
					t1.setToX(bot.getPosition().get_x()*Cases.SIZE);
					t2.setToY(-bot.getPosition().get_z()*Cases.SIZE+bot.tailleR());
					s.getChildren().addAll(t2,t1);
				}
				
				if(taille == 1){
					t1.setToX(bot.getPosition().get_x()*Cases.SIZE);
					t2.setToY(-bot.getPosition().get_z()*Cases.SIZE+bot.tailleR());
					s.getChildren().addAll(t1,t2);
				}
				
				if(taille == 0){
					t1 = new TranslateTransition(Duration.millis(500),bot.getRobot3D());
					t1.setToX(bot.getPosition().get_x()*Cases.SIZE);
					s.getChildren().addAll(t1);
				}
				Main.addT(bot.getRobotId(), s);
			}
			else{
				Main.addT(bot.getRobotId(), new TranslateTransition(Duration.millis(500),bot.getRobot3D()));
			}
			break;
		}
		default:
			System.out.println("erreur direction");
		}
	}
}

package main;

import interfaces.Editor;

import java.util.Iterator;
import java.util.LinkedList;

import javafx.animation.ParallelTransition;
import javafx.animation.SequentialTransition;
import javafx.animation.Transition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.paint.Color;
import lightbotException.OutOfBoundBot;
import menu.Menu;
import ordre_bot.Ordre_bot;
import position.Position3D;
import procedures.Procedures;
import thread.ThreadTemps;
import world.World;
import affichage.AffichagePartie;

public class Main {
	private static LinkedList<Ordre_bot> mainBotR1 = new LinkedList<Ordre_bot>();
	private static LinkedList<Ordre_bot> listp1R1 = new LinkedList<Ordre_bot>();
	private static LinkedList<Ordre_bot> listp2R1 = new LinkedList<Ordre_bot>();
	
	private static LinkedList<Ordre_bot> mainBotR2 = new LinkedList<Ordre_bot>();
	private static LinkedList<Ordre_bot> listp1R2 = new LinkedList<Ordre_bot>();
	private static LinkedList<Ordre_bot> listp2R2 = new LinkedList<Ordre_bot>();
	
	private static LinkedList<Position3D> listPosR1 = new LinkedList<Position3D>();
	private static LinkedList<Position3D> listPosR2 = new LinkedList<Position3D>();
	
	private static SequentialTransition sR1 = new SequentialTransition();
	private static SequentialTransition sR2 = new SequentialTransition();
	private static ParallelTransition pR1R2 = new ParallelTransition();
	
	private static int nb_r;
	
	public static ThreadTemps thread;
	
	public static boolean faire = true;
	
	public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException, InterruptedException {
		//World.init("./Niveaux/2bots/2bots1.xml");
		//World.init("./Niveaux/Base + Teleport/Level1.xml");
		//World.init("./test/lvl4.xml", "./test/lvl4.xml", 4);
		//World.init("./Niveaux/Facile/If/If1.xml");
		//nb_r = World.getNbRobot();
		//AffichagePartie.run();
		Menu.run();
		//Editor.run();
	}
	
	public static void addMainR1Ordre(Ordre_bot ordre){
		mainBotR1.add(ordre);
		System.out.println("mainR1 : "+mainBotR1.size());
	}
	
	public static void suppMainR1Ordre (Ordre_bot Ob){
		mainBotR1.remove(Ob);
		System.out.println("mainR1 : "+mainBotR1.size());
	}
	
	public static void addP1R1Ordre(Ordre_bot ordre){
		listp1R1.add(ordre);
		System.out.println("listp1R1 : "+listp1R1.size());
	}
	
	public static void suppP1R1Ordre (Ordre_bot Ob){
		listp1R1.remove(Ob);
		System.out.println("listp1R1 : "+listp1R1.size());
	}

	public static void addP2R1Ordre(Ordre_bot ordre){
		listp2R1.add(ordre);
		System.out.println("listp2R1 : "+listp2R1.size());
	}
	
	public static void suppP2R1Ordre (Ordre_bot Ob){
		listp2R1.remove(Ob);
		System.out.println("listp2R1 : "+listp2R1.size());
	}
	
	public static Procedures addP1toMainR1(Color c){
		Procedures p = new Procedures(listp1R1, c);
		mainBotR1.add(p);
		System.out.println("mainR1 : "+mainBotR1.size());
		return p;
	}
	
	public static Procedures addP2toMainR1(Color c){
		Procedures p = new Procedures(listp2R1, c);
		mainBotR1.add(p);
		System.out.println("mainR1 : "+mainBotR1.size());
		return p;
	}
	
	public static Procedures addP1toP1R1(Color c){
		Procedures p = new Procedures(listp1R1, c);
		listp1R1.add(p);
		System.out.println("listp1R1 : "+listp1R1.size());
		return p;
	}
	
	public static Procedures addP2toP1R1(Color c){
		Procedures p = new Procedures(listp2R1, c);
		listp1R1.add(p);
		System.out.println("listp1R1 : "+listp1R1.size());
		return p;
	}
	
	public static Procedures addP1toP2R1(Color c){
		Procedures p = new Procedures(listp1R1, c);
		listp2R1.add(p);
		System.out.println("listp2R1 : "+listp2R1.size());
		return p;
	}
	
	public static Procedures addP2toP2R1(Color c){
		Procedures p = new Procedures(listp2R1, c);
		listp2R1.add(p);
		System.out.println("listp2R1 : "+listp2R1.size());
		return p;
	}
	
	public static void addMainR2Ordre(Ordre_bot ordre){
		mainBotR2.add(ordre);
		System.out.println("mainR2 : "+mainBotR2.size());
	}
	
	public static void suppMainR2Ordre (Ordre_bot Ob){
		mainBotR2.remove(Ob);
		System.out.println("mainR2 : "+mainBotR2.size());
	}
	
	public static void addP1R2Ordre(Ordre_bot ordre){
		listp1R2.add(ordre);
		System.out.println("listp1R2 : "+listp1R2.size());
	}
	
	public static void suppP1R2Ordre (Ordre_bot Ob){
		listp1R2.remove(Ob);
		System.out.println("listp1R2 : "+listp1R2.size());
	}

	public static void addP2R2Ordre(Ordre_bot ordre){
		listp2R2.add(ordre);
		System.out.println("listp2R2 : "+listp2R2.size());
	}
	
	public static void suppP2R2Ordre (Ordre_bot Ob){
		listp2R2.remove(Ob);
		System.out.println("listp2R2 : "+listp2R2.size());
	}
	
	public static Procedures addP1toMainR2(Color c){
		Procedures p = new Procedures(listp1R2, c);
		if(World.getNbRobot() == 2){
			mainBotR2.add(p);
			System.out.println("mainR2 : "+mainBotR2.size());
		}
		return p;
	}
	
	public static Procedures addP2toMainR2(Color c){
		Procedures p = new Procedures(listp2R2, c);
		if(World.getNbRobot() == 2){
			mainBotR2.add(p);
			System.out.println("mainR2 : "+mainBotR2.size());
		}	
		return p;
	}
	
	public static Procedures addP1toP1R2(Color c){
		Procedures p = new Procedures(listp1R2, c);
		if(World.getNbRobot() == 2){
			listp1R2.add(p);
			System.out.println("listp1R2 : "+listp1R2.size());
		}
		return p;
	}
	
	public static Procedures addP2toP1R2(Color c){
		Procedures p = new Procedures(listp2R2, c);
		if(World.getNbRobot() == 2){
			listp1R2.add(p);
			System.out.println("listp1R2 : "+listp1R2.size());
		}
		return p;
	}
	
	public static Procedures addP1toP2R2(Color c){
		Procedures p = new Procedures(listp1R2, c);
		if(World.getNbRobot() == 2){
			listp2R2.add(p);
			System.out.println("listp2R2 : "+listp2R2.size());
		}
		return p;
	}
	
	public static Procedures addP2toP2R2(Color c){
		Procedures p = new Procedures(listp2R2, c);
		if(World.getNbRobot() == 2){
			listp2R2.add(p);
			System.out.println("listp2R2 : "+listp2R2.size());
		}
		return p;
	}
	
	public static void addPos(int r, Position3D p){
		if(r==0){
			listPosR1.add(new Position3D(p));
		}
		if(r==1){
			listPosR2.add(new Position3D(p));
		}
	}
	
	public static void runMain() throws OutOfBoundBot{
	    if(faire){
	    	faire = false;
	    	Procedures m1 = new Procedures(mainBotR1, Color.CYAN);
		    Procedures m2 = new Procedures(mainBotR2, Color.CYAN);
		    
		    thread = new ThreadTemps();
		    
		    thread.start();
		    
		    World.getRobot(0).setMain(m1);
		    World.getRobot(0).setNotSeul();
		    World.getRobot(0).waitR();
		    
		    World.getRobot(0).start();
		    
		    if(World.getNbRobot()>1){
		    	World.getRobot(1).setMain(m2);
		    	World.getRobot(1).waitR();
		    	World.getRobot(1).setNotSeul();
		    	World.getRobot(1).start();
		    }
		    else{
		    	World.getRobot(0).setSeul();
		    }
		    
		    System.out.println("3333");
		    
		    World.getRobot(0).notifyR();
	    }
	}
	
	public static void playT12(){
		pR1R2.getChildren().addAll(sR1,sR2);
		
		System.out.println(sR1.getChildren().size()+" "+sR2.getChildren().size());
		
		pR1R2.play();
	}
	
	public static void playT1(){
		pR1R2.getChildren().addAll(sR1);
		pR1R2.play();
	}
	
	public static void replayLastT1T2(){
		sR1.getChildren().get(sR1.getChildren().size()-1).setAutoReverse(true);
		sR1.getChildren().get(sR1.getChildren().size()-1).setCycleCount(3);
		sR2.getChildren().get(sR2.getChildren().size()-1).setAutoReverse(true);
		sR2.getChildren().get(sR2.getChildren().size()-1).setCycleCount(3);
	}
	
	public static void init(){
		pR1R2.stop();
		
		sR1 = new SequentialTransition();
		sR2 = new SequentialTransition();
		pR1R2 = new ParallelTransition();
		
		World.initRobot();
		World.reinitCases();
		sR1.getChildren().add(World.getRobot(0).initRobot());
		pR1R2.getChildren().add(sR1);
		
		if(World.getNbRobot()>1){
			sR2.getChildren().add(World.getRobot(1).initRobot());
			pR1R2.getChildren().add(sR2);
		}
		
		pR1R2.play();
		
		pR1R2.setOnFinished(new EventHandler<ActionEvent>(){
            @Override
            public void handle(ActionEvent arg0) {
            	faire = true;
            	sR1 = new SequentialTransition();
        		sR2 = new SequentialTransition();
        		pR1R2 = new ParallelTransition();
            }
        });
	}
	
	public static void addT(int r, Transition t){
		if(r==0){
			sR1.getChildren().add(t);
		}
		if(r==1){
			sR2.getChildren().add(t);
		}
	}
	
	public static void vider(){
		mainBotR1 = new LinkedList<Ordre_bot>();
		listp1R1 = new LinkedList<Ordre_bot>();
		listp2R1 = new LinkedList<Ordre_bot>();
		
		mainBotR2 = new LinkedList<Ordre_bot>();
		listp1R2 = new LinkedList<Ordre_bot>();
		listp2R2 = new LinkedList<Ordre_bot>();
		
		faire = true;
		
		sR1 = new SequentialTransition();
		sR2 = new SequentialTransition();
		pR1R2 = new ParallelTransition();
	}
}
package procedures;

import java.util.Iterator;
import java.util.LinkedList;

import javafx.scene.paint.Color;
import lightbotException.OutOfBoundBot;
import main.Main;
import ordre_bot.Ordre_bot;
import robot.Robot;
import world.World;

public class Procedures extends Ordre_bot {
	private LinkedList<Ordre_bot> Ordre;
	
	private static boolean continuer = true;
	
	public Procedures(LinkedList<Ordre_bot> l, Color c){
		Ordre = l;
		clr = c;
		code = 0;
	}
		
	public void addOrdre (Ordre_bot Ob){
		Ordre.add(Ob);
	}
	
	public void suppOrdre (Ordre_bot Ob){
		Ordre.remove(Ob);
	}

	public void run_exe(Robot bot) throws OutOfBoundBot{
		Iterator i = Ordre.iterator();
		Ordre_bot cur;
		while(i.hasNext()){
			while(!bot.getContinuer() && !bot.getSeul()){
				System.out.println();
				if(!continuer){
					break;
				}
			}
			
			if(World.getNbRobot() == 2 &&(bot.getRobotId() == 0 || bot.getSeul())){
 				if(bot.getRobotId() == 0){
 					if(bot.getPosition().equals(World.getRobot(1).getPosition())){
 						Procedures.arreter();
 						Main.replayLastT1T2();
 						return;
 					}
 				}
 				else{
 					if(bot.getPosition().equals(World.getRobot(0).getPosition())){
 						Procedures.arreter();
 						Main.replayLastT1T2();
 						return;
 					}
 				}
 			}
			
			if(!continuer){
				break;
			}
			
			cur = (Ordre_bot)i.next();
 			if(World.VerifyCasesAColorer()){
 			}
 			else{
 				if(World.VerifyCasesAColorer()){
 	 			}
 	 			else{
 	 				switch(cur.get_code()){
						case 0 : 
						{
							cur.run(bot);
							
							if(World.getNbRobot()>1 && !bot.getSeul()){
								bot.waitR();
								if(bot.getRobotId() == 0){
									World.getRobot(1).notifyR();
								}
								if(bot.getRobotId() == 1){
									World.getRobot(0).notifyR();
								}
							}
							break;
						}
						case (-2) :
						{
							int nb_for = 0;
							LinkedList<Ordre_bot> For = new LinkedList<Ordre_bot>();
							Color c = cur.get_color();
							cur = (Ordre_bot) i.next();
							
							while(cur.get_code() <= 0 || nb_for != 0){
								if(cur.get_code() == -2){
									nb_for++;
								}
								if(cur.get_code() > 0 && nb_for != 0){
									nb_for--;
								}
								For.add(cur);
								cur = (Ordre_bot) i.next();
							}
							
							Procedures p = new Procedures(For, Color.CYAN);
							
							if(c == Color.CYAN || (World.get_case(bot.getPosition().get_x(),bot.getPosition().get_y()).get_color() == c)){
								for(int j = 0;j<cur.get_code();j++){
									p.run(bot);
								}
							}
							
							if(World.getNbRobot()>1 && !bot.getSeul()){
								bot.waitR();
								if(bot.getRobotId() == 0){
									World.getRobot(1).notifyR();
								}
								if(bot.getRobotId() == 1){
									World.getRobot(0).notifyR();
								}
							}
							
							break;
						}
						case (-1) : 
						{
							cur.run(bot);
							if(World.getNbRobot()>1 && !bot.getSeul()){
								bot.waitR();
								if(bot.getRobotId() == 0){
									World.getRobot(1).notifyR();
								}
								if(bot.getRobotId() == 1){
									World.getRobot(0).notifyR();
								}
							}
							if(cur.get_color() == Color.CYAN || cur.get_color() == World.get_case(bot.getPosition().get_x(), bot.getPosition().get_y()).get_color()){
								return;
							}
	 					}
 	 				}
 	 			}	
 			}
		}
		
		return;
	}
	
	public static void arreter(){
		continuer = false;
	}
	
	public static void continuer(){
		continuer = true;
	}
}
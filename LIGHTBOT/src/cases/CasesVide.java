package cases;
import java.util.Iterator;

import robot.*;
import interfaces.Editor;
import javafx.animation.Transition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.DepthTest; 
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Box;
import javafx.scene.shape.DrawMode;
import position.Position3D;

public class CasesVide implements Cases {
	private String nom = "CasesVide";
	private Position3D position = new Position3D(0,0,0);
	public Group cube;
	private Color clr;
	final PhongMaterial material = new PhongMaterial(Color.BLUE);
	
	public CasesVide(int x, int y, int z){
		position = new Position3D(x,y,z);
	}
	
	public boolean can_step_on(Robot bot){
		return false;
	}
	
	public boolean can_jump_on(Robot bot){
		return false;
	}
	
	public Button Aff2D(){
		Button res = new Button();
		res.maxHeight(Taille);
		res.maxWidth(Taille);
		res.minHeight(Taille);
		res.minWidth(Taille);
		res.setStyle("-fx-base: #"+(Color.WHITE).toString().split("x")[1]+";");
		res.setTextFill(Color.TRANSPARENT);
		res.setTranslateX((position.get_y() * (Taille+5)));
		res.setTranslateY((position.get_x() * (Taille+5)));
		
		res.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                Editor.changecases(position.get_x(), position.get_y());  
            }
        });
		
		return res;
	}
	
	public Group Aff3D(){
		Group grp = new Group();
		Box res = new Box(Cases.SIZE,0,Cases.SIZE);
		res.setDrawMode(DrawMode.FILL);
		
		res.setMaterial(material);
		res.setOpacity(0);
		res.setTranslateX(SIZE*position.get_x());
		res.setTranslateY(SIZE*0.5);
		res.setTranslateZ(SIZE*position.get_y());
		res.setDepthTest(DepthTest.ENABLE);
		res.setFocusTraversable(true);
		res.setMouseTransparent(true);
		//res.setDisable(true);
		
		
		grp.getChildren().add(res);
		
		cube = grp;
		
		return grp;
	}
	
	public String nom() {
		return nom;
	}
	
	public Position3D getPos(){
		return position;
	}
	
	public void set_color(Color c){
		
	}
	
	public Color get_color(){
		return Color.BLUE;
	}

	@Override
	public void changeColor(Color c) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Transition initColor(int time) {
		// TODO Auto-generated method stub
		return null;
	}
	
}

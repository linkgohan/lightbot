package cases;

import java.util.Iterator;

import interfaces.Editor;
import item.*;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.ParallelTransition;
import javafx.animation.Timeline;
import javafx.animation.Transition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Cursor;
import javafx.scene.DepthTest;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Box;
import javafx.scene.shape.DrawMode;
import javafx.util.Duration;
import lightbotException.ElectrocutionBot;
import lightbotException.OutOfBoundBot;
import position.Position3D;
import robot.Robot;
import world.World;
 
public class CasesPleine implements Cases {
	private Position3D position;
	private ItemsStatique Objet_case;
	private String nom = "CasesPleine";
	private Color clr;
	private Color clrPrim;
	public Group cube;
	Image diffuseMap; 
	PhongMaterial material; 
	
	
	public CasesPleine(int x, int y, int z, ItemsStatique o){
		position = new Position3D(x, y, z);
		Objet_case = o;
		clr = o.getColor();
		clrPrim = o.getColor();
	}
	
	public Transition run(Robot bot) throws ElectrocutionBot{
		Transition t = Objet_case.run(bot, this);
		clr = Objet_case.getColor();
		//diffuseMap = new Image(getClass().getResource("contours_cube.jpg").toExternalForm()); 
		//material = new PhongMaterial(clr, diffuseMap, null, null, null);
		//initColor();
		return t;
	}
	
	public Position3D getPos(){
		return position;
	}
	
	public void set_pos_x(int x){
		position.set_x(x);
	}
	
	public void set_pos_y(int y){
		position.set_y(y);
	}
	
	public void set_pos_z(int z){
		position.set_z(z);
	}

	public ItemsStatique getObjet() {
		return Objet_case;
	}
	
	public void setObjet(ItemsStatique o) {
		Objet_case = o;
	}
	
	public boolean can_step_on(Robot bot){
		return (bot.getPosition().get_z() >= position.get_z());
	}
	
	public boolean can_jump_on(Robot bot){
		return ((bot.getPosition().get_z()) + 1 >= position.get_z());
	}
	
	public Button Aff2D(){
		Button res = new Button();
		res.maxHeight(Taille);
		res.maxWidth(Taille);
		res.minHeight(Taille);
		res.minWidth(Taille);
		res.setStyle("-fx-base: #"+Objet_case.getColor().toString().split("x")[1]+";");
		res.setTranslateX((position.get_y() * (Taille+5)));
		res.setTranslateY((position.get_x() * (Taille+5)));
		
		res.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                Editor.changecases(position.get_x(), position.get_y());  
            }
        });
		
		return res;
	}
	
	public Group Aff3D(){
		 
		Group grp = new Group();
		Box res;
		
		diffuseMap = new Image(getClass().getResource("contours_cube.jpg").toExternalForm()); 
		material = new PhongMaterial(Objet_case.getColor(), diffuseMap, null, null, null); 
		
		for(int i = 0; i<position.get_z(); i++){
			res = new Box(Cases.SIZE,Cases.SIZE,Cases.SIZE);
			res.setDrawMode(DrawMode.FILL);
			res.setMaterial(material);
			res.setTranslateX(SIZE*position.get_x());
			res.setTranslateY(-SIZE*i);
			res.setTranslateZ(SIZE*position.get_y());
			res.setDepthTest(DepthTest.ENABLE);
			
			if(Objet_case.getNom().equals("Teleport")){
				final int x = ((Teleport)this.Objet_case).get_xvers();
				final int y = ((Teleport)this.Objet_case).get_yvers();
				
				res.setCursor(Cursor.CROSSHAIR);
				res.setOnMouseEntered(new EventHandler<MouseEvent>() {
		            public void handle(MouseEvent mouseEvent) {
		                try {
		                	World.get_case(x, y).changeColor(Color.GOLD);
						} catch (OutOfBoundBot e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
		            }
		        });
				res.setOnMouseExited(new EventHandler<MouseEvent>() {
		            public void handle(MouseEvent mouseEvent) {
		            	try {
							World.get_case(x, y).initColor(500);
						} catch (OutOfBoundBot e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
		            }
		        });
			}
			
			grp.getChildren().add(res);
		}
		
		if(Objet_case.getNom().equals("Teleport")){
			final int x = ((Teleport)this.Objet_case).get_xvers();
			final int y = ((Teleport)this.Objet_case).get_yvers();
			
			grp.setCursor(Cursor.CROSSHAIR);
			grp.setOnMouseEntered(new EventHandler<MouseEvent>() {
	            public void handle(MouseEvent mouseEvent) {
	                try {
						World.get_case(x, y).changeColor(Color.GOLD);
					} catch (OutOfBoundBot e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	            }
	        });
			grp.setOnMouseExited(new EventHandler<MouseEvent>() {
	            public void handle(MouseEvent mouseEvent) {
	            	try {
						World.get_case(x, y).changeColor(World.get_case(x, y).get_color());
					} catch (OutOfBoundBot e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	            }
	        });
		}
		
		cube = grp;
		
		return grp;
	}
	
	public String nom() {
		return nom;
	}
	
	public Color get_color(){
		return clr;
	}
	
	public void set_color(Color c){
		clr = c;
	}

	public Transition initColor(int time){
		Image di = new Image(getClass().getResource("contours_cube.jpg").toExternalForm()); 
		PhongMaterial ma = new PhongMaterial(clrPrim, di, null, null, null); 
		Iterator<Node> i = cube.getChildren().iterator();
		Box n;
		Objet_case.setColor(clrPrim);
		
		clr = clrPrim;
		
		if(clrPrim == Color.YELLOW){
			System.out.println("YELLOW");
		}
		else{
			System.out.println("PAS BON");
		}
		
		return changeColorTransition(clrPrim, time);
	}
	
	public void changeColor(Color c) {
		final Image di = new Image(getClass().getResource("contours_cube.jpg").toExternalForm()); 
		final PhongMaterial ma = new PhongMaterial(c, di, null, null, null); 
		
		Iterator<Node> i = cube.getChildren().iterator();
		Box n;
		while(i.hasNext()){
			n = (Box) i.next();
			n.setMaterial(ma);
		}
	}
	
	public Transition changeColorTransition(Color c2, int time) {
		ParallelTransition s = new ParallelTransition();
		
		final Image di = new Image(getClass().getResource("contours_cube.jpg").toExternalForm()); 
		final PhongMaterial ma = new PhongMaterial(c2, di, null, null, null); 
		
		Timeline colorchange;
		
		Iterator<Node> i = cube.getChildren().iterator();
		Box n = null;
		while(i.hasNext()){
			n = (Box) i.next();
			colorchange = new Timeline(new KeyFrame(new Duration(time),  new KeyValue(n.materialProperty(), ma)));
			s.getChildren().add(colorchange);
		}
		
		return s;
	}
}

package cases;
import javafx.animation.Transition;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.paint.Color;
import position.Position3D;
import robot.*;

public interface Cases {
	public Position3D position = new Position3D(0,0,0);
	public int SIZE = 200;
	public int H = 200;
	public Button Aff2D();
	public Group Aff3D();
	public int Taille = 40;
	public String nom();
	public Position3D getPos();
	public boolean can_step_on(Robot bot);
	public boolean can_jump_on(Robot bot);
	public Color get_color();
	public void set_color(Color c);
	public Transition initColor(int time);
	public void changeColor(Color c);
}

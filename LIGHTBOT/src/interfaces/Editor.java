package interfaces;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Iterator;
import java.util.List;

import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import item.*;
import direction.*;
import cases.Cases;
import cases.CasesPleine;
import cases.CasesVide;
import editor.*;
import javafx.scene.control.*;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.*;
import javafx.application.Application; 
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.scene.Group;
import javafx.scene.PerspectiveCamera;

public class Editor extends Application{
	private static int nb_col;
	private static int nb_lig;
	private static Scene scene;
	private static Scene scene_tab;
	private static Output o;
	private static Stage stage;
	private static int nb_map;
	private static Button[][] mesBouttons;

	public static void run(){
		Application.launch(Editor.class);
	}
	
	public static void suite(int l, int c){
		nb_lig = l;
		nb_col = c;
		org.jdom2.Document document = null;
		
		SAXBuilder sxb = new SAXBuilder();
		try
		{
			document = sxb.build(new File("./Niveaux/Bonus/sauv.xml"));
		}
		catch(Exception e){};
		
		Element racine = document.getRootElement();
		
		Element courant;
		List listTaille = racine.getChildren("nb_maps");
		Iterator iteTaille = listTaille.iterator();
		
		courant = (Element)iteTaille.next();
		nb_map = Integer.parseInt(courant.getAttributeValue("nb"));
		nb_map++;
		o = new Output("./Niveaux/Bonus/Bonus"+nb_map+".xml", l, c);
		
		mesBouttons = new Button[nb_lig][nb_col];
		
		for(int i=0; i<nb_lig;i++){
	 		for(int j=0; j<nb_col;j++){
	 			mesBouttons[i][j] = o.getCases(i, j).Aff2D();
	 		}
	 	}
		
		aff_tab();
	}
	
	public static void aff_tab(){
		Group cube = new Group();
        StackPane root = new StackPane();
        Scene scn = new Scene(root, 600, 600, true);
        
        for(int i=0; i<nb_lig;i++){
 		   for(int j=0; j<nb_col;j++){
 			   cube.getChildren().add(mesBouttons[i][j]);
 		   }
 	    }
        
        //add cube to root
        root.getChildren().add(cube);
        
        Button butSave = new Button("Save"); 
        butSave.setTranslateY(280);
        butSave.setOnAction(new EventHandler<ActionEvent>() {
	        @Override public void handle(ActionEvent e) {
	        	if(o.existBot()){
	        		o.save();
	        		
	        		Element racine = new Element("map");
	        		org.jdom2.Document document = new Document(racine);
	        		Element taille = new Element("nb_maps");
	        	    Attribute nb = new Attribute("nb",""+nb_map);
	        	    taille.setAttribute(nb);
	        	    racine.addContent(taille);
	        		try
	        	    {
	        			XMLOutputter sortie = new XMLOutputter(Format.getPrettyFormat());
	        			sortie.output(document, new FileOutputStream("./Niveaux/Bonus/sauv.xml"));
	        	    }
	        	    catch (java.io.IOException ex){}
	        		
	        		stage.hide();
	        	}
	        }
	    });
		
        root.getChildren().add(butSave);
        
        scn.setCamera(new PerspectiveCamera());
        
        scene_tab = scn;
        
        stage.setResizable(true);
        stage.setTitle("Editeur de niveau");
        stage.setScene(scene_tab);
        stage.show();
	}
	
	public static void maj_tab(){
        stage.setResizable(true);
        stage.setTitle("Editeur de niveau");
        stage.setScene(scene_tab);
        stage.show();
	}
	
	public void start(Stage primaryStage) {
    	StackPane root = new StackPane();
        Scene scn = new Scene(root, 600, 600, true);
    	
    	 Button button = new Button("OK");
         
         final TextField t1 = new TextField("0");
         final TextField t2 = new TextField("0");
         
         button.setOnAction(new EventHandler<ActionEvent>() {
             @Override public void handle(ActionEvent e) {
                 if(!t1.getText().equals("") && !t2.getText().equals("")){
                 	Editor.suite(Integer.parseInt(t1.getText()), Integer.parseInt(t2.getText()));
                 }   
             }
         });
         
         
         //add cube to root
         //root.getChildren().add(cube);
         root.getChildren().add(t1);
         t1.setTranslateY(-100);
         t1.setMaxWidth(100);
         root.getChildren().add(t2);
         t2.setTranslateY(-50);
         t2.setMaxWidth(100);
         root.getChildren().add(button);
         
         
         scn.setCamera(new PerspectiveCamera());
    	
         scene = scn;
    	stage = primaryStage;
        primaryStage.setResizable(true);
        primaryStage.setTitle("Taille");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
    
	public static void changecases(final int get_x, final int get_y) {
		StackPane root = new StackPane();
        Scene scn = new Scene(root, 600, 600, true);
		Group g = new Group();
		
		Cases lacase = o.getCases(get_x, get_y);
		
		ObservableList<String> options = FXCollections.observableArrayList("CasesVide","CasesPleine");
		
		final ComboBox<String> cb1 = new ComboBox<String>(options);
		
		options = FXCollections.observableArrayList("Inerte","Peignable","Teleport");
		
		final ComboBox<String> cb2 = new ComboBox<String>(options);
		
		options = FXCollections.observableArrayList("SUD","NORD","EST","OUEST");
		
		final ComboBox<String> cb = new ComboBox<String>(options);
		
		cb.setValue("SUD");
		
		final CheckBox check = new CheckBox();
		check.setTranslateY(50);
		
		check.selectedProperty().addListener(new ChangeListener<Boolean>() {@Override
			public void changed(ObservableValue<? extends Boolean> arg0, Boolean arg1, Boolean arg2) {
				if(check.selectedProperty().getValue()){
					cb.setVisible(true);
				}
				else{
					cb.setVisible(false);
				}
			}    
	    });
		
		final TextField t0 = new TextField("0"); 
		final TextField t1 = new TextField("0");
		final TextField t2 = new TextField("0");
		
		if(lacase.nom().equals("CasesVide")){
			cb1.setValue("CasesVide");
			cb2.setValue("Inerte");
			t0.setText("1");
			cb2.setVisible(false);
			check.setVisible(false);
			t0.setVisible(false);
			t1.setVisible(false);
			t2.setVisible(false);
			cb.setVisible(false);
		}
		else{
			t0.setText(""+((CasesPleine)o.getCases(get_x, get_y)).getPos().get_z());
			t0.setVisible(true);
			cb1.setValue("CasesPleine");
			
			String obj = ((CasesPleine)o.getCases(get_x, get_y)).getObjet().getNom();
			
			if(obj.equals("Inerte")){
				cb2.setValue("Inerte");
			}
			else if(obj.equals("Peignable")){
				cb2.setValue("Peignable");
			}
			else if(obj.equals("Teleport")){
				cb2.setValue("Teleport");
			}
			
			if(obj.equals("Teleport")){
				t1.setText(""+((Teleport)((CasesPleine)o.getCases(get_x, get_y)).getObjet()).get_xvers());
		        t2.setText(""+((Teleport)((CasesPleine)o.getCases(get_x, get_y)).getObjet()).get_yvers());
		        t1.setVisible(true);
		        t2.setVisible(true);
			}
			else{
				t1.setVisible(false);
			    t2.setVisible(false);
			}
			
			if(o.testBotExist(get_x, get_y)){
				check.setSelected(true);
				cb.setValue(o.capBot(get_x, get_y));
			}
			else{
				cb.setVisible(false);
			}
		}
		
		cb1.setTranslateY(-100);
		
		cb1.valueProperty().addListener(new ChangeListener<String>() {
	        @Override public void changed(ObservableValue ov, String t, String ffgg) {
	            //o.set_Cases(new CasesPleine(get_x, get_y, 1, new Inerte()), get_x, get_y);
	            //affEditCasePleine("Inerte", get_x, get_y,1);
	        	if(cb1.getValue().equals("CasesPleine")){
		        	cb2.setVisible(true);
		        	t0.setVisible(true);
		        	check.setVisible(true);
		        	
		        	if(o.testBotExist(get_x, get_y)){
						check.setSelected(true);
						cb.setValue(o.capBot(get_x, get_y));
					}
		        	
		        	String vers = cb2.getValue();
		            
		            if(vers.equals("Inerte")){
		            	t1.setVisible(false);
				        t2.setVisible(false);
					}
					else if(vers.equals("Peignable")){
						t1.setVisible(false);
				        t2.setVisible(false);
					}
					else if(vers.equals("Teleport")){
						t1.setVisible(true);
				        t2.setVisible(true);
					}
		        }
	        	else{
	        		check.setVisible(false);
	        		cb2.setVisible(false);
		        	t0.setVisible(false);
		        	t1.setVisible(false);
		        	t2.setVisible(false);
		        	cb.setVisible(false);
	        	}
	        	
	        	stage.show();
	        }    
	    });
		
		g.getChildren().add(check);
		
		g.getChildren().add(cb1);
		
		cb.setTranslateY(100);
		cb.setMaxWidth(100);
		g.getChildren().add(cb);
		
		t0.setMaxWidth(100);
		g.getChildren().add(t0);	
		cb2.setTranslateY(-50);
		
		cb2.valueProperty().addListener(new ChangeListener<String>() {
	        @Override public void changed(ObservableValue ov, String t, String tr) {
	            String vers = cb2.getValue();
	            
	            if(vers.equals("Inerte")){
	            	t1.setVisible(false);
			        t2.setVisible(false);
				}
				else if(vers.equals("Peignable")){
					t1.setVisible(false);
			        t2.setVisible(false);
				}
				else if(vers.equals("Teleport")){
					t1.setVisible(true);
			        t2.setVisible(true);
				}
	            stage.show();
	            //changecases(get_x, get_y);
	        }    
	    });
		
		g.getChildren().add(cb2);
		
		t1.setTranslateY(150);
        t1.setMaxWidth(100);
        t2.setTranslateY(200);
        t2.setMaxWidth(100);
		g.getChildren().add(t1);
		g.getChildren().add(t2);
		
		Button butQuit = new Button("Quitter");
		butQuit.setTranslateY(250);
		butQuit.setTranslateX(-50);
		butQuit.setOnAction(new EventHandler<ActionEvent>() {
	        @Override public void handle(ActionEvent e) {
	        	maj_tab();
	        }
	    });
        
		Button butChange = new Button("Appliquer");
		butChange.setTranslateY(250);
		butChange.setTranslateX(50);
		butChange.setOnAction(new EventHandler<ActionEvent>() {
	        @Override public void handle(ActionEvent e) {
	        	String lacase = cb1.getValue();
	        	if(lacase.equals("CasesVide")){
	        		if(o.VerifyAllCases(get_x, get_y)){
	        			o.set_Cases(new CasesVide(get_x, get_y, 0), get_x, get_y);
	        			o.suppBot(get_x, get_y);
	        			mesBouttons[get_x][get_y].setStyle("-fx-base: #"+(Color.WHITE).toString().split("x")[1]+";");
	        		}
	        	}
	        	else{
		        	String Type_vers = cb2.getValue();
		        	int z = Integer.parseInt(t0.getText());
		        	
		        	if(z<1){
		        		z = 1;
		        	}
		        	
	        		if(Type_vers.equals("Inerte")){
	        			o.set_Cases(new CasesPleine(get_x,get_y,z,new Inerte()),get_x, get_y);
	        			if(check.selectedProperty().getValue()){
        					o.addBot(get_x, get_y, Direction.capString(cb.getValue()));	
        				}
	        		}
	        		else if(Type_vers.equals("Peignable")){
	        			o.set_Cases(new CasesPleine(get_x,get_y,z,new Peignable()),get_x, get_y);
	        			if(check.selectedProperty().getValue()){
        					o.addBot(get_x, get_y, Direction.capString(cb.getValue()));	
        				}
	        		}
	        		else if(Type_vers.equals("Teleport")){
	        			int x_vers = Integer.parseInt(t1.getText());
	        			int y_vers = Integer.parseInt(t2.getText());
	        			
	        			if(o.CasesValide(x_vers,y_vers)){
	        				o.set_Cases(new CasesPleine(get_x,get_y,z,new Teleport(x_vers,y_vers,1)),get_x, get_y);
	        				
	        				if(check.selectedProperty().getValue()){
	        					o.addBot(get_x, get_y, Direction.capString(cb.getValue()));	
	        				}
	        			}
	        		}
	        		mesBouttons[get_x][get_y].setStyle("-fx-base: #"+((CasesPleine)(o.getCases(get_x,get_y))).getObjet().getColor().toString().split("x")[1]+";");
	        	}
	        	
	        	maj_tab();
	        }
	    });
        
        g.getChildren().addAll(butQuit, butChange);
        
        root.getChildren().add(g);
        scn.setCamera(new PerspectiveCamera());
    	
        scene = scn;
        stage.setResizable(true);
        stage.setTitle("Edition case "+get_x+" "+get_y);
        stage.setScene(scene);
        stage.show();
	}
}
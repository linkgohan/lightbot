package world;

import item.ItemsStatique;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Sphere;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javafx.scene.Group;
import lightbotException.OutOfBoundBot;
import main.Main;

import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import affichage.AffichagePartie;
import robot.Robot;
import cases.Cases;
import cases.CasesPleine;
import cases.CasesVide;
import direction.Direction;

public class World {
	static private Cases Plateau[][];
	static int nb_col;
	static int nb_lig;
	static int nb_r;
	static Robot[] tab_r;
	static org.jdom2.Document document; 
	static Element racine;
	static String sauvegarde;
	static int nb_lvl;
	static LinkedList<CasesPleine> listCasesAColorer;
	public static LinkedList<Sphere> listSphere;
	
	public static void initRobot(){
		for(int i = 0; i<listSphere.size(); i++){
			AffichagePartie.getField().getChildren().remove(listSphere.get(i));
		}
		
		listSphere = new LinkedList<Sphere>();
		
		for(int i = 0; i<nb_r; i++){
			AffichagePartie.getField().getChildren().remove(AffichagePartie.getField().getChildren().size()-1);
		}
		
		for(int i = 0; i<nb_r; i++){
			tab_r[i] = new Robot(tab_r[i]);
			AffichagePartie.getField().getChildren().add(tab_r[i].getRobot3D());
		}
	}
	
	public static boolean VerifyCasesAColorer(){
		for(CasesPleine c : listCasesAColorer){
			if(!c.get_color().equals(Color.YELLOW)){
				return false;
			}
		}
		return true;
	}
	
	public static void reinitCases(){
		Iterator i = listCasesAColorer.iterator();
		CasesPleine cP;
		
		while(i.hasNext()){
			cP = (CasesPleine) i.next();
			Main.addT(0,cP.initColor(1));
		}
	}
	
	public static void debloqueLvl(){
		if(nb_lvl != -1){
			org.jdom2.Document document = null; 
	    	Element racine;
	    	int nb_maps = 0;
	    	int nb_maps_debloques = 0;
	    	SAXBuilder sxb = new SAXBuilder();
			try
			{ 
				document = sxb.build(new File(sauvegarde+"sauv.xml"));
			}
			catch(Exception e){};
			
			System.out.println(sauvegarde+"sauv.xml");
			
			racine = document.getRootElement();
			
			Element courant = null;
			List listMap = racine.getChildren("nb_maps");
			Iterator iteMap = listMap.iterator();
			while(iteMap.hasNext())
			{
				courant = (Element)iteMap.next();
				nb_maps = Integer.parseInt(courant.getAttributeValue("nb"));
			}
			
			List listDebloque = racine.getChildren("nb_maps_debloques");
			Iterator iteDebloque = listDebloque.iterator();
			while(iteDebloque.hasNext())
			{
				courant = (Element)iteDebloque.next();
				nb_maps_debloques = Integer.parseInt(courant.getAttributeValue("nb"));
			}
			
			System.out.println(nb_maps+" "+nb_maps_debloques);
			
			if(nb_maps_debloques < nb_maps && nb_lvl == nb_maps_debloques){
				nb_maps_debloques++;
				System.out.println(nb_maps+" "+nb_maps_debloques);
				courant.getAttribute("nb").setValue(""+nb_maps_debloques);
				try{
					XMLOutputter sortie = new XMLOutputter(Format.getPrettyFormat());
					sortie.output(document, new FileOutputStream(sauvegarde+"sauv.xml"));
				}
				catch (java.io.IOException e){}
			}
		}
	}
	
	public static void init(String fichier, String sauv, int lvl) throws ClassNotFoundException, InstantiationException, IllegalAccessException{
		sauvegarde = sauv;
		nb_lvl = lvl;
		listCasesAColorer = new LinkedList<CasesPleine>();
		listSphere = new LinkedList<Sphere>();
		
		SAXBuilder sxb = new SAXBuilder();
		try
		{ 
			document = sxb.build(new File(fichier));
		}
		catch(Exception e){};
		
		racine = document.getRootElement();

		initWorld();
	}
	
	static void initWorld() throws ClassNotFoundException, InstantiationException, IllegalAccessException
	{
		Element courant;
		List listTaille = racine.getChildren("taille");
		Iterator iteTaille = listTaille.iterator();
		List listRobot = racine.getChildren("robot");
		Iterator iteRobot = listRobot.iterator();
		List listCasesP = racine.getChildren("CasesPleine");
		Iterator iteCasesP = listCasesP.iterator();
		List listCasesV = racine.getChildren("CasesVide");
		Iterator iteCasesV = listCasesV.iterator();
		
		while(iteTaille.hasNext())
		{
			courant = (Element)iteTaille.next();
			nb_col = Integer.parseInt(courant.getAttributeValue("colonne"));
			nb_lig = Integer.parseInt(courant.getAttributeValue("ligne"));
			
		}
		
		int i = 0;
		tab_r = new Robot[2];
		int x;
		int y;
		int z;
		String direc;
		Direction.capType d = Direction.capType.Sud;
		
		while(iteRobot.hasNext())
		{
			courant = (Element)iteRobot.next();
			if(i < 2){
				x = Integer.parseInt(courant.getAttributeValue("x"));
				y = Integer.parseInt(courant.getAttributeValue("y"));
				z = Integer.parseInt(courant.getAttributeValue("z"));
				direc = courant.getAttributeValue("cap");
				
				switch(direc){
					case "SUD" : d = Direction.capType.Sud; break;
					case "NORD" : d = Direction.capType.Nord; break;
					case "EST" : d = Direction.capType.Est; break;
					case "OUEST" : d = Direction.capType.Ouest;
				}
				
				tab_r[i] = new Robot(x, y, z, d, i);
				i++;
			}
		}
		
		nb_r = i;
		
		Plateau = new Cases[nb_lig][nb_col];
		
		String type;
		ItemsStatique objet = null;
		
		while(iteCasesP.hasNext())
		{
			courant = (Element)iteCasesP.next();
			type = courant.getAttributeValue("type");
			
			x = Integer.parseInt(courant.getAttributeValue("x"));
			y = Integer.parseInt(courant.getAttributeValue("y"));
			z = Integer.parseInt(courant.getAttributeValue("z"));
			
			if(type.equals("Inerte")){
				objet = new item.Inerte();
			}
			else if(type.equals("Peignable")){
				objet = new item.Peignable();
			}
			else if(type.equals("Teleport")){
				objet = new item.Teleport(Integer.parseInt(courant.getAttributeValue("x_vers")), Integer.parseInt(courant.getAttributeValue("y_vers")), Integer.parseInt(courant.getAttributeValue("z_vers")));
			}
			
			Plateau[x][y] = new CasesPleine(x, y, z, objet);
			
			if(type.equals("Peignable")){
				listCasesAColorer.add((CasesPleine) Plateau[x][y]);
			}
		}
		
		System.out.println(""+listCasesAColorer.size());
		
		while(iteCasesV.hasNext())
		{
			courant = (Element)iteCasesV.next();
			
			x = Integer.parseInt(courant.getAttributeValue("x"));
			y = Integer.parseInt(courant.getAttributeValue("y"));
			
			Plateau[x][y] = new CasesVide(x,y,0);
		}
		
	}
	
	public static boolean can_step_on(Robot bot,int l,int c){
		if(l>=0 && l<=(nb_lig-1) && c>=0 && c<=(nb_col-1)){
			return ((Plateau[l][c]).can_step_on(bot));
		}
		return false;
	}
	
	public static boolean can_jump_on(Robot bot,int l,int c){
		if(l>=0 && l<=(nb_lig-1) && c>=0 && c<=(nb_col-1)){
			return ((Plateau[l][c]).can_jump_on(bot));
		}
		else return false;
	}
	
	 public static Cases get_case(int l,int c) throws OutOfBoundBot{
		 if(l>=0 && l<=(nb_lig-1) && c>=0 && c<=(nb_col-1)){
		   return Plateau[l][c];
	   }
	   else {
		   throw new OutOfBoundBot();
	   }
	 }
	 
	 public static Group getBox(int x, int y){
		 return Plateau[x][y].Aff3D();
	 }
	 
	 public static int getCol(){
		 return nb_col;
	 }
	 
	 public static int getLig(){
		 return nb_lig;
	 }
	 
	 public static int getNbRobot(){
		 return nb_r;
	 }
	 
	 public static Robot getRobot(int nb){
		 return tab_r[nb];
	 }
}

package ordre_bot;
import javafx.animation.TranslateTransition;
import javafx.scene.paint.Color;
import javafx.util.Duration;
import lightbotException.OutOfBoundBot;
import main.Main;
import robot.*;
import world.*;

public abstract class Ordre_bot {
	protected Color clr;
	protected int code;
	
	public void run(Robot bot) throws OutOfBoundBot{
		try{
			if(clr == Color.CYAN || (World.get_case(bot.getPosition().get_x(),bot.getPosition().get_y()).get_color() == clr)){
				run_exe(bot);
			}
			else{
				Main.addT(bot.getRobotId(), new TranslateTransition(Duration.millis(500),bot.getRobot3D()));
			}
		}
		catch(OutOfBoundBot o){
			System.out.println("BALALALALAL "+bot.getRobotId());
			Main.addT(bot.getRobotId(), new TranslateTransition(Duration.millis(500),bot.getRobot3D()));
		}
	}
	
	public abstract void run_exe(Robot bot) throws OutOfBoundBot;
	
	public int get_code(){
		return code;
	}
	
	public Color get_color(){
		return clr;
	}
}

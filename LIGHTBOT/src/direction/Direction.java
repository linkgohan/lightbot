package direction;

public class Direction {
	private capType monCap;
	
	public Direction(capType cap){
		this.monCap = cap;
	}
	
	
	public enum capType{
		Nord, Sud, Est, Ouest;
	}
	
	public static capType capString(String cap){
		switch(cap){
			case "SUD": return capType.Sud;
			case "NORD": return capType.Nord;
			case "EST": return capType.Est;
			default: return capType.Ouest;
		}
	}
	
	public static String capToString(capType cap){
		switch(cap){
			case Sud: return "SUD";
			case Nord: return "NORD";
			case Est: return "EST";
			default: return "OUEST";
		}
	}
	
	public capType getCap(){
		return monCap;
	}
	
	public void setCap(capType monCap){
		this.monCap = monCap;
	}
}